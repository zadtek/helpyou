package com.zadetk.helpyou.receiver;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.util.Log;


import com.zadetk.helpyou.R;
import com.zadetk.helpyou.activities.MainActivity;
import com.zadetk.helpyou.activities.MyOrderActivity;
import com.zadetk.helpyou.utils.JsonUtils;

import java.util.Map;

import cn.jpush.android.api.JPushInterface;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * 自定义接收器
 * 
 * 如果不定义这个 Receiver，则： 1) 默认用户会打开主界面 2) 接收不到自定义消息
 */
public class MyReceiver extends BroadcastReceiver {
	private static final String TAG = "JPush";

	private NotificationManager nm;

	private String uid;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (null == nm) {
			nm = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);

		}
		Bundle bundle = intent.getExtras();

		if (JPushInterface.ACTION_REGISTRATION_ID.equals(intent.getAction())) {
			String regId = bundle
					.getString(JPushInterface.EXTRA_REGISTRATION_ID);
			// Log.d(TAG, "[MyReceiver] 接收Registration Id : " + regId);
			// send the Registration Id to your server...

		} else if (JPushInterface.ACTION_MESSAGE_RECEIVED.equals(intent
				.getAction())) {
			// 自定义消息
//			processCustomMessage(context, bundle);

		} else if (JPushInterface.ACTION_NOTIFICATION_RECEIVED.equals(intent
				.getAction())) {
			// 通知消息
			//TODO 更改推送铃声，会弹出两个提示框
//			 String title = bundle.getString(JPushInterface.EXTRA_TITLE);
//			 String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
			 String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
//			 NotificationHelper.showMessageNotification(context, nm, title,
//			 message, extras, bundle, "");
			Log.i("yudan", "收到了通知" + bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE) + "---" + bundle.getString(JPushInterface.EXTRA_ALERT));

			processCustomMessage(context, bundle);
		} else if (JPushInterface.ACTION_NOTIFICATION_OPENED.equals(intent
				.getAction())) {
//			 Log.d(TAG, "[MyReceiver] 用户点击打开了通知");
			//TODO 推送跳转测试代码，根据以后需求待改进
//            Intent i = new Intent(context, MainActivity.class);  //自定义打开的主页面
//            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//            context.startActivity(i);


		}
	}

	/**
	 * 自定义推送的声音
	 *
	 * @param context
	 * @param bundle
	 */
	private boolean processCustomMessage(Context context, Bundle bundle) {
//        JSONObject json = null;
		NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
		//这一步必须要有而且setSmallIcon也必须要，没有就会设置自定义声音不成功
		notification.setAutoCancel(true).setSmallIcon(R.mipmap.ic_launcher);
		/**
		 *  这个地方特殊说明一下
		 *  因为我们发出推送大部分是分为两种，一种是在极光官网直接发送推送，第二是根据后台定义
		 * 的NOTIFICATION发送通知，所以在这里你要根据不同的方式获取不同的内容
		 **/
		//如果是使用后台定的NOTIFICATION发送推送，那么使用下面的方式接收消息
//        String alert = bundle.getString(JPushInterface.EXTRA_EXTRA);  //扩展消息
		String title = bundle.getString(JPushInterface.EXTRA_NOTIFICATION_TITLE);  //推送标题
		String msg = bundle.getString(JPushInterface.EXTRA_ALERT);   //推送消息

		//如果是直接使用极光官网发送推送，那么使用这个来接收消息内容
		// String title = bundle.getString(JPushInterface.EXTRA_TITLE);
		//String msg = bundle.getString(JPushInterface.EXTRA_MESSAGE);
		//  String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
		notification.setSound(
				Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.new_dianba_order));
		notification.setContentTitle(title).setContentText(msg);
		//alert属于附加字段消息 这里我们增加了一个sound的附加字段，判断sound="trade_mall”时则播放自定义的消息，即：R.raw.test就是自己定义的声音，这个是放在raw文件夹内的，需要自己提供
		//如果没有sound字段，或者字段内容不是trade_mall，这里就直接返回false ，再继续使用系统的声音。这样就可以实现再特殊情况使用自定义铃声，也可以在一般情况使用系统铃声
//        try {
//            json = new JSONObject(alert);
//            if (json.has("sound")) {
//                if (json.getString("sound").equals("trade_mall")) {
//                    notification.setSound(
//                            Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.new_dianba_order));
//                } else {
//                    return false;
//                }
//            } else {
//                return false;
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }

		//这是点击通知栏做的对应跳转操作，可以根据自己需求定义
		Intent i = new Intent(context, MyOrderActivity.class);  //自定义打开的主页面
		i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);

		Map<String,Object> map = JsonUtils.json2Map(extras); // 1-我的需求 2-我的服务 3-系统消息
		if (map.get("state") != null){
			if ("1".equals(map.get("state").toString())){
				i.putExtra("index",1);
			} else if ("2".equals(map.get("state").toString())){
				i.putExtra("index",2);
			} else if ("3".equals(map.get("state").toString())){
				i.putExtra("index",0);
			}
		}
		i.putExtra("index",1);
//		context.startActivity(i);
//        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);

//        Intent mIntent = new Intent(context, );
//        mIntent.putExtra("msgId", json.getString("msgId"));
//        mIntent.putExtras(bundle);
//        mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//        Bundle newBundle=new Bundle();
//        bundle.putString("action","autologin");
		PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, i, 0);
		notification.setContentIntent(pendingIntent)
				.setAutoCancel(true)
				.setContentText(msg)
				.setContentTitle(title.equals("") ? "title" : title)
				.setSmallIcon(R.mipmap.ic_launcher);//.setExtras(newBundle)
		//获取推送id
		int notifactionId = bundle.getInt(JPushInterface.EXTRA_NOTIFICATION_ID);
		Log.i("yudan", title + "," + msg + "," + notifactionId);
		//bundle.get(JPushInterface.EXTRA_ALERT);推送内容
		//最后刷新notification是必须的
		NotificationManager notificationManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
		notificationManager.notify(notifactionId, notification.build()); //在此处放入推送id —notifactionId
		return true;
	}
//	// send msg to MenuTabActivity
//	private void processCustomMessage(Context context, Bundle bundle) {
//		String title = bundle.getString(JPushInterface.EXTRA_TITLE);
//		String message = bundle.getString(JPushInterface.EXTRA_MESSAGE);
//		String extras = bundle.getString(JPushInterface.EXTRA_EXTRA);
//
//		if (extras != null) {
//			PushUserData pushData = JsonProcessHelper
//					.jsonProcess_getPushData(extras);
//			if (pushData != null) {
//				String type = pushData.getQs_type();
//				String authorid = pushData.getXyrr_qsid();
//				SharedPreferences sp = context.getSharedPreferences(
//						Const.PREF_NAME, Context.MODE_PRIVATE);
//				uid = sp.getString("uid", "");
//				if (!TextUtils.isEmpty(extras)) {
//					try {
//						NotificationCompat.Builder notification = new NotificationCompat.Builder(context);
//
//						JSONObject extraJson = new JSONObject(extras);
//						if (null != extraJson && extraJson.length() > 0) {
//							String sound = extraJson.getString("sound");
//							if("1".equals(sound)){
//								notification.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" + R.raw.new_order));
//							} else {
//								notification.setSound(Uri.parse("android.resource://" + context.getPackageName() + "/" +R.raw.new_order));
//							}
//						}
//					} catch (JSONException e) {
//						e.printStackTrace();
//					}
//				}
//
//				NotificationHelper.showMessageNotification(context, nm, title,
//						message, extras, bundle, type);
//
//				// 获取数量
//				initArrays(new Handler() {
//					@Override
//					public void handleMessage(Message msg) {
//						if (!"emp".equals(msg.obj.toString())) {
//							QiangDanData sumcout = (QiangDanData) msg.obj;
//							String xincount = sumcout.getNeworder();
//							String noget = sumcout.getNoget();
//							String nosend = sumcout.getNosend();
//							if (MainActivity.radio_goqh != null) {
//								MainActivity.radio_goqh.setText("待取货(" + noget
//										+ ")");
//							}
//							if (MainActivity.radio_gops != null) {
//								MainActivity.radio_gops.setText("配送中(" + nosend
//										+ ")");
//							}
//						} else {
//						}
//					}
//				});
//
//			}
//		}
//
//	}
}
