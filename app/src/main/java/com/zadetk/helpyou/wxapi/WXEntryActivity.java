package com.zadetk.helpyou.wxapi;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

import com.tencent.mm.opensdk.constants.ConstantsAPI;
import com.tencent.mm.opensdk.modelbase.BaseReq;
import com.tencent.mm.opensdk.modelbase.BaseResp;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.zadetk.helpyou.App;
import com.zadetk.helpyou.CityPicker.Constant;
import com.zadetk.helpyou.MessageEvents;
import com.zadetk.helpyou.activities.LoginActivity;
import com.zadetk.helpyou.activities.MainActivity;
import com.zadetk.helpyou.activities.MySkilldetailActivity;
import com.zadetk.helpyou.activities.WXLoginActivity;
import com.zadetk.helpyou.bean.UserLoginBean;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.SPUtil;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.isWxLogin;
import static com.zadetk.helpyou.other.Const.APP_ID;
import static com.zadetk.helpyou.other.Const.APP_SECRET;
import static com.zadetk.helpyou.other.Const.USER_IS_LOGIN;

/**
 * Created by Administrator on 2017/11/9.
 */
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {

    private Context context;
    private IWXAPI iwxapi;
    private ProgressDialog dialog;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String error_code = bundle.getString("error_code");
            switch (msg.what) {
                case 0:
                    if (error_code.equals("0")) {//已注册
                        String uid = bundle.getString("uid");
                        String username = bundle.getString("username");
                        String token = bundle.getString("token");
                        UserInfoManger.login(token,username,Integer.parseInt(uid));
                        App.finishAllActivity();
                        startActivity(new Intent(WXEntryActivity.this, MainActivity.class));
//                        EventBus.getDefault().postSticky(
//                                new MessageEvents()
//                                        .setEventsType(
//                                                MessageEvents.WXLOGIN_SUCCESS));
                        finish();
                    } else if (error_code.equals("1")) {//未注册过，跳转绑定手机界面
                        Intent intent = new Intent(WXEntryActivity.this, WXLoginActivity.class);
                        intent.putExtra("access_token", bundle.getString("access_token"));
                        intent.putExtra("openId", bundle.getString("openId"));
                        startActivity(intent);
                    }
                    closeDialog();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        context = this;
        iwxapi = WXAPIFactory.createWXAPI(this, "wx071a16ef2a9e5d5d", false);
        iwxapi.handleIntent(getIntent(), this);
    }

    @Override
    public void onReq(BaseReq req) {//发送请求时调用
        // TODO Auto-generated method stub


    }

    @Override
    public void onResp(BaseResp resp) {
        // TODO Auto-generated method stub
        if (resp.getType() == ConstantsAPI.COMMAND_SENDAUTH) {
            switch (resp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    //登录成功
                    dialog = ProgressDialog.show(WXEntryActivity.this, "", "正在登录……", true, false, null);
                    String code = ((SendAuth.Resp) resp).code;
                    getOpenId(code);

                    break;
            }

        } else if (resp.getType() == ConstantsAPI.COMMAND_SENDMESSAGE_TO_WX) {
            switch (resp.errCode) {
                case BaseResp.ErrCode.ERR_OK:
                    //分享成功
                    Toast.makeText(context, "分享成功", Toast.LENGTH_SHORT).show();
                    this.finish();
                    break;
                case BaseResp.ErrCode.ERR_USER_CANCEL:
                    //分享取消
                    Toast.makeText(context, "分享取消", Toast.LENGTH_SHORT).show();
                    this.finish();
                    break;
                case BaseResp.ErrCode.ERR_AUTH_DENIED:
                    //分享拒绝
                    Toast.makeText(context, "分享拒绝", Toast.LENGTH_SHORT).show();
                    this.finish();
                    break;
            }
        }
    }

    private void getOpenId(final String code) {
        StringBuffer loginUrl = new StringBuffer();
        loginUrl.append("https://api.weixin.qq.com/sns/oauth2/access_token")
                .append("?appid=")
                .append(APP_ID)
                .append("&secret=")
                .append(APP_SECRET)
                .append("&code=")
                .append(code)
                .append("&grant_type=authorization_code");
        OkHttpClient okHttpClient = new OkHttpClient();

        Request.Builder builder = new Request.Builder();
        Request request = builder.get().url(String.valueOf(loginUrl)).build();
        Log.i("yudan", "url=" + String.valueOf(loginUrl));
        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String responseData = response.body().string();
                    Log.i("yudan", "微信登录获取openId:" + responseData);
                    try {
                        JSONObject object = new JSONObject(responseData);
                        String access_token = object.getString("access_token");
                        String refresh_token = object.getString("refresh_token");
                        String openid = object.getString("openid");
                        ifLogin(access_token, openid);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    throw new IOException("Unexpected code" + response);
                }
            }
        });
    }

    /**
     * 微信登录是否注册校验接口
     *
     * @param openId
     */
    private void ifLogin(final String access_token, final String openId) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();
                builder.add("openId", openId);
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(isWxLogin).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "微信登录是否注册校验接口:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 0;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {//已注册
                            JSONObject data = object.getJSONObject("data");
                            String uid = data.getString("uid");
                            String username = data.getString("username");
                            String token = data.getString("token");
                            bundle.putString("uid", uid);
                            bundle.putString("username", username);
                            bundle.putString("token", token);
                        }
                        bundle.putString("access_token", access_token);
                        bundle.putString("openId", openId);
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    closeDialog();
                    Log.i("yudan", "微信登录是否注册校验接口:" + e);
                    e.printStackTrace();
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "微信登录是否注册校验接口:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }





}