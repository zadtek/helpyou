package com.zadetk.helpyou.base;

import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.zadetk.helpyou.R;

/**
 * Created by Zackv on 2018/4/11.
 */

public abstract class AbsListFragment extends AbsFragment implements BaseQuickAdapter.RequestLoadMoreListener, BaseQuickAdapter.OnItemClickListener, SwipeRefreshLayout.OnRefreshListener {
    private SwipeRefreshLayout swipeRefreshLayout;
    private RecyclerView recyclerView;
    private BaseQuickAdapter adapter;

    @Override
    public int setView() {
        return R.layout.fragment_list;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initView();
        initLogic();
        firstRequestData();
    }


    private void initLogic() {
        adapter = getAdapter();
        adapter.setOnLoadMoreListener(this, recyclerView);
        adapter.setOnItemClickListener(this);
        adapter.setEmptyView(R.layout.normal_empty_view);
        swipeRefreshLayout.setOnRefreshListener(this);
        recyclerView.setAdapter(adapter);
    }


    private void initView() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.refresh);
        recyclerView = (RecyclerView) findViewById(R.id.recycleview);
        LinearLayoutManager layoutManager = new LinearLayoutManager(mActivity);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void onLoadMoreRequested() {
        loadMore();
    }

    @Override
    public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
        OnItemClick(position);
    }


    @Override
    public void onRefresh() {
        OnRefresh();
    }

    public void setRefreshing(boolean b) {
        swipeRefreshLayout.setRefreshing(b);
    }

    protected abstract void firstRequestData();

    protected abstract void loadMore();

    public abstract BaseQuickAdapter getAdapter();

    protected abstract void OnItemClick(int position);

    protected abstract void OnRefresh();
}
