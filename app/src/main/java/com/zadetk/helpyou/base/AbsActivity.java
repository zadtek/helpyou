package com.zadetk.helpyou.base;

/**
 * Created by Zackv on 2018/4/2.
 */

import android.app.Activity;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.gyf.barlibrary.ImmersionBar;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.receiver.ExampleUtil;

import java.util.Set;

import cn.jpush.android.api.JPushInterface;
import cn.jpush.android.api.TagAliasCallback;


public abstract class AbsActivity extends BaseActivity {

    public ImmersionBar immersionBar;
    protected Activity mActivity;
    protected LinearLayout mToolbar;

    public Handler mHandler;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        mActivity = this;
        immersionBar = ImmersionBar.with(this);
        immersionBar.statusBarDarkFont(true, 0.2f);
        immersionBar.fitsSystemWindows(isfit());
        immersionBar.statusBarColor(getStatusBarColor());
        immersionBar.init();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);//竖屏

        if (fullScreen()) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                    , WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }

        overridePendingTransition(R.anim.push_pic_left_in, R.anim.push_pic_left_out);
        final int vid = setView();

        super.onCreate(savedInstanceState);

        if (vid != 0) {
            LinearLayout layout = new LinearLayout(this);
            mToolbar = initToolbar(layout);
            if (mToolbar != null) {

                layout.setOrientation(LinearLayout.VERTICAL);

                View container = getLayoutInflater().inflate(vid, layout, false);

                layout.addView(mToolbar);

                layout.addView(container, new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT
                        , LinearLayout.LayoutParams.MATCH_PARENT));

                setContentView(layout);

            } else {
                setContentView(vid);
            }
            initBasic(savedInstanceState);
        }

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                // TODO Auto-generated method stub
                super.handleMessage(msg);
                switch (msg.what) {
                    case Const.MSG_SET_ALIAS:// 绑定别名
                        JPushInterface.setAliasAndTags(getApplicationContext(),
                                (String) msg.obj, null, mAliasCallback);
                        break;
                }
            }
        };
    }

    public boolean isfit() {
        return true;
    }

    public LinearLayout initToolbar(ViewGroup parent) {
        LinearLayout toolbar = (LinearLayout) getLayoutInflater().inflate(R.layout.title_bar, parent, false);
        View back = toolbar.findViewById(R.id.iv_back);
        if (back != null) {
            back.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onBackPressed();
                }
            });
        }
        return toolbar;
    }


    // 设置推送别名
    public void setAlias(String alias) {
        if (!ExampleUtil.isValidTagAndAlias(alias)) {
            return;
        }
        // 调用JPush API设置Alias
        mHandler.sendMessage(mHandler.obtainMessage(Const.MSG_SET_ALIAS, alias));
    }



    // 设置别名与标签方法的回调类，可在 gotResult 方法上得到回调的结果。回调 responseCode = 0，则确认设置成功。
    public final TagAliasCallback mAliasCallback = new TagAliasCallback() {
        @Override
        public void gotResult(int code, String alias, Set<String> tags) {
            String logs;
            switch (code) {
                case 0:
                    logs = "Set tag and alias success:" + alias;
                    break;

                case 6002:
                    logs = "Failed to set alias and tags due to timeout. Try again after 60s.";
                    if (ExampleUtil.isConnected(getApplicationContext())) {
                        mHandler.sendMessageDelayed(
                                mHandler.obtainMessage(Const.MSG_SET_ALIAS, alias),
                                1000 * 60);
                    } else {
                        Log.v("sd", "No network");
                    }
                    break;

                default:
                    logs = "Failed with errorCode = " + code;
            }

//             ExampleUtil.showToast(logs, getApplicationContext());
        }

    };

    public void setTopTitle(String title) {
        if (mToolbar != null) {
            TextView titleBar = (TextView) mToolbar.findViewById(R.id.tv_title);
            if (titleBar != null) {
                titleBar.setText(title);
            }
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        Glide.with(this).pauseRequestsRecursive();
        JPushInterface.onPause(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Glide.with(this).resumeRequestsRecursive();
        JPushInterface.onResume(this);
    }

    @Override
    public void finish() {
        super.finish();
        overridePendingTransition(R.anim.push_pic_right_in, R.anim.push_pic_right_out);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (immersionBar != null)
            immersionBar.destroy();
    }

    /**
     * 是否全屏
     *
     * @return true则全屏
     */
    public boolean fullScreen() {
        return false;
    }

    /**
     * 关联主界面
     */
    public abstract int setView();

    /**
     * 基础初始化
     */
    public abstract void initBasic(Bundle savedInstanceState);

    public int getStatusBarColor() {
        return R.color.colorWhite;
    }
}

