package com.zadetk.helpyou.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.zadetk.helpyou.R;
import com.zadetk.helpyou.utils.ToastUtil;

import java.util.List;

public class AboutAdapter extends RecyclerView.Adapter<AboutAdapter.RecyclerViewHolder>{

    private List<String> data;
    public AboutAdapter(List<String> num){
        this.data = num;
    }
    @Override
    public AboutAdapter.RecyclerViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_about_item,parent,false);
        return new RecyclerViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(AboutAdapter.RecyclerViewHolder holder, int position) {
        holder.about.setText(data.get(position));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ToastUtil.showToast("需要点击跳转到下一界面！");
            }
        });
    }

    @Override
    public int getItemCount() {
        return data.size();
    }
    class RecyclerViewHolder extends RecyclerView.ViewHolder{
        TextView about;
        public RecyclerViewHolder(View itemView) {
            super(itemView);
            about = itemView.findViewById(R.id.tv_about);
        }
    }
}
