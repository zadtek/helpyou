package com.zadetk.helpyou.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.bean.VoiceBean;

import java.util.List;

/**
 * Created by Administrator on 2017/5/18.
 */

public class VoiceAdapter extends RecyclerView.Adapter<VoiceAdapter.ViewHolder> implements View.OnClickListener {
    private List<VoiceBean> recyclerItemList;
    private OnItemClickListener mOnItemClickListener = null;
    //    private ViewHolder holder;
    private Context context;

    public VoiceAdapter(Context context, List<VoiceBean> recyclerItemList) {
        this.recyclerItemList = recyclerItemList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.voice_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        view.setOnClickListener(this);
        return holder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        VoiceBean item = recyclerItemList.get(position);
//        TextView tv_name,tv_desc1,tv_comment_num;
//        SimpleRatingBar rb_rating;
//        ImageView icon;
        holder.tv_name.setText(item.getSitetitle());
        holder.tv_desc1.setText(item.getSitedesc());
        holder.tv_comment_num.setText("(" + item.getCommentnum() + ")");
        holder.rb_rating.setRating((float) item.getStarnum());
        Glide.with(context).load(item.getPhotourl()).into(holder.icon);
        //将position保存在itemView的Tag中，以便点击时进行获取
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return recyclerItemList == null ? 0 : recyclerItemList.size();
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取position
            mOnItemClickListener.onItemClick(v, (int) v.getTag());
        }
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public static interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        TextView tv_name, tv_desc1, tv_comment_num;
        SimpleRatingBar rb_rating;
        ImageView icon;

        public ViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.icon);
            tv_name = (TextView) itemView.findViewById(R.id.tv_name);
            tv_desc1 = (TextView) itemView.findViewById(R.id.tv_desc1);
            tv_comment_num = (TextView) itemView.findViewById(R.id.tv_comment_num);
            rb_rating = (SimpleRatingBar) itemView.findViewById(R.id.rb_rating);
        }
    }
}
