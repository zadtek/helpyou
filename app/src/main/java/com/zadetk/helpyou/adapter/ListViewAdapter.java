package com.zadetk.helpyou.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.zadetk.helpyou.R;
import com.zadetk.helpyou.activities.PublishServiceActivity;
import com.zadetk.helpyou.bean.ServiceSub;
import com.zadetk.helpyou.bean.ServicetwocatBean;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.view.NoScrollView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ListViewAdapter extends BaseAdapter {

	private List<ServicetwocatBean> mList;
	private List<ServiceSub> tList;
	private Context mContext;
	private GridViewAdapter gridViewAdapter;
	private ServicetwocatBean servicetwocatBean;
	public ListViewAdapter(List<ServicetwocatBean> mList, Context mContext, List<ServiceSub> tList) {
		super();
		this.mList = mList;
		this.tList =tList;
		this.mContext = mContext;
	}


	@Override
	public int getCount() {
		if (mList == null) {
			return 0;
		} else {
			return this.mList.size();
		}
	}

	@Override
	public Object getItem(int position) {
		if (mList == null) {
			return null;
		} else {
			return this.mList.get(position);
		}
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder = null;

		if (convertView == null) {
			holder = new ViewHolder();
			convertView = LayoutInflater.from(this.mContext).inflate(R.layout.layout_listview, null, false);
			holder.textView = (TextView) convertView.findViewById(R.id.secondtitle) ;
			holder.gridView = (NoScrollView) convertView.findViewById(R.id.fixed_gridView);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}


		if (this.mList != null) {
			holder.textView.setText(mList.get(position).getCatname());
			if (holder.gridView != null) {
				List<ServiceSub> arrayListForEveryGridView = mList.get(position).getLevel();
				gridViewAdapter=new GridViewAdapter(mContext, arrayListForEveryGridView);
				holder.gridView.setAdapter(gridViewAdapter);

				holder.gridView.setClickable(false);
				holder.gridView.setPressed(false);
				holder.gridView.setEnabled(false);

			}
		}
		return convertView;
	}


	private class ViewHolder {
		TextView textView;
		NoScrollView gridView;
	}
}
