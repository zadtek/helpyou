package com.zadetk.helpyou.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.zadetk.helpyou.R;
import com.zadetk.helpyou.activities.MyServiceActivity;
import com.zadetk.helpyou.activities.MySkilldetailActivity;
import com.zadetk.helpyou.activities.PublishServiceActivity;
import com.zadetk.helpyou.activities.PublishServiceSubActivity;
import com.zadetk.helpyou.bean.ServiceSub;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GridViewAdapter extends BaseAdapter {
    private static final String TAG = PublishServiceSubActivity.class.getName();
    private Context mContext;
    private List<ServiceSub> mList;
    private LayoutInflater inflater;
    private ServiceSub serviceSub;

    public GridViewAdapter(Context mContext, List<ServiceSub>  mList) {
        this.mContext = mContext;
        this.mList = mList;
        inflater = LayoutInflater.from(mContext);
    }

    @Override
    public int getCount() {
        if (mList == null)
            return 0;
        return this.mList.size();
    }

    @Override
    public Object getItem(int position) {
        if (mList == null)
            return null;
        return this.mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder = null;
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = inflater.inflate(R.layout.layout_gridview, null);
            viewHolder.mTextView = (TextView) convertView.findViewById(R.id.textView_gridView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if (mList != null) {
            viewHolder.mTextView.setText(mList.get(position).getCatname());
            viewHolder.mTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.i(TAG, "=========position========"+ position);
                    Intent in = new Intent();
                    in.setClass(mContext, PublishServiceActivity.class);
                    in.putExtra("catid", mList.get(position).getCatid());
                    mContext.startActivity(in);
                }
            });

        }
        return convertView;
    }



    class ViewHolder {
        TextView mTextView;
    }

}












