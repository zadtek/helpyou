package com.zadetk.helpyou.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.bean.ServiceDetailsBean;
import com.zadetk.helpyou.view.NoScrollView;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: </p>
 *
 * @author Xirtam
 * <p>CreateDate: 2018.07.24 15:08<p>
 * @version 1.0
 */
public class ServiceDetailsAdapter extends RecyclerView.Adapter<ServiceDetailsAdapter.ViewHolder> implements View.OnClickListener {

    private List<ServiceDetailsBean> recyclerItemList;
    private VoiceAdapter.OnItemClickListener mOnItemClickListener = null;
    //    private ViewHolder holder;
    private Context context;

    public ServiceDetailsAdapter(Context context, List<ServiceDetailsBean> recyclerItemList) {
        this.recyclerItemList = recyclerItemList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.voice_item2, parent, false);
        ViewHolder holder = new ViewHolder(view);
        view.setOnClickListener(this);
        return holder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final ServiceDetailsBean item = recyclerItemList.get(position);
        holder.name.setText(item.getUsername());
        holder.date.setText(item.getCommenttime());
        holder.content.setText(item.getContent());
        holder.rb_star.setRating(Float.parseFloat(item.getStarnum()));
        Glide.with(context).load(item.getAvatar()).into(holder.user_img);
        holder.gv_img.setAdapter(new BaseAdapter() {
            @Override
            public int getCount() {
                return item.getCommentimg() == null ? 0 : item.getCommentimg().size();
            }

            @Override
            public Object getItem(int i) {
                return item.getCommentimg().get(i);
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @Override
            public View getView(int i, View view, ViewGroup viewGroup) {
                ImageView v = (ImageView) View.inflate(context, R.layout.img, null);
                Glide.with(context).load(item.getCommentimg().get(i)).into(v);
                return null;
            }
        });
        //将position保存在itemView的Tag中，以便点击时进行获取
        holder.itemView.setTag(position);
    }

    @Override
    public int getItemCount() {
        return recyclerItemList == null ? 0 : recyclerItemList.size();
    }

    @Override
    public void onClick(View v) {
        if (mOnItemClickListener != null) {
            //注意这里使用getTag方法获取position
            mOnItemClickListener.onItemClick(v, (int) v.getTag());
        }
    }

    public void setOnItemClickListener(VoiceAdapter.OnItemClickListener listener) {
        this.mOnItemClickListener = listener;
    }

    public static interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        CircleImageView user_img;
        TextView name, date, content;
        SimpleRatingBar rb_star;
        NoScrollView gv_img;

        public ViewHolder(View itemView) {
            super(itemView);
            user_img = (CircleImageView) itemView.findViewById(R.id.user_img);
            name = (TextView) itemView.findViewById(R.id.name);
            date = (TextView) itemView.findViewById(R.id.date);
            content = (TextView) itemView.findViewById(R.id.content);
            rb_star = (SimpleRatingBar) itemView.findViewById(R.id.rb_star);
            gv_img = itemView.findViewById(R.id.gv_img);
        }
    }
}
