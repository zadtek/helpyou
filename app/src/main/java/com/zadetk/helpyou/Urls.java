package com.zadetk.helpyou;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: </p>
 *
 * @author Xirtam
 * <p>CreateDate: 2018.07.24 10:29<p>
 * @version 1.0
 */
public class Urls {
    /**
     * YuD
     */
    public static String backUrl = "http://helpu.zadtek.com/interface.php?m=";
    //搜索
    public static String search = backUrl + "search";
    //获取服务者详情
    public static String serverinfo = backUrl + "serverinfo";
    //获取服务者详情
    public static String book = backUrl + "book";
    //收藏
    public static String collect = backUrl + "collect";
    //取消收藏
    public static String collectDelete = backUrl + "collectDelete";
    //发布岗位接口
    public static String jobinsert = backUrl + "jobinsert";
    //发布服务
    public static String saveskilldetail = backUrl + "saveskilldetail";
    //启动页
    public static String start = backUrl + "start";
    //服务详情回显
    public static String skilldetail = backUrl + "skilldetail";
    //编辑服务
    public static String saveskill = backUrl + "saveskill";
    //版本更新
    public static String getversion = backUrl + "getversion";
    //完善信息
    public static String saveinfo = backUrl + "saveinfo";
    //岗位详情回显
    public static String jobdetail = backUrl + "jobdetail";
    //编辑岗位
    public static String savejob = backUrl + "savejob";
    //微信登录是否注册校验接口
    public static String isWxLogin = backUrl + "isWxLogin";
    //微信登录
    public static String wxlogin = backUrl + "wxlogin";
    //评价页信息
    public static String commentinfo = backUrl + "commentinfo";
    //评价内容提交
    public static String submitcomment = backUrl + "submitcomment";
}
