package com.zadetk.helpyou.im;

import android.os.Bundle;


import com.zadetk.helpyou.R;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

/**
 *
 */
public class ChatListActivity extends BaseActivity{
    private ConversationListFragment chatFragment;

    @Override
    protected void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        EventBus.getDefault().register(this);
        setContentView(R.layout.em_activity_chat);
        chatFragment = new ConversationListFragment();
        chatFragment.setArguments(getIntent().getExtras());
        getSupportFragmentManager().beginTransaction().add(R.id.container, chatFragment).commit();
    }

    @Subscribe
    public void onEventMainThread(final EventMsg event) {
        try {
            chatFragment.refresh();
        } catch ( Exception e){
            e.printStackTrace();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }
}
