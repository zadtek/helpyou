package com.zadetk.helpyou.utils;

/**
 * Created by zack on 2018/5/21.
 */

import android.content.Context;
import android.os.Environment;

import java.io.File;


/**
 * Created by zack on 2018/5/21.
 */

public class PathUtils {
    public static String getFilePath(Context context, String dir) {
        String dirFilePath = "";
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())) {
            //SD卡有用
            dirFilePath = context.getExternalFilesDir(dir).getAbsolutePath();
        } else {
            //SD卡没有用
            dirFilePath = context.getFilesDir() + File.separator + dir;
        }
        File file = new File(dirFilePath);
        if (!file.exists()) {
            file.mkdirs();
        }
        return dirFilePath;
    }
}

