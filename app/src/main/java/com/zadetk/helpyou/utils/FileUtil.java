package com.zadetk.helpyou.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.MessageDigest;

/**
 * Created by zack on 2018/5/21.
 */

public class FileUtil {
    private static final int BITMAP_BYTE_SIZE = 512 * 1024;// *2 or * 10

    /**
     * 适用于上G大的文件
     */
    public static String getFileSha1(File file) {
        FileInputStream in = null;
        try {
            in = new FileInputStream(file);
            MessageDigest messagedigest = MessageDigest.getInstance("SHA-1");
            byte[] buffer = new byte[BITMAP_BYTE_SIZE];
            int len = 0;
            while ((len = in.read(buffer)) > 0) {
                //该对象通过使用 update()方法处理数据
                messagedigest.update(buffer, 0, len);
            }
            //对于给定数量的更新数据，digest 方法只能被调用一次。在调用 digest 之后，MessageDigest 对象被重新设置成其初始状态。
            return bytesToHexString(messagedigest.digest());
        } catch (Throwable e) {
            e.printStackTrace();
        } finally {
            if (in != null) {
                try {
                    in.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * byte[] 数组转换为16进制字符串
     *
     * @param src
     * @return
     */
    public static String bytesToHexString(byte[] src) {
        StringBuilder stringBuilder = new StringBuilder("");
        if (src == null || src.length <= 0) {
            return null;
        }
        for (int i = 0; i < src.length; i++) {
            int v = src[i] & 0xFF;
            String hv = Integer.toHexString(v);
            if (hv.length() < 2) {
                stringBuilder.append(0);
            }
            stringBuilder.append(hv);
        }
        return stringBuilder.toString();
    }

    /**
     * 获取已经下载到内部cache文件夹的文件，此方法用处有限!!!
     */
    public static File getDownloaded(Context context, String url) {
        if (context == null || TextUtils.isEmpty(url)) return null;
        if (!url.contains("/")) return null;
        final int indexOf = url.lastIndexOf('/');
        if (indexOf <= 0) return null;
        final String name = url.substring(indexOf + 1);
        System.out.println(name);
        final File cacheDir = context.getCacheDir();
        return new File(cacheDir, name);
    }

    /**
     * 获取某路径下可读取的文件大小
     *
     * @param f 文件路径
     * @return
     * @throws Exception
     * @date FionaLiu 2015年7月24日 下午1:50:41
     */
    public static long getFileSize(File f) throws Exception {
        long s = 0;
        if (f.exists()) {
            //FileInputStream fis = new FileInputStream(f);
            s = (new FileInputStream(f)).available();
        } else {
            f.createNewFile();
            //			System.out.println("文件不存在");
            LogUtil.d("文件不存在");
        }
        return s;
    }


    public static long getFolderSize(File file) throws Exception {
        long size = 0;
        try {
            File[] fileList = file.listFiles();
            for (int i = 0; i < fileList.length; i++) {
                // 如果下面还有文件
                if (fileList[i].isDirectory()) {
                    size = size + getFolderSize(fileList[i]);
                } else {
                    size = size + fileList[i].length();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return size;
    }

    /**
     * 删除路径下的所有文件即包括其子目录文件
     *
     * @param filename
     * @return
     * @date FionaLiu 2015年7月24日 下午1:50:26
     */
    public static boolean deleteFile(String filename) {
        File file = new File(filename);
        return deleteFile(file);
    }

    /**
     * 删除路径下的所有文件即包括其子目录的文件
     * 函数名称 : deleteFoder<br>
     * 功能描述 : 删除文件 <p>
     * 参数及返回值说明：<br>
     *
     * @param file
     * @return boolean<br>
     * @date FionaLiu
     */
    public static boolean deleteFile(File file) {
        boolean isSuccess = false;
        if (file.exists()) { // 判断文件是否存在
            if (file.isFile()) { // 判断是否是文件
                isSuccess = file.delete(); // delete()方法 你应该知道 是删除的意思;
            } else if (file.isDirectory()) { // 否则如果它是一个目录
                File files[] = file.listFiles(); // 声明目录下所有的文件 files[];
                if (files != null) {
                    if (1 > files.length) {
                        isSuccess = file.delete();
                    } else {
                        for (int i = 0; i < files.length; i++) { // 遍历目录下所有的文件
                            deleteFile(files[i]); // 把每个文件 用这个方法进行迭代
                        }
                    }
                }
            }
            file.delete();
        }
        return isSuccess;
    }

    /**
     * 保存图片到本应用下
     **/
    public static void savePicture(Activity a, String fileName, Bitmap bitmap) {

        FileOutputStream fos = null;
        try {//直接写入名称即可，没有会被自动创建；私有：只有本应用才能访问，重新内容写入会被覆盖
            fos = a.openFileOutput(fileName, Context.MODE_PRIVATE);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);// 把图片写入指定文件夹中

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (null != fos) {
                    fos.close();
                    fos = null;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * 保存bitmap到SD卡
     *
     * @param bitmap
     * @param path
     */
    public static String saveBitmapToSDCard(Bitmap bitmap, String path) {
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(path);
            if (fos != null) {
                bitmap.compress(Bitmap.CompressFormat.PNG, 90, fos);
                fos.close();
            }
            return path;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * @param fromPath 被复制的文件路径
     * @param toPath   复制的目录文件路径
     * @param rewrite  是否重新创建文件
     *                 <p>文件的复制操作方法
     */
    public static void copyfile(String fromPath, String toPath, Boolean rewrite) {

        File fromFile = new File(fromPath);
        File toFile = new File(toPath);

        if (!fromFile.exists()) {
            return;
        }
        if (!fromFile.isFile()) {
            return;
        }
        if (!fromFile.canRead()) {
            return;
        }
        if (!toFile.getParentFile().exists()) {
            toFile.getParentFile().mkdirs();
        }
        if (toFile.exists() && rewrite) {
            toFile.delete();
        }
        try {
            FileInputStream fosfrom = new FileInputStream(fromFile);
            FileOutputStream fosto = new FileOutputStream(toFile);

            byte[] bt = new byte[1024];
            int c;
            while ((c = fosfrom.read(bt)) > 0) {
                fosto.write(bt, 0, c);
            }
            //关闭输入、输出流
            fosfrom.close();
            fosto.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 保存二维码图片
     *
     * @param filePath 保存图片的路径
     * @param fileName 保存图片的名字
     * @param bm       保存图片的资源
     * @author 连灿平
     */
    public static void saveBitmap(Context context, String filePath, String fileName, Bitmap bm) {
        File f = new File(filePath, fileName);
        try {
            FileOutputStream out = new FileOutputStream(f);
            bm.compress(Bitmap.CompressFormat.JPEG, 90, out);
            out.flush();
            out.close();
            ToastUtil.showToast("保存成功！");
        } catch (FileNotFoundException e) {
            ToastUtil.showToast("保存失败！");
            e.printStackTrace();
        } catch (IOException e) {
            ToastUtil.showToast("保存失败！");
            e.printStackTrace();
        }
    }

}
