package com.zadetk.helpyou.utils;

import android.widget.Toast;

import com.zadetk.helpyou.App;


/**
 * Created by Administrator on 2018/2/28.
 */

public class ToastUtil {
    private static Toast toast;

    public static void showToast(String text) {
        if (toast == null) {
            toast = Toast.makeText(App.getApplication(), text, Toast.LENGTH_SHORT);
        } else {
            toast.setText(text);//如果不为空，则直接改变当前toast的文本
        }
        toast.show();
    }

    public static void cancelToast() {
       try{
           toast.cancel();
           toast.cancel();
       } catch (Exception e){
           e.printStackTrace();
       }
    }
}
