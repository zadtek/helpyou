package com.zadetk.helpyou.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.support.v4.content.FileProvider;

import com.zadetk.helpyou.R;

import java.io.File;

/**
 * 内容摘要	：获取应用版本信息工具类<p>
 * <p>
 * 作者	：连灿平
 * 当前版本号：V1.0.0Beta<br>
 * 历史记录	:<br>
 * 日期	: 2014年8月29日 下午1:44:05 	修改人：连灿平<br>
 * 描述	:<br>
 **/
public class VersionUtils {
    /**
     * 函数名称 : getAppVersionCode<br>
     * 功能描述 :  返回当前程序版本名VersionCode<p>
     * 参数及返回值说明：<br>
     *
     * @param context
     * @return int<br>
     */
    public static int getAppVersionCode(Context context) {
        //		String versionName = "";
        int versioncode = 0;
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            //			versionName = pi.versionName;
            versioncode = pi.versionCode;
            //			if (versionName == null || versionName.length() <= 0) {
            //				return "";
            //			}
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.d("VersionInfo Exception" + e);
        }
        return versioncode;
    }

    /**
     * 函数名称 : getAppVersionName<br>
     * 功能描述 :  返回当前程序版本名versionName<p>
     * 参数及返回值说明：<br>
     *
     * @param context
     * @return int<br>
     */
    public static String getAppVersionName(Context context) {
        String versionName = "";
        try {
            // ---get the package info---
            PackageManager pm = context.getPackageManager();
            PackageInfo pi = pm.getPackageInfo(context.getPackageName(), 0);
            versionName = pi.versionName;
        } catch (Exception e) {
            e.printStackTrace();
            LogUtil.d("VersionInfo Exception" + e);
        }
        return versionName;
    }

    /**
     * 函数名称 : getAppVersionCode<br>
     * 功能描述 :  返回APK代码版本号VersionCode(非目前使用的APK哦，是路径存放的APK哦)<p>
     * 参数及返回值说明：<br>
     *
     * @param context
     * @param filePath
     * @return int<br>
     */
    public static int getAppVersionCode(Context context, String filePath) {
        PackageManager pm = context.getPackageManager();
        PackageInfo info = pm.getPackageArchiveInfo(filePath,
                PackageManager.GET_ACTIVITIES);
        int versionCode = 0;
        if (info != null) {
            //			ApplicationInfo appInfo = info.applicationInfo;
            //			String appName = pm.getApplicationLabel(appInfo).toString();
            //			String packageName = appInfo.packageName; //得到安装包名称
            versionCode = info.versionCode; //得到版本信息
        }
        return versionCode;
    }

    /**
     * 函数名称 : getAppVersionName<br>
     * 功能描述 :  返回APK代码版本号versionName(非目前使用的APK哦，是路径存放的APK哦)<p>
     * 参数及返回值说明：<br>
     *
     * @param context
     * @param filePath
     * @return String<br>
     */
    public static String getAppVersionName(Context context, String filePath) {
        PackageManager pm = context.getPackageManager();
        PackageInfo info = pm.getPackageArchiveInfo(filePath,
                PackageManager.GET_ACTIVITIES);
        String versionName = "";
        if (info != null) {
            //			ApplicationInfo appInfo = info.applicationInfo;
            //			String appName = pm.getApplicationLabel(appInfo).toString();
            //			String packageName = appInfo.packageName; //得到安装包名称
            //			versionCode = info.versionCode; //得到版本信息
            versionName = info.versionName;
        }
        return versionName;
    }

    /**
     * 函数名称 : installApk<br>
     * 功能描述 : 安装apk <p>
     * 参数及返回值说明：<br>
     *
     * @param path
     * @return void<br>
     */
    public static void installApk(Context context, String path) {
        // 通过Intent安装APK文件
        Intent intent = new Intent();
        intent.setAction("wangs.intent.action.INSTALL_PACKAGE");
        intent.setDataAndType(Uri.parse("file://" + path),
                "application/vnd.android.package-archive");
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("auto_start", true);
        context.startActivity(intent);
    }

    /**
     * 安装apk
     *
     * @param context
     * @param file
     */
    public static void installApk(Context context, File file) {
        Intent intent = new Intent(Intent.ACTION_VIEW);
        Uri data;
        // 判断版本大于等于7.0
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            // "net.csdn.blog.ruancoder.fileprovider"即是在清单文件中配置的authorities
            data = FileProvider.getUriForFile(context, "com.zadtek.helpyou.fileprovider", file);
            // 给目标应用一个临时授权
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        } else {
            data = Uri.fromFile(file);
        }
        intent.setDataAndType(data, "application/vnd.android.package-archive");
        context.startActivity(intent);
    }

    /**
     * 函数名称 : getAndroidSDKVersion
     * 功能描述 : 获取当前SDK版本 参数及返回值说明：
     *
     * @return
     */
    public static int getAndroidSDKVersion() {
        int version = 0;
        try {
            version = Integer.valueOf(Build.VERSION.SDK);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        return version;
    }

    /**
     * 判断桌面是否已添加快捷方式
     *
     * @param cx 快捷方式名称
     * @return
     */
    public static boolean hasShortcut(Context cx) {

        boolean isInstallShortcut = false;
        final ContentResolver cr = cx.getContentResolver();
        final String AUTHORITY = "com.android.launcher.settings";
        final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
                + "/favorites?notify=true");
        Cursor c = cr.query(CONTENT_URI,
                new String[]{"title", "iconResource"}, "title=?",
                new String[]{cx.getString(R.string.app_name).trim()}, null);
        if (c != null && c.getCount() > 0) {
            isInstallShortcut = true;
        }
        return isInstallShortcut;
        // ==================================
        // boolean result = false;
        // // 获取当前应用名称
        // String title = null;
        // try {
        // final PackageManager pm = cx.getPackageManager();
        // title = pm.getApplicationLabel(
        // pm.getApplicationInfo(cx.getPackageName(),
        // PackageManager.GET_META_DATA)).toString();
        // } catch (Exception e) {
        // e.printStackTrace();
        // }
        //
        // final String uriStr;
        // if (android.os.Build.VERSION.SDK_INT < 8) {
        // uriStr =
        // "content://com.android.launcher.settings/favorites?notify=true";
        // } else {
        // uriStr =
        // "content://com.android.launcher2.settings/favorites?notify=true";
        // }
        // final Uri CONTENT_URI = Uri.parse(uriStr);
        // final Cursor c = cx.getContentResolver().query(CONTENT_URI, null,
        // "title=?", new String[] { title }, null);
        // if (c != null && c.getCount() > 0) {
        // result = true;
        // }
        // return result;
    }

    /**
     * 为当前应用添加桌面快捷方式
     *
     * @param cx 快捷方式名称
     */
    public static void addShortcut(Context cx) {
        Intent shortcut = new Intent(
                "com.android.launcher.action.INSTALL_SHORTCUT");

        Intent shortcutIntent = cx.getPackageManager()
                .getLaunchIntentForPackage(cx.getPackageName());
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        // 快捷方式名称
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, getAPKName(cx));
        // 不允许重复创建（不一定有效）
        shortcut.putExtra("duplicate", false);// 经测试不是根据快捷方式的名字判断重复的
        // 应该是根据快链的Intent来判断是否重复的,即Intent.EXTRA_SHORTCUT_INTENT字段的value
        // 但是名称不同时，虽然有的手机系统会显示Toast提示重复，仍然会建立快链
        // 屏幕上没有空间时会提示
        // 注意：重复创建的行为MIUI和三星手机上不太一样，小米上似乎不能重复创建快捷方式

        // 快捷方式的图标
        Parcelable iconResource = Intent.ShortcutIconResource.fromContext(cx,
                R.mipmap.ic_launcher);
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE, iconResource);

        cx.sendBroadcast(shortcut);
    }

    /**
     * 获取当前应用名称
     *
     * @param context
     * @return
     */
    public static String getAPKName(Context context) {
        // 获取当前应用名称
        String title = null;
        try {
            final PackageManager pm = context.getPackageManager();
            title = pm.getApplicationLabel(
                    pm.getApplicationInfo(context.getPackageName(),
                            PackageManager.GET_META_DATA)).toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return title;
    }

    /**
     * 删除当前应用的桌面快捷方式
     *
     * @param cx
     */
    public static void delShortcut(Context cx) {
        Intent shortcut = new Intent(
                "com.android.launcher.action.UNINSTALL_SHORTCUT");
        // 快捷方式名称
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_NAME, getAPKName(cx));
        // 设置关联程序
        Intent shortcutIntent = cx.getPackageManager()
                .getLaunchIntentForPackage(cx.getPackageName());
        shortcut.putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
        // 发送广播
        cx.sendBroadcast(shortcut);
    }

}
