package com.zadetk.helpyou.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;

import java.util.List;
import java.util.Map;

public class JsonUtils {

	public static <T> T json2Class(String json, Class<T> clazz) {
		return JSON.parseObject(json, clazz);
	}

	public static <T> List<T> json2List(String json, Class<T> clazz) {
		return  JSON.parseArray(json, clazz);
	}

	public static <T> List<T> jsonArr2List(JSONArray arr, Class<T> clazz) {
		return  JSON.parseArray(JSON.toJSONString(arr), clazz);
	}

	public static <T> String x2json(Object object) {
		return  JSON.toJSONString(object);
	}

	public static JSONObject string2json(String str) {
		return  JSON.parseObject(str);
	}

	@SuppressWarnings("unchecked")
	public static Map<String, Object> json2Map(String json) {
		return (Map<String, Object>) JSON.parse(json);
	}
}
