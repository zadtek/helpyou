package com.zadetk.helpyou.utils;


public class RegexUtils {
    public static boolean isPhone(String phone) {
        String telRegex = "^1\\d{10}$";
        return phone.matches(telRegex);
    }

    public static boolean isEmail(String phone) {
        String telRegex = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
        return phone.matches(telRegex);
    }
}
