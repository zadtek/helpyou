package com.zadetk.helpyou.net;

import android.content.Context;
import android.support.v4.util.ArrayMap;

import com.google.gson.Gson;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.SPUtil;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import okhttp3.FormBody;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import retrofit2.Retrofit;

/**
 * Created by Zackv on 2018/3/16.
 */

public class NetTool {
    public static final String BaseUrl = "http://helpu.zadtek.com/";
    private static Gson gson;
    private static NetTool netTool = null;
    private Api mAPI;

    private NetTool() {
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();

        httpClient.addInterceptor(new LogInterceptor());
        mAPI = new Retrofit.Builder()
                .client(httpClient.build())
                .baseUrl(BaseUrl)
                .addConverterFactory(CustomGsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Api.class);
    }

    private static NetTool getHelper() {
        if (netTool == null) {
            synchronized (NetTool.class) {
                if (netTool == null) {
                    netTool = new NetTool();
                }
            }
        }
        return netTool;
    }

    public static Api getApi() {
        return getHelper().mAPI;
    }

    public static Map<String, Object> newParams() {
        final Map<String, Object> params = new ArrayMap<String, Object>();
        //params.put("method", method);
        //params.put("app_key", "123123");
        return params;
    }

    public static Map<String, Object> newParams(Context context) {
        final Map<String, Object> params = new ArrayMap<String, Object>();
        int uid = (int) SPUtil.getData(context, Const.USER_UID, 0);
        String token = (String) SPUtil.getData(context, Const.USER_TOKEN, "");
        params.put("token", token);
        params.put("uid", uid);
        return params;
    }

    public static RequestBody buildUploadMultipart(File file, String key) {
        return new MultipartBody.Builder().setType(MultipartBody.FORM)
                .addFormDataPart(key, file.getName(), RequestBody.create(MediaType.parse("multipart/form-data"), file))
                .build();
    }

    private static class LogInterceptor implements Interceptor {
        @Override
        public Response intercept(Chain chain) throws IOException {
            Request request = chain.request();
            LogUtil.d2(request);
            if (request.body() instanceof FormBody) {
                FormBody form = (FormBody) request.body();
                for (int index = 0, size = form.size(); index < size; index++) {
                    LogUtil.d2(form.encodedName(index), form.encodedValue(index));
                }
            }
            Response response = chain.proceed(request);
            MediaType mediaType = response.body().contentType();
            String content = response.body().string();
            LogUtil.d(content);
            return response.newBuilder().body(ResponseBody.create(mediaType, content)).build();
        }
    }
}
