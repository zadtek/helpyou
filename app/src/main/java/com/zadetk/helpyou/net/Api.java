package com.zadetk.helpyou.net;


import com.zadetk.helpyou.bean.BookBean;
import com.zadetk.helpyou.bean.CollectBean;
import com.zadetk.helpyou.bean.CommentinfoBean;
import com.zadetk.helpyou.bean.CompleteserviceBean;
import com.zadetk.helpyou.bean.ConfirmorderBean;
import com.zadetk.helpyou.bean.DeleteimgBean;
import com.zadetk.helpyou.bean.DeletesjobBean;
import com.zadetk.helpyou.bean.DeleteskillBean;
import com.zadetk.helpyou.bean.FinishorderBean;
import com.zadetk.helpyou.bean.GetarticlelistBean;
import com.zadetk.helpyou.bean.GetimgBean;
import com.zadetk.helpyou.bean.HotservicecatBean;
import com.zadetk.helpyou.bean.IndexicoBean;
import com.zadetk.helpyou.bean.InfoItemBean;
import com.zadetk.helpyou.bean.IsWxLoginBean;
import com.zadetk.helpyou.bean.JobcatlistBean;
import com.zadetk.helpyou.bean.JobdetailBean;
import com.zadetk.helpyou.bean.JobinsertBean;
import com.zadetk.helpyou.bean.JoblistBean;
import com.zadetk.helpyou.bean.LocationBean;
import com.zadetk.helpyou.bean.LogoutResponseBean;
import com.zadetk.helpyou.bean.MyCollectResponseBean;
import com.zadetk.helpyou.bean.MyMoneyResponseBean;
import com.zadetk.helpyou.bean.MydemandBean;
import com.zadetk.helpyou.bean.OrderCancelBean;
import com.zadetk.helpyou.bean.OrdertomeBean;
import com.zadetk.helpyou.bean.RegsisterBean;
import com.zadetk.helpyou.bean.RobBean;
import com.zadetk.helpyou.bean.RoblistBean;
import com.zadetk.helpyou.bean.SavejobBean;
import com.zadetk.helpyou.bean.SaveskillBean;
import com.zadetk.helpyou.bean.SaveskilldetailBean;
import com.zadetk.helpyou.bean.SearchBean;
import com.zadetk.helpyou.bean.SearchservicecatBean;
import com.zadetk.helpyou.bean.SendBean;
import com.zadetk.helpyou.bean.ServerinfoBean;
import com.zadetk.helpyou.bean.ServicecatBean;
import com.zadetk.helpyou.bean.ServicetwocatBean;
import com.zadetk.helpyou.bean.SkilldetailBean;
import com.zadetk.helpyou.bean.SkilllistBean;
import com.zadetk.helpyou.bean.SubmitcommentBean;
import com.zadetk.helpyou.bean.SwitchMessageBean;
import com.zadetk.helpyou.bean.System_messageBean;
import com.zadetk.helpyou.bean.UpdateInfo;
import com.zadetk.helpyou.bean.UploadcommentimgBean;
import com.zadetk.helpyou.bean.UploadjobimgBean;
import com.zadetk.helpyou.bean.UploadkillimgBean;
import com.zadetk.helpyou.bean.UserInfoBean;
import com.zadetk.helpyou.bean.UserLoginBean;
import com.zadetk.helpyou.bean.WithDrawResponseBean;
import com.zadetk.helpyou.bean.WxloginBean;
import com.zadetk.helpyou.bean.WxpayBean;

import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

/**
 * Created by Zackv on 2018/3/16.
 */

public interface Api {

    @FormUrlEncoded
    @POST("interface.php?m=login")
    Observable<BaseResponse<UserLoginBean>> login(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=register")
    Observable<BaseResponse<RegsisterBean>> regsister(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=sendsms")
    Observable<BaseResponse<SendBean>> sendSmsCode(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=forget")
    Observable<BaseResponse<UserLoginBean>> resetPassword(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=userinfo")
    Observable<BaseResponse<UserInfoBean>> getUserInfo(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=saveinfo")
    Observable<BaseResponse<InfoItemBean>> saveUserInfo(@FieldMap Map<String, Object> params);

    @POST("interface.php?m=saveinfo")
    Observable<BaseResponse<InfoItemBean>> saveUserInfo(@Body RequestBody params);


    @FormUrlEncoded
    @POST("interface.php?m=mymoney")
    Observable<BaseResponse<MyMoneyResponseBean>> getMyMoney(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=mycollect")
    Observable<BaseResponse<MyCollectResponseBean>> getMyCollect(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=withdraw")
    Observable<BaseResponse<WithDrawResponseBean>> withDraw(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=logout")
    Observable<BaseResponse<LogoutResponseBean>> logout(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("test")
    Observable<BaseResponse<UpdateInfo>> checkUpdate(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=location")
    Observable<BaseResponse<LocationBean>> sendLocation(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=indexico")
    Observable<BaseResponse<List<IndexicoBean>>> getindexIco(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=roblist")
    Observable<BaseResponse<List<RoblistBean>>> getRoblist(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=mydemand")
    Observable<BaseResponse<List<MydemandBean>>> getMydemand(@FieldMap Map<String, Object> params);

    @FormUrlEncoded
    @POST("interface.php?m=ordertome")
    Observable<BaseResponse<List<OrdertomeBean>>> getOrdertome(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //点击“关于帮你”“用户指南”后获取文章列表接口
    @POST("interface.php?m=getarticlelist")
    Observable<BaseResponse<List<GetarticlelistBean>>> getGetarticlelist(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //系统消息
    @POST("interface.php?m=system_message")
    Observable<BaseResponse<List<System_messageBean>>> getSystem_message(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //该接口对应8-3工人详情页
    @POST("interface.php?m=serverinfo")
    Observable<BaseResponse<List<ServerinfoBean>>> getServerinfo(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //搜索接口  用户说出的语音转换为文本信息后，发送到此接口，返回搜索结果，在【8-2需求列表】这个页面显示结果
    @POST("interface.php?m=search")
    Observable<BaseResponse<List<SearchBean>>> getSearch(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //微信登录是否注册校验接口
    @POST("interface.php?m=isWxLogin")
    Observable<BaseResponse<List<IsWxLoginBean>>> getIsWxLogin(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //收藏
    @POST("interface.php?m=collect")
    Observable<BaseResponse<List<CollectBean>>> getCollect(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //微信登录接口
    @POST("interface.php?m=wxlogin")
    Observable<BaseResponse<List<WxloginBean>>> getWxlogin(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //发布岗位接口
    @POST("interface.php?m=jobinsert")
    Observable<BaseResponse<List<JobinsertBean>>> getJobinsert(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //发布岗位详情页图片上传，返回图片的URL地址和id，如果用户想删除图片，向接口上传图片id即可（删除和获取图片复用服务相关接口）
    @POST("interface.php?m=uploadjobimg")
    Observable<BaseResponse<List<UploadjobimgBean>>> getUploadjobimg(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //保存/新增服务技能
    @POST("interface.php?m=savejob")
    Observable<BaseResponse<List<SavejobBean>>> getSavejob(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //查看已发布的岗位详情
    @POST("interface.php?m=jobdetail")
    Observable<BaseResponse<List<JobdetailBean>>> getJobdetail(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //发布服务时点击搜索，下面显示的热门类别
    @POST("interface.php?m=jobcatlist")
    Observable<BaseResponse<List<JobcatlistBean>>> getJobcatlist(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //删除岗位
    @POST("interface.php?m=deletesjob")
    Observable<BaseResponse<List<DeletesjobBean>>> getDeletesjob(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //已发布岗位
    @POST("interface.php?m=joblist")
    Observable<BaseResponse<List<JoblistBean>>> getJoblist(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //发布服务时所要选择的服务类别
    @POST("interface.php?m=servicetwocat")
    Observable<BaseResponse<List<ServicetwocatBean>>> getServicetwocat(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //发布新的服务技能或者打开之前已经存在的服务进行编辑后保存
    @POST("interface.php?m=saveskilldetail")
    Observable<BaseResponse<SaveskilldetailBean>> getSaveskilldetail(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //输入关键词后搜索服务类别接口；选中某一项后，跳转到填写服务详情的页面，选中的分类id为一级分类id
    @POST("interface.php?m=searchservicecat")
    Observable<BaseResponse<List<List<SearchservicecatBean>>>> getSearchservicecat(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //发布服务时点击搜索，下面显示的热门类别
    @POST("interface.php?m=hotservicecat")
    Observable<BaseResponse<List<HotservicecatBean>>> getHotservicecat(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //发布服务时所要选择的服务类别
    @POST("interface.php?m=servicecat")
    Observable<BaseResponse<List<ServicecatBean>>> getServicecat(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //删除图片
    @POST("interface.php?m=deleteimg")
    Observable<BaseResponse<List<DeleteimgBean>>> getDeleteimg(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //传入图片的id，返回图片在服务器上的URL
    @POST("interface.php?m=getimg")
    Observable<BaseResponse<List<GetimgBean>>> getGetimg(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //服务详情页图片上传，返回图片的URL地址和id，如果用户想删除图片，向接口上传图片id即可
    @POST("interface.php?m=uploadkillimg")
    Observable<BaseResponse<List<UploadkillimgBean>>> getUploadkillimg(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //保存服务技能
    @POST("interface.php?m=saveskill")
    Observable<BaseResponse<SaveskillBean>> getSaveskill(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //查看服务技能详情
    @POST("interface.php?m=skilldetail")
    Observable<BaseResponse<SkilldetailBean>> getSkilldetail(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //删除服务技能
    @POST("interface.php?m=deleteskill")
    Observable<BaseResponse<DeleteskillBean>> getDeleteskill(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //个人中心，点击发布服务，首先列出我已经发布的服务（或者我的技能），该接口返回用户已经发布的服务
    @POST("interface.php?m=skilllist")
    Observable<BaseResponse<List<SkilllistBean>>> getskilllist(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //确认预约
    @POST("interface.php?m=book")
    Observable<BaseResponse<List<BookBean>>> getBook(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //确认接单
    @POST("interface.php?m=confirmorder")
    Observable<BaseResponse<List<ConfirmorderBean>>> getConfirmorder(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //支付金额
    @POST("interface.php?m=finishorder")
    Observable<BaseResponse<List<FinishorderBean>>> getfinishorder(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //抢单
    @POST("interface.php?m=rob")
    Observable<BaseResponse<List<RobBean>>> getRob(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //评价页信息
    @POST("interface.php?m=commentinfo")
    Observable<BaseResponse<List<CommentinfoBean>>> getCmmentinfo(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //用户评价内容提交
    @POST("interface.php?m=submitcomment")
    Observable<BaseResponse<List<SubmitcommentBean>>> getSubmitcomment(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //评价中图片上传
    @POST("interface.php?m=uploadcommentimg")
    Observable<BaseResponse<List<UploadcommentimgBean>>> getUploadcommentimg(@FieldMap Map<String, Object> params);


    @FormUrlEncoded  //点击完成服务,请求该接口
    @POST("interface.php?m=completeservice")
    Observable<BaseResponse<List<CompleteserviceBean>>> getCompleteservice(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //取消订单 用户注册接口
    @POST("interface.php?m=orderCancel")
    Observable<BaseResponse<List<OrderCancelBean>>> getOrderCancel(@FieldMap Map<String, Object> params);
    @FormUrlEncoded  //设置接收推送消息
    @POST("interface.php?m=switchmessage")
    Observable<BaseResponse<SwitchMessageBean>> getSwitchMessage(@FieldMap Map<String, Object> params);

    @FormUrlEncoded  //微信支付
    @POST("interface.php?m=wxpay")
    Observable<BaseResponse<List<WxpayBean>>> getwxpay(@FieldMap Map<String, Object> params);
}
