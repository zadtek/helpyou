package com.zadetk.helpyou.net;

/**
 * Created by Zackv on 2018/3/16.
 */

public class BaseResponse<T> {
    public int error_code;
    public String message;
    public T data;

    public int getCode() {
        return error_code;
    }

    public void setCode(int code) {
        this.error_code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
