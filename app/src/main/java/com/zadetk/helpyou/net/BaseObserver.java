package com.zadetk.helpyou.net;


import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.ToastUtil;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Zackv on 2018/4/24.
 */

public abstract class BaseObserver<T> implements Observer<T> {
    private DialogHelper dialogHelper;

    public BaseObserver(DialogHelper dialog) {
        dialogHelper = dialog;
    }

    public BaseObserver() {

    }

    @Override
    public void onSubscribe(Disposable d) {
        LogUtil.d("onSubscribe");
        if (dialogHelper != null) {
            dialogHelper.showLoadingProgress();
        }
    }

    @Override
    public void onNext(T value) {
        onData(value);
        if (dialogHelper != null) {
            dialogHelper.dismissLoadingProgress();
        }
    }

    public abstract void onData(T value);

    @Override
    public void onError(Throwable e) {
        ToastUtil.showToast("出错了！" + e.getLocalizedMessage());
        LogUtil.e("网络请求错误：" + e.getLocalizedMessage());
        if (dialogHelper != null) {
            dialogHelper.dismissLoadingProgress();
        }
    }

    @Override
    public void onComplete() {
        if (dialogHelper != null) {
            dialogHelper.dismissLoadingProgress();
        }
    }

    public interface DialogHelper {
        void showLoadingProgress();

        void dismissLoadingProgress();

        void showRetryView();
    }
}
