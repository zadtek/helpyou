package com.zadetk.helpyou.bean;

/**
 * Created by zack on 2018/5/17.
 */

public class UserLoginBean {
    private String username, token;
    private int uid;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
