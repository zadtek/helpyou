package com.zadetk.helpyou.bean;

/**
 * 传入图片的id，返回图片在服务器上的URL
 */
public class GetimgBean {
    private String imgurl;

    @Override
    public String toString() {
        return "GetimgBean{" +
                "imgurl='" + imgurl + '\'' +
                '}';
    }

    public String getImgurl() {
        return imgurl;
    }

    public void setImgurl(String imgurl) {
        this.imgurl = imgurl;
    }
}
