package com.zadetk.helpyou.bean;

/**
 *已发布岗位
 */
public class JoblistBean {
    private  String jobid;
    private  String jobtitle;

    @Override
    public String toString() {
        return "JoblistBean{" +
                "jobid=" + jobid +
                ", jobtitle='" + jobtitle + '\'' +
                '}';
    }

    public String getJobid() {
        return jobid;
    }

    public void setJobid(String jobid) {
        this.jobid = jobid;
    }

    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }
}
