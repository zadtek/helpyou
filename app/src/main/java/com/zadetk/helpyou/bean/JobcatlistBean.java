package com.zadetk.helpyou.bean;

/**
 * 发布服务时点击搜索，下面显示的热门类别
 */
public class JobcatlistBean {
    private String catid, catname;
    private int selected;

    @Override
    public String toString() {
        return "JobcatlistBean{" +
                "catid='" + catid + '\'' +
                ", catname='" + catname + '\'' +
                '}';
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public void setSelected(int selected) {
        this.selected = selected;
    }

    public int getSelected() {
        return selected;
    }
}
