package com.zadetk.helpyou.bean;

/**
 * Created by Zackv on 2018/5/14.
 */

public class VoiceBean {


    /**
     * siteid : 1
     * sitetitle : 黄师傅（浑南长白苏家屯）
     * sitedesc : 维修站点·500米
     * starnum : 4.5
     * commentnum : 647
     * photourl : http://tc.zadtek.com/aaa/data/gallery_album/original_img/58db8a08900cf.png
     */

    private int siteid;
    private String sitetitle;
    private String sitedesc;
    private double starnum;
    private int commentnum;
    private String photourl;

    public int getSiteid() {
        return siteid;
    }

    public void setSiteid(int siteid) {
        this.siteid = siteid;
    }

    public String getSitetitle() {
        return sitetitle;
    }

    public void setSitetitle(String sitetitle) {
        this.sitetitle = sitetitle;
    }

    public String getSitedesc() {
        return sitedesc;
    }

    public void setSitedesc(String sitedesc) {
        this.sitedesc = sitedesc;
    }

    public double getStarnum() {
        return starnum;
    }

    public void setStarnum(double starnum) {
        this.starnum = starnum;
    }

    public int getCommentnum() {
        return commentnum;
    }

    public void setCommentnum(int commentnum) {
        this.commentnum = commentnum;
    }

    public String getPhotourl() {
        return photourl;
    }

    public void setPhotourl(String photourl) {
        this.photourl = photourl;
    }
}
