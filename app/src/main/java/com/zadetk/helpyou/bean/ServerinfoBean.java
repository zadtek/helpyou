package com.zadetk.helpyou.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取服务者详情 该接口对应8-3工人详情页
 */
public class ServerinfoBean {

    private String errorCode;
    private Data data;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 参数名	类型	    说明
     * toptitle	string	顶部标题
     * siteid	int	该服务者的id
     * sitetitle	string	服务标题
     * sitedesc	string	服务点描述
     * status	int	工人与
     * response	string	按钮显示的文字(0-确认预约，1-等待确认，2-进行中,3-待支付)
     * starnum	string	星级
     * commentnum	int	评论数
     * price	string	价格
     * is_collect	int	0关注；1关注
     * lat	string	纬度
     * lnt	string	经度
     * icourl	string	在地图中的图标
     * mobile	string	工人手机号
     * address	string	地址
     * slideimg	string	地址下面的展示图片
     * username	string	评论者昵称
     * starnum	string	评论者所标记的星星数
     * commenttime	string	评论时间
     * content	string	评论内容
     * commentimg	string	评论中发布的照片
     */
    public ServerinfoBean() {
    }

    /**
     * @param data
     * @param errorCode
     */
    public ServerinfoBean(String errorCode, Data data) {
        super();
        this.errorCode = errorCode;
        this.data = data;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public class Commentlist {

        private String avatar;
        private String username;
        private String starnum;
        private String commenttime;
        private String content;
        private List<String> commentimg = null;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * No args constructor for use in serialization
         */
        public Commentlist() {
        }

        /**
         * @param content
         * @param username
         * @param commentimg
         * @param starnum
         * @param commenttime
         * @param avatar
         */
        public Commentlist(String avatar, String username, String starnum, String commenttime, String content, List<String> commentimg) {
            super();
            this.avatar = avatar;
            this.username = username;
            this.starnum = starnum;
            this.commenttime = commenttime;
            this.content = content;
            this.commentimg = commentimg;
        }

        public String getAvatar() {
            return avatar;
        }

        public void setAvatar(String avatar) {
            this.avatar = avatar;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getStarnum() {
            return starnum;
        }

        public void setStarnum(String starnum) {
            this.starnum = starnum;
        }

        public String getCommenttime() {
            return commenttime;
        }

        public void setCommenttime(String commenttime) {
            this.commenttime = commenttime;
        }

        public String getContent() {
            return content;
        }

        public void setContent(String content) {
            this.content = content;
        }

        public List<String> getCommentimg() {
            return commentimg;
        }

        public void setCommentimg(List<String> commentimg) {
            this.commentimg = commentimg;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    public class Data {

        private String siteid;
        private String toptitle;
        private String sitetitle;
        private String sitedesc;
        private String starnum;
        private String commentnum;
        private String price;
        private String isCollect;
        private String response;
        private String lat;
        private String lnt;
        private String mobile;
        private String address;
        private String slideimg;
        private List<Commentlist> commentlist = null;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * No args constructor for use in serialization
         */
        public Data() {
        }

        /**
         * @param sitedesc
         * @param commentlist
         * @param sitetitle
         * @param starnum
         * @param toptitle
         * @param isCollect
         * @param slideimg
         * @param response
         * @param commentnum
         * @param price
         * @param address
         * @param siteid
         * @param lnt
         * @param lat
         * @param mobile
         */
        public Data(String siteid, String toptitle, String sitetitle, String sitedesc, String starnum, String commentnum, String price, String isCollect, String response, String lat, String lnt, String mobile, String address, String slideimg, List<Commentlist> commentlist) {
            super();
            this.siteid = siteid;
            this.toptitle = toptitle;
            this.sitetitle = sitetitle;
            this.sitedesc = sitedesc;
            this.starnum = starnum;
            this.commentnum = commentnum;
            this.price = price;
            this.isCollect = isCollect;
            this.response = response;
            this.lat = lat;
            this.lnt = lnt;
            this.mobile = mobile;
            this.address = address;
            this.slideimg = slideimg;
            this.commentlist = commentlist;
        }

        public String getSiteid() {
            return siteid;
        }

        public void setSiteid(String siteid) {
            this.siteid = siteid;
        }

        public String getToptitle() {
            return toptitle;
        }

        public void setToptitle(String toptitle) {
            this.toptitle = toptitle;
        }

        public String getSitetitle() {
            return sitetitle;
        }

        public void setSitetitle(String sitetitle) {
            this.sitetitle = sitetitle;
        }

        public String getSitedesc() {
            return sitedesc;
        }

        public void setSitedesc(String sitedesc) {
            this.sitedesc = sitedesc;
        }

        public String getStarnum() {
            return starnum;
        }

        public void setStarnum(String starnum) {
            this.starnum = starnum;
        }

        public String getCommentnum() {
            return commentnum;
        }

        public void setCommentnum(String commentnum) {
            this.commentnum = commentnum;
        }

        public String getPrice() {
            return price;
        }

        public void setPrice(String price) {
            this.price = price;
        }

        public String getIsCollect() {
            return isCollect;
        }

        public void setIsCollect(String isCollect) {
            this.isCollect = isCollect;
        }

        public String getResponse() {
            return response;
        }

        public void setResponse(String response) {
            this.response = response;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLnt() {
            return lnt;
        }

        public void setLnt(String lnt) {
            this.lnt = lnt;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getSlideimg() {
            return slideimg;
        }

        public void setSlideimg(String slideimg) {
            this.slideimg = slideimg;
        }

        public List<Commentlist> getCommentlist() {
            return commentlist;
        }

        public void setCommentlist(List<Commentlist> commentlist) {
            this.commentlist = commentlist;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}