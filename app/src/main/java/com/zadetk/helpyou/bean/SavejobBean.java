package com.zadetk.helpyou.bean;

/**
 * 保存/新增服务技能
 */
public class SavejobBean {
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    private String  status;
    private String  response;

    @Override
    public String toString() {
        return "SavejobBean{" +
                "status=" + status +
                ", response='" + response + '\'' +
                '}';
    }
}
