package com.zadetk.helpyou.bean;

/**
 * 发布岗位接口
 */
public class JobinsertBean {

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }


    private String  status;
    private String response;

    @Override
    public String toString() {
        return "JobinsertBean{" +
                "status=" + status +
                ", response='" + response + '\'' +
                '}';
    }
}
