package com.zadetk.helpyou.bean;

/**
 * Created by zack on 2018/5/17.
 */

public class InfoItemBean {
    private String key, value;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
