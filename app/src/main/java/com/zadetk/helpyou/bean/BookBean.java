package com.zadetk.helpyou.bean;

/**
 * 确认预约
 */
public class BookBean {
    private String response;

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
