package com.zadetk.helpyou.bean;

/**
 * 支付金额
 */
public class FinishorderBean {
    private String status,prompt;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }
}
