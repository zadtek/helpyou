package com.zadetk.helpyou.bean;

/**
 * 微信登录是否注册校验接口
 */
public class IsWxLoginBean {
    private String error_code;

    public String getError_code() {
        return error_code;
    }

    public void setError_code(String error_code) {
        this.error_code = error_code;
    }
}
