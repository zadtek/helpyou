package com.zadetk.helpyou.bean;

/**
 * 评价中图片上传
 */
public class UploadcommentimgBean {
    private String status,response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
