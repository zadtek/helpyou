package com.zadetk.helpyou.bean;

import java.util.List;

/**
 * Created by zack on 2018/5/17.
 */

public class MyMoneyResponseBean {
    private String money;
    private List<MoneyItemBean> detail;

    public String getMoney() {
        return money;
    }

    public void setMoney(String money) {
        this.money = money;
    }

    public List<MoneyItemBean> getMoneyItemBeans() {
        return detail;
    }

    public void setMoneyItemBeans(List<MoneyItemBean> detail) {
        this.detail = detail;
    }
}
