package com.zadetk.helpyou.bean;

public class MydemandBean {

    String order_id;
    String order_name;
    String order_amount;
    int order_status;
    String add_time;
    String distance;
    String worker_addr;
    String worker_tel;
    String order_sn;
    String worker_id;

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public String getOrder_name() {
        return order_name;
    }

    public void setOrder_name(String order_name) {
        this.order_name = order_name;
    }

    public String getOrder_amount() {
        return order_amount;
    }

    public void setOrder_amount(String order_amount) {
        this.order_amount = order_amount;
    }

    public int getOrder_status() {
        return order_status;
    }

    public void setOrder_status(int order_status) {
        this.order_status = order_status;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getWorker_addr() {
        return worker_addr;
    }

    public void setWorker_addr(String worker_addr) {
        this.worker_addr = worker_addr;
    }

    public String getWorker_tel() {
        return worker_tel;
    }

    public void setWorker_tel(String worker_tel) {
        this.worker_tel = worker_tel;
    }

    public String getOrder_sn() {
        return order_sn;
    }

    public void setOrder_sn(String order_sn) {
        this.order_sn = order_sn;
    }

    public String getWorker_id() {
        return worker_id;
    }

    public void setWorker_id(String worker_id) {
        this.worker_id = worker_id;
    }

    @Override
    public String toString() {
        return "MydemandBean{" +
                "order_id='" + order_id + '\'' +
                ", order_name='" + order_name + '\'' +
                ", order_amount='" + order_amount + '\'' +
                ", order_status=" + order_status +
                ", add_time='" + add_time + '\'' +
                ", distance='" + distance + '\'' +
                ", worker_addr='" + worker_addr + '\'' +
                ", worker_tel='" + worker_tel + '\'' +
                ", order_sn='" + order_sn + '\'' +
                ", worker_id='" + worker_id + '\'' +
                '}';
    }
}