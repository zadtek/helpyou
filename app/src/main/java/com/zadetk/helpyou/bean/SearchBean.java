package com.zadetk.helpyou.bean;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 搜索接口  用户说出的语音转换为文本信息后，发送到此接口，返回搜索结果，在【8-2需求列表】这个页面显示结果
 */
public class SearchBean {

    private Integer errorCode;
    private Data data;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 参数名	类型	说明
     * words	string	语音识别结果
     * reponse	string	识别后的回复
     * listtitle	string	list总表头
     * siteid	int	list中每一项的id
     * <p>
     * |sitetitle |string |list中每一项的标题 |
     * |sitedesc |string |list中每一项表头下方文字 ,站点描述 |
     * |starnum |string |星级，可以是半颗星 |
     * |commentnum |int |评论数 |
     * |photourl |string |右侧头像路径 |
     */
    public SearchBean() {
    }

    /**
     * @param data
     * @param errorCode
     */
    public SearchBean(Integer errorCode, Data data) {
        super();
        this.errorCode = errorCode;
        this.data = data;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public class Item {

        private Integer siteid;
        private String sitetitle;
        private String sitedesc;
        private Double starnum;
        private Integer commentnum;
        private String photourl;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * No args constructor for use in serialization
         */
        public Item() {
        }

        /**
         * @param commentnum
         * @param sitedesc
         * @param photourl
         * @param starnum
         * @param sitetitle
         * @param siteid
         */
        public Item(Integer siteid, String sitetitle, String sitedesc, Double starnum, Integer commentnum, String photourl) {
            super();
            this.siteid = siteid;
            this.sitetitle = sitetitle;
            this.sitedesc = sitedesc;
            this.starnum = starnum;
            this.commentnum = commentnum;
            this.photourl = photourl;
        }

        public Integer getSiteid() {
            return siteid;
        }

        public void setSiteid(Integer siteid) {
            this.siteid = siteid;
        }

        public String getSitetitle() {
            return sitetitle;
        }

        public void setSitetitle(String sitetitle) {
            this.sitetitle = sitetitle;
        }

        public String getSitedesc() {
            return sitedesc;
        }

        public void setSitedesc(String sitedesc) {
            this.sitedesc = sitedesc;
        }

        public Double getStarnum() {
            return starnum;
        }

        public void setStarnum(Double starnum) {
            this.starnum = starnum;
        }

        public Integer getCommentnum() {
            return commentnum;
        }

        public void setCommentnum(Integer commentnum) {
            this.commentnum = commentnum;
        }

        public String getPhotourl() {
            return photourl;
        }

        public void setPhotourl(String photourl) {
            this.photourl = photourl;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

    public class Data {

        private String words;
        private String reponse;
        private String listtitle;
        private List<Item> item = null;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * No args constructor for use in serialization
         */
        public Data() {
        }

        /**
         * @param listtitle
         * @param reponse
         * @param words
         * @param item
         */
        public Data(String words, String reponse, String listtitle, List<Item> item) {
            super();
            this.words = words;
            this.reponse = reponse;
            this.listtitle = listtitle;
            this.item = item;
        }

        public String getWords() {
            return words;
        }

        public void setWords(String words) {
            this.words = words;
        }

        public String getReponse() {
            return reponse;
        }

        public void setReponse(String reponse) {
            this.reponse = reponse;
        }

        public String getListtitle() {
            return listtitle;
        }

        public void setListtitle(String listtitle) {
            this.listtitle = listtitle;
        }

        public List<Item> getItem() {
            return item;
        }

        public void setItem(List<Item> item) {
            this.item = item;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }
}