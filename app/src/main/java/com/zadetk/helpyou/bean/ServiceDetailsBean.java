package com.zadetk.helpyou.bean;

import java.util.List;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2018</p>
 * <p>Company: </p>
 *
 * @author Xirtam
 * <p>CreateDate: 2018.07.24 14:26<p>
 * @version 1.0
 */
public class ServiceDetailsBean {

    /**
     * avatar : http://wx.qlogo.cn/mmopen/g3MonUZtNHkdmzicIlibx6iaFqAc56vxLSUfpb6n5WKSYVY0ChQKkiaJSgQ1dZuTOgvLLrhJbERQQ4eMsv84eavHiaiceqxibJxCfHe/0
     * username : 薄荷微凉
     * starnum : 1
     * commenttime : 2018-06-07
     * content : adsgasdg
     * commentimg : ["http://helpu.zadtek.com/images/comment_pic/15283375547654.jpg","http://helpu.zadtek.com/images/comment_pic/15283375542487.jpg","http://helpu.zadtek.com/images/comment_pic/15283375544308.jpg","http://helpu.zadtek.com/images/comment_pic/15283375546524.jpg"]
     */

    private String avatar;
    private String username;
    private String starnum;
    private String commenttime;
    private String content;
    private List<String> commentimg;

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getStarnum() {
        return starnum;
    }

    public void setStarnum(String starnum) {
        this.starnum = starnum;
    }

    public String getCommenttime() {
        return commenttime;
    }

    public void setCommenttime(String commenttime) {
        this.commenttime = commenttime;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public List<String> getCommentimg() {
        return commentimg;
    }

    public void setCommentimg(List<String> commentimg) {
        this.commentimg = commentimg;
    }
}
