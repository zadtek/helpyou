package com.zadetk.helpyou.bean;

import java.util.HashMap;
import java.util.Map;

/**
 * 获取用户经纬度
 */
public class LocationBean {

    private Integer errorCode;
    private Data data;
    private Map<String, Object> additionalProperties = new HashMap<String, Object>();

    /**
     * 参数名	类型	说明
     * lat	string	纬度
     * lng	string	经度
     */
    public LocationBean() {
    }

    /**
     * @param data
     * @param errorCode
     */
    public LocationBean(Integer errorCode, Data data) {
        super();
        this.errorCode = errorCode;
        this.data = data;
    }

    public Integer getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(Integer errorCode) {
        this.errorCode = errorCode;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public Map<String, Object> getAdditionalProperties() {
        return this.additionalProperties;
    }

    public void setAdditionalProperty(String name, Object value) {
        this.additionalProperties.put(name, value);
    }

    public class Data {

        private String lat;
        private String lng;
        private Map<String, Object> additionalProperties = new HashMap<String, Object>();

        /**
         * No args constructor for use in serialization
         */
        public Data() {
        }

        /**
         * @param lng
         * @param lat
         */
        public Data(String lat, String lng) {
            super();
            this.lat = lat;
            this.lng = lng;
        }

        public String getLat() {
            return lat;
        }

        public void setLat(String lat) {
            this.lat = lat;
        }

        public String getLng() {
            return lng;
        }

        public void setLng(String lng) {
            this.lng = lng;
        }

        public Map<String, Object> getAdditionalProperties() {
            return this.additionalProperties;
        }

        public void setAdditionalProperty(String name, Object value) {
            this.additionalProperties.put(name, value);
        }

    }

}