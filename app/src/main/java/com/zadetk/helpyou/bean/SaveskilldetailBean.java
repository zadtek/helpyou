package com.zadetk.helpyou.bean;

/**
 * 发布新的服务技能或者打开之前已经存在的服务进行编辑后保存
 */
public class SaveskilldetailBean {
    private String status;
    private String response;

    @Override
    public String toString() {
        return "SaveskilldetailBean{" +
                "status='" + status + '\'' +
                ", response='" + response + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
