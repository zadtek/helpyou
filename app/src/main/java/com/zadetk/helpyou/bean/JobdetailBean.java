package com.zadetk.helpyou.bean;

/**
 * 查看已发布的岗位详情
 */
public class JobdetailBean {
    private String  jobtitle;
    private String jobdesc;
    private String low;
    private String high;
    private String can_bargin;
    private String jobimg;
    private String job_cat;
    @Override
    public String toString() {
        return "JobdetailBean{" +
                "jobtitle='" + jobtitle + '\'' +
                ", jobdesc='" + jobdesc + '\'' +
                ", low='" + low + '\'' +
                ", high='" + high + '\'' +
                ", can_bargin='" + can_bargin + '\'' +
                ", jobimg='" + jobimg + '\'' +
                ", job_cat='" + job_cat + '\'' +
                '}';
    }


    public String getJobtitle() {
        return jobtitle;
    }

    public void setJobtitle(String jobtitle) {
        this.jobtitle = jobtitle;
    }

    public String getJobdesc() {
        return jobdesc;
    }

    public void setJobdesc(String jobdesc) {
        this.jobdesc = jobdesc;
    }

    public String getLow() {
        return low;
    }

    public void setLow(String low) {
        this.low = low;
    }

    public String getHigh() {
        return high;
    }

    public void setHigh(String high) {
        this.high = high;
    }

    public String getCan_bargin() {
        return can_bargin;
    }

    public void setCan_bargin(String can_bargin) {
        this.can_bargin = can_bargin;
    }

    public String getJobimg() {
        return jobimg;
    }

    public void setJobimg(String jobimg) {
        this.jobimg = jobimg;
    }

    public String getJob_cat() {
        return job_cat;
    }

    public void setJob_cat(String job_cat) {
        this.job_cat = job_cat;
    }



}
