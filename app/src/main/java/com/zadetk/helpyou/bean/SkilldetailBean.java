package com.zadetk.helpyou.bean;

/**
 * 查看服务技能详情
 */
public class SkilldetailBean {
    private String skilltitle ;
    private String skilldesc;
    private String skillcat;
    private String type;
    private String payway;
    private String amount;
    private String skillimg;


    public String getSkilltitle() {
        return skilltitle;
    }

    public void setSkilltitle(String skilltitle) {
        this.skilltitle = skilltitle;
    }

    public String getSkilldesc() {
        return skilldesc;
    }

    public void setSkilldesc(String skilldesc) {
        this.skilldesc = skilldesc;
    }

    public String getSkillcat() {
        return skillcat;
    }

    public void setSkillcat(String skillcat) {
        this.skillcat = skillcat;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPayway() {
        return payway;
    }

    public void setPayway(String payway) {
        this.payway = payway;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getSkillimg() {
        return skillimg;
    }

    public void setSkillimg(String skillimg) {
        this.skillimg = skillimg;
    }




}
