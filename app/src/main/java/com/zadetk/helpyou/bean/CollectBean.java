package com.zadetk.helpyou.bean;

/**
 * 收藏
 */
public class CollectBean {
    private String data;

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
