package com.zadetk.helpyou.bean;

/**
 * 发布服务时所要选择的服务类别
 */
public class ServicecatBean {
    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatico() {
        return catico;
    }

    public void setCatico(String catico) {
        this.catico = catico;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    private String catid,catico,catname;

    @Override
    public String toString() {
        return "ServicecatBean{" +
                "catid='" + catid + '\'' +
                ", catico='" + catico + '\'' +
                ", catname='" + catname + '\'' +
                '}';
    }
}
