package com.zadetk.helpyou.bean;

public class SearchSubBean {
    private String catid;
    private String catname;
    private String leval;

    @Override
    public String toString() {
        return "SearchSubBean{" +
                "catid='" + catid + '\'' +
                ", catname='" + catname + '\'' +
                ", leval='" + leval + '\'' +
                '}';
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    public String getLeval() {
        return leval;
    }

    public void setLeval(String leval) {
        this.leval = leval;
    }
}
