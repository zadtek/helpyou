package com.zadetk.helpyou.bean;

public class ServiceSub {
    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

    private String catid ;
    private String catname;
}
