package com.zadetk.helpyou.bean;

/**
 * 删除岗位
 */
public class DeletesjobBean {
    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    private String  data;

    @Override
    public String toString() {
        return "DeletesjobBean{" +
                "data='" + data + '\'' +
                '}';
    }
}
