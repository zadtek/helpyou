package com.zadetk.helpyou.bean;

/**
 * 个人中心，点击发布服务，首先列出我已经发布的服务（或者我的技能），该接口返回用户已经发布的服务
 */
public class SkilllistBean {
    public String getSkillid() {
        return skillid;
    }

    public void setSkillid(String skillid) {
        this.skillid = skillid;
    }

    public String getSkilltitle() {
        return skilltitle;
    }

    public void setSkilltitle(String skilltitle) {
        this.skilltitle = skilltitle;
    }

    @Override
    public String toString() {
        return "SkilllistBean{" +
                "skillid='" + skillid + '\'' +
                ", skilltitle='" + skilltitle + '\'' +
                '}';
    }

    private String skillid ;
    private String skilltitle ;

}
