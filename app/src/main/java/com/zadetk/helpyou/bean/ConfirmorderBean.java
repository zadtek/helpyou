package com.zadetk.helpyou.bean;

/**
 * 确认接单
 */
public class ConfirmorderBean {
    private String status,prompt;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }
}
