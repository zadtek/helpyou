package com.zadetk.helpyou.bean;

/**
 * 删除图片
 */
public class DeleteimgBean {
    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }

    @Override
    public String toString() {
        return "DeleteimgBean{" +
                "prompt='" + prompt + '\'' +
                '}';
    }

    private String prompt;
}
