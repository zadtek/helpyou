package com.zadetk.helpyou.bean;

/**
 * 点击“关于帮你”“用户指南”后获取文章列表接口
 */
public class GetarticlelistBean {


    private String articleid;
    private String articletitle,url;

    @Override
    public String toString() {
        return "GetarticlelistBean{" +
                "articleid=" + articleid +
                ", articletitle='" + articletitle + '\'' +
                ", url='" + url + '\'' +
                '}';
    }

    public String getArticleid() {
        return articleid;
    }

    public void setArticleid(String articleid) {
        this.articleid = articleid;
    }

    public String getArticletitle() {
        return articletitle;
    }

    public void setArticletitle(String articletitle) {
        this.articletitle = articletitle;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}