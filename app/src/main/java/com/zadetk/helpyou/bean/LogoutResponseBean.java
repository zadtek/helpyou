package com.zadetk.helpyou.bean;

/**
 * Created by zack on 2018/5/18.
 */

public class LogoutResponseBean {
    private int status;
    private String prompt;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }
}
