package com.zadetk.helpyou.bean;

import java.util.List;

/**
 * 输入关键词后搜索服务类别接口；选中某一项后，跳转到填写服务详情的页面，选中的分类id为一级分类id
 */
public class SearchservicecatBean {


    public String getLeval() {
        return leval;
    }

    public void setLeval(String leval) {
        this.leval = leval;
    }

    public String getCat_id() {
        return cat_id;
    }

    public void setCat_id(String cat_id) {
        this.cat_id = cat_id;
    }

    public String getCat_name() {
        return cat_name;
    }

    public void setCat_name(String cat_name) {
        this.cat_name = cat_name;
    }

    private String cat_id;
    private String cat_name;
    private String leval;
}
