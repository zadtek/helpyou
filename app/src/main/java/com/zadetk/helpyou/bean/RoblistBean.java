package com.zadetk.helpyou.bean;

public class RoblistBean {

    private String orderid;
    private String ordertitle;
    private String add_time;
    private String distance;
    private String custom_addr;
    private String custom_score;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getOrdertitle() {
        return ordertitle;
    }

    public void setOrdertitle(String ordertitle) {
        this.ordertitle = ordertitle;
    }


    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getCustomAddr() {
        return custom_addr;
    }

    public void setCustomAddr(String customAddr) {
        this.custom_addr = customAddr;
    }

    public String getAdd_time() {
        return add_time;
    }

    public void setAdd_time(String add_time) {
        this.add_time = add_time;
    }

    public String getCustom_score() {
        return custom_score;
    }

    public void setCustom_score(String custom_score) {
        this.custom_score = custom_score;
    }
}