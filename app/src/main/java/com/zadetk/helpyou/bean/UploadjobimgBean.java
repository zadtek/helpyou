package com.zadetk.helpyou.bean;

/**
 * -发布岗位详情页图片上传，返回图片的URL地址和id，如果用户想删除图片，向接口上传图片id即可（删除和获取图片复用服务相关接口）
 */
public class UploadjobimgBean {
    private String status, response,jobimgurl,imgid;

    @Override
    public String toString() {
        return "UploadjobimgBean{" +
                "status=" + status +
                ", imgid=" + imgid +
                ", response='" + response + '\'' +
                ", jobimgurl='" + jobimgurl + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImgid() {
        return imgid;
    }

    public void setImgid(String  imgid) {
        this.imgid = imgid;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getJobimgurl() {
        return jobimgurl;
    }

    public void setJobimgurl(String jobimgurl) {
        this.jobimgurl = jobimgurl;
    }

}
