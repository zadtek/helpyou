package com.zadetk.helpyou.bean;

/**
 * 首页地图中的小图标
 */
public class IndexicoBean {
    private String icourl;
    private String icolat;
    private String icolnt;
    private String icotype;

    public String getIcourl() {
        return icourl;
    }

    public void setIcourl(String icourl) {
        this.icourl = icourl;
    }

    public String getIcolat() {
        return icolat;
    }

    public void setIcolat(String icolat) {
        this.icolat = icolat;
    }

    public String getIcolnt() {
        return icolnt;
    }

    public void setIcolnt(String icolnt) {
        this.icolnt = icolnt;
    }

    public String getIcotype() {
        return icotype;
    }

    public void setIcotype(String icotype) {
        this.icotype = icotype;
    }

    @Override
    public String toString() {
        return "IndexicoBean{" +
                "icourl='" + icourl + '\'' +
                ", icolat='" + icolat + '\'' +
                ", icolnt='" + icolnt + '\'' +
                ", icotype='" + icotype + '\'' +
                '}';
    }
}