package com.zadetk.helpyou.bean;

/**
 * Created by zack on 2018/5/17.
 */

public class SendBean {
    private int flag;
    private String response;

    public int getFlag() {
        return flag;
    }

    public void setFlag(int flag) {
        this.flag = flag;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
