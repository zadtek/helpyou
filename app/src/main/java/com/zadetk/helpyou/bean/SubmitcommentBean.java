package com.zadetk.helpyou.bean;

/**
 * 用户评价内容提交
 */
public class SubmitcommentBean {
    private String status,response;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
