package com.zadetk.helpyou.bean;

/**
 * Created by zack on 2018/5/17.
 */

public class UserInfoBean {
    private String avatar, nickname, userphone, useraddress, useridcard_front, useridcard_back, companyname, companyphone, companyaddress, companyidcard_front, companyidcard_back;
    private int uid, startnum, ordernum, worknum, usersex;
    private String username;
    private String userToken;

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getUsersex() {
        return usersex;
    }

    public void setUsersex(int usersex) {
        this.usersex = usersex;
    }

    public String getUserphone() {
        return userphone;
    }

    public void setUserphone(String userphone) {
        this.userphone = userphone;
    }

    public String getUseraddress() {
        return useraddress;
    }

    public void setUseraddress(String useraddress) {
        this.useraddress = useraddress;
    }

    public String getUseridcard_front() {
        return useridcard_front;
    }

    public void setUseridcard_front(String useridcard_front) {
        this.useridcard_front = useridcard_front;
    }

    public String getUseridcard_back() {
        return useridcard_back;
    }

    public void setUseridcard_back(String useridcard_back) {
        this.useridcard_back = useridcard_back;
    }

    public String getCompanyphone() {
        return companyphone;
    }

    public void setCompanyphone(String companyphone) {
        this.companyphone = companyphone;
    }

    public String getCompanyaddress() {
        return companyaddress;
    }

    public void setCompanyaddress(String companyaddress) {
        this.companyaddress = companyaddress;
    }

    public String getCompanyidcard_front() {
        return companyidcard_front;
    }

    public void setCompanyidcard_front(String companyidcard_front) {
        this.companyidcard_front = companyidcard_front;
    }

    public String getCompanyidcard_back() {
        return companyidcard_back;
    }

    public void setCompanyidcard_back(String companyidcard_back) {
        this.companyidcard_back = companyidcard_back;
    }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public int getStartnum() {
        return startnum;
    }

    public void setStartnum(int startnum) {
        this.startnum = startnum;
    }

    public int getOrdernum() {
        return ordernum;
    }

    public void setOrdernum(int ordernum) {
        this.ordernum = ordernum;
    }

    public int getWorknum() {
        return worknum;
    }

    public void setWorknum(int worknum) {
        this.worknum = worknum;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getMobile() {
        return userphone;
    }

    public void setMobile(String mobile) {
        this.userphone = mobile;
    }

    public String getAddress() {
        return useraddress;
    }

    public void setAddress(String address) {
        this.useraddress = address;
    }

    public String getId_up() {
        return useridcard_front;
    }

    public void setId_up(String id_up) {
        this.useridcard_front = id_up;
    }

    public String getId_down() {
        return useridcard_back;
    }

    public void setId_down(String id_down) {
        this.useridcard_back = id_down;
    }

    public String getCompanyname() {
        return companyname;
    }

    public void setCompanyname(String companyname) {
        this.companyname = companyname;
    }

    public String getCompanytell() {
        return companyphone;
    }

    public void setCompanytell(String companytell) {
        this.companyphone = companytell;
    }

    public String getCompanyaddr() {
        return companyaddress;
    }

    public void setCompanyaddr(String companyaddr) {
        this.companyaddress = companyaddr;
    }
}
