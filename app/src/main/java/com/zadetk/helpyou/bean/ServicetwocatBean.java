package com.zadetk.helpyou.bean;

import android.app.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 发布服务时所要选择的服务类别
 */
public class ServicetwocatBean {
    private String catid ;
    private String catname;

    public List<ServiceSub> getLevel() {
        return level;
    }

    public void setLevel(List<ServiceSub> level) {
        this.level = level;
    }

    private List<ServiceSub> level;




    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }

}
