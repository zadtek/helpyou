package com.zadetk.helpyou.bean;

/**
 * 保存服务技能
 */
public class SaveskillBean {
    @Override
    public String toString() {
        return "SaveskillBean{" +
                "status='" + status + '\'' +
                ", response='" + response + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    private  String status;
    private  String response;
}
