package com.zadetk.helpyou.bean;

import java.util.List;

/**
 * Created by zack on 2018/5/18.
 */

public class MyCollectResponseBean {
    private List<CollectItemBean> item;

    public List<CollectItemBean> getItem() {
        return item;
    }

    public void setItem(List<CollectItemBean> item) {
        this.item = item;
    }
}
