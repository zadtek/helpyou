package com.zadetk.helpyou.bean;

/**
 *点击完成服务,请求该接口
 */
public class CompleteserviceBean {
    private String status,prompt;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }
}
