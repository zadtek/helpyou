package com.zadetk.helpyou.bean;

/**
 * 发布服务时点击搜索，下面显示的热门类别
 */
public class HotservicecatBean {
    private String catid;
    private String catname;

    @Override
    public String toString() {
        return "HotservicecatBean{" +
                "catid='" + catid + '\'' +
                ", catname='" + catname + '\'' +
                '}';
    }

    public String getCatid() {
        return catid;
    }

    public void setCatid(String catid) {
        this.catid = catid;
    }

    public String getCatname() {
        return catname;
    }

    public void setCatname(String catname) {
        this.catname = catname;
    }


}
