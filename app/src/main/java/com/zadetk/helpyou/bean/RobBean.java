package com.zadetk.helpyou.bean;

/**
 * 抢单
 */
public class RobBean {
    private String status,prompt;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }
}
