package com.zadetk.helpyou.bean;

/**
 * 服务详情页图片上传，返回图片的URL地址和id，如果用户想删除图片，向接口上传图片id即可
 */
public class UploadkillimgBean {
    private String status;
    private String response;
    private String imgid;
    private String skillimgurl;

    @Override
    public String toString() {
        return "UploadkillimgBean{" +
                "status='" + status + '\'' +
                ", response='" + response + '\'' +
                ", imgid='" + imgid + '\'' +
                ", skillimgurl='" + skillimgurl + '\'' +
                '}';
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public String getImgid() {
        return imgid;
    }

    public void setImgid(String imgid) {
        this.imgid = imgid;
    }

    public String getSkillimgurl() {
        return skillimgurl;
    }

    public void setSkillimgurl(String skillimgurl) {
        this.skillimgurl = skillimgurl;
    }


}
