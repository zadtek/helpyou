package com.zadetk.helpyou.bean;

/**
 * 取消订单 用户注册接口
 */
public class OrderCancelBean {
    private String status,prompt;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPrompt() {
        return prompt;
    }

    public void setPrompt(String prompt) {
        this.prompt = prompt;
    }
}
