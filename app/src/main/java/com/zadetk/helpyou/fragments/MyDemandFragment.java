package com.zadetk.helpyou.fragments;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.tencent.mm.opensdk.modelpay.PayReq;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.zadetk.helpyou.App;
import com.zadetk.helpyou.MessageEvent;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.activities.EvaluateActivity;
import com.zadetk.helpyou.base.AbsListFragment;
import com.zadetk.helpyou.bean.MydemandBean;
import com.zadetk.helpyou.bean.OrderCancelBean;
import com.zadetk.helpyou.bean.WxpayBean;
import com.zadetk.helpyou.im.ChatActivity;
import com.zadetk.helpyou.im.Constant;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.SPUtil;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zadetk.helpyou.view.dialog.BaseNiceDialog;
import com.zadetk.helpyou.view.dialog.NiceDialog;
import com.zadetk.helpyou.view.dialog.ViewConvertListener;
import com.zadetk.helpyou.view.dialog.ViewHolder;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.backUrl;
import static com.zadetk.helpyou.other.Const.USER_NAME;
import static com.zadetk.helpyou.other.Const.USER_UID;

/**
 * Created by Zackv on 2018/5/11.
 */

public class MyDemandFragment extends AbsListFragment {
    private static final String TAG = MyDemandFragment.class.getName();
    private MyDemandAdapter robAdapter;
    private List<MydemandBean> data = new LinkedList<>();
    private List<WxpayBean> data2 = new LinkedList<>();
    private NiceDialog niceDialog;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String error_code = bundle.getString("error_code");
            switch (msg.what) {
                case 0:
                    if (error_code.equals("0")) {
                        String appid = bundle.getString("appid");
                        String partnerid = bundle.getString("partnerid");
                        String prepayid = bundle.getString("prepayid");
                        String packages = bundle.getString("package");
                        String noncestr = bundle.getString("noncestr");
                        String timestamp = bundle.getString("timestamp");
                        String sign = bundle.getString("sign");
                        WxPay(appid, partnerid, prepayid, packages, noncestr, timestamp, sign);
                    }
                    //                            "appid": "wx071a16ef2a9e5d5d",
//                                    "partnerid": "1508265981",
//                                    "prepayid": "wx30154900030975c0d0bb39c72240365128",
//                                    "package": "Sign=WXPay",
//                                    "noncestr": "O1UpDnjRTSEN38Qo",
//                                    "timestamp": "1532936940",
//                                    "sign": "73C120560F28C0A1C5C94387555594A1"

                    break;
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EventBus.getDefault().register(this);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void Event(MessageEvent messageEvent) {
        if (messageEvent.getMessage() == "Hello EventBus!") {
            getMydemand();
        }


    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        // 注销订阅者
        EventBus.getDefault().unregister(this);
    }

    @Override
    protected void firstRequestData() {
        getMydemand();


    }


    @Override
    protected void loadMore() {

        robAdapter.loadMoreEnd();
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        robAdapter = new MyDemandAdapter(R.layout.myorder_item1, data);
        robAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent;
                switch (view.getId()) {
                    case R.id.tv_confirm:
                        break;
                    case R.id.tv_reject:
                        break;
                    case R.id.tv_complete:
                        break;
                    case R.id.tv_unpay:
                        break;
                    case R.id.tv_pay:
                        showdialog(data.get(position).getOrder_amount(), data.get(position).getOrder_sn());
//                        showdialog(data.get(position).);
                        break;
                    case R.id.tv_evaluate:
                        intent = new Intent(mActivity, EvaluateActivity.class);
                        intent.putExtra("orderid", data.get(position).getOrder_id());
                        startActivity(intent);
                        break;
                    case R.id.tv_cancel:
                        getOrderCancel(data.get(position).getOrder_id());
                        break;
                    case R.id.iv_call:
                        call(data.get(position).getWorker_tel());
                        break;
                    case R.id.iv_message:
                        final String name = SPUtil.getData(getActivity(), USER_UID, 0).toString();
                        if (!TextUtils.isEmpty(name)){
                            Intent i = new Intent(mActivity, ChatActivity.class);
                            i.putExtra(Constant.EXTRA_USER_ID, name);
                            i.putExtra("username", name);
                            startActivity(i);
                        }

                        break;

                }
            }
        });
        return robAdapter;
    }

    private void showdialog(final String getOrder_amount, final String getOrder_sn) {
        niceDialog = NiceDialog.init();
        niceDialog.setMargin(20);
        niceDialog.setLayoutId(R.layout.activity_wxapi_dialog)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder var1, BaseNiceDialog var2) {
                        TextView btn = (TextView) var1.getView(R.id.tv_btn);

                        final TextView et_money = (TextView) var1.getView(R.id.et_money);
                        et_money.setText(getOrder_amount);
                        btn.setOnClickListener(new View.OnClickListener() {
                                                   @Override
                                                   public void onClick(View view) {
                                                       getWxpay(getOrder_amount, getOrder_sn);
                                                       return;
                                                   }
                                               }
                        );
                    }
                })
                .show(getFragmentManager());
    }

    private void call(String getWorker_tel) {
        Uri uri = Uri.parse("tel:" + getWorker_tel);
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        startActivity(intent);
    }

    /**
     * 取消订单
     */
    private void getOrderCancel(String getOrder_id) {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        param.put("orderid", getOrder_id);
        NetTool.getApi().getOrderCancel(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<OrderCancelBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<OrderCancelBean>> value) {
                        if (value.getCode() == 0) {
                            getMydemand();
                            ToastUtil.showToast("取消订单成功！");
                            LogUtil.e("1111111111111111111111111" + " " + value.getMessage());
                        } else {
                            LogUtil.e("1111111111111111111111111" + " error code");
                        }
                    }
                });

    }

    @Override
    protected void OnItemClick(int position) {

    }

    @Override
    protected void OnRefresh() {
        setRefreshing(false);
    }

    private class MyDemandAdapter extends BaseQuickAdapter<MydemandBean, BaseViewHolder> {

        public MyDemandAdapter(int layoutResId, @Nullable List<MydemandBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, MydemandBean item) {
            helper.addOnClickListener(R.id.tv_confirm);
            helper.addOnClickListener(R.id.tv_reject);
            helper.addOnClickListener(R.id.tv_complete);
            helper.addOnClickListener(R.id.tv_unpay);
            helper.addOnClickListener(R.id.tv_done);
            helper.addOnClickListener(R.id.tv_pay);
            helper.addOnClickListener(R.id.tv_evaluate);
            helper.addOnClickListener(R.id.tv_cancel);
            helper.addOnClickListener(R.id.iv_call);
            helper.addOnClickListener(R.id.iv_message);
            helper.getView(R.id.tv_confirm).setVisibility(View.GONE);
            helper.getView(R.id.tv_reject).setVisibility(View.GONE);
            helper.getView(R.id.tv_complete).setVisibility(View.GONE);
            helper.getView(R.id.tv_unpay).setVisibility(View.GONE);
            helper.getView(R.id.tv_done).setVisibility(View.GONE);
            helper.getView(R.id.tv_pay).setVisibility(View.GONE);
            helper.getView(R.id.tv_evaluate).setVisibility(View.GONE);
            helper.getView(R.id.tv_cancel).setVisibility(View.GONE);
            helper.getView(R.id.tv_order_amount).setVisibility(View.GONE);

            switch (item.getOrder_status()) {
                case Const.OREDER_STATUS_DONE:
                    helper.setText(R.id.tv_order_status, "已完成");
                    break;
                case Const.OREDER_STATUS_CARRY_ON:
                    helper.setText(R.id.tv_order_status, "进行中");
                    break;
                case Const.OREDER_STATUS_UNEVALUATE:
                    helper.setVisible(R.id.tv_order_amount, true);
                    helper.setVisible(R.id.tv_evaluate, true);
                    helper.setText(R.id.tv_order_status, "待评价");
                    helper.setBackgroundColor(R.id.tv_evaluate, Color.rgb(255, 115, 0));//去评价变色
                    break;
                case Const.OREDER_STATUS_UNPAY:
                    helper.setVisible(R.id.tv_order_amount, true);//显示金钱数
                    helper.setVisible(R.id.tv_pay, true);
                    helper.setText(R.id.tv_order_status, "去支付");
                    break;

                case Const.OREDER_STATUS_WAIT_CONFIRM:
                    helper.setVisible(R.id.tv_cancel, true);
                    helper.setText(R.id.tv_order_status, "待确认");
                    break;
            }


            //x add
            helper.setText(R.id.tv_order_name, item.getOrder_name());
            helper.setText(R.id.tv_order_time, "服务时间：" + item.getAdd_time());
            helper.setText(R.id.tv_order_distance, "订单距离：" + item.getDistance());
            helper.setText(R.id.tv_order_address, "客户地址：" + item.getWorker_addr());
            helper.setText(R.id.tv_phonenumber, "电话：" + item.getWorker_tel());
            helper.setText(R.id.tv_order_amount, "￥ " + item.getOrder_amount());

        }
    }

    /**
     * 我的需求列表
     */
    private void getMydemand() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getMydemand(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<MydemandBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<MydemandBean>> value) {
                        if (value.getCode() == 0) {
                            data.clear();
                            List<MydemandBean> user = value.getData();
                            data.addAll(user);
                            for (MydemandBean bean : user) {
                                LogUtil.e(bean.toString());
                            }
                            robAdapter.notifyDataSetChanged();
                            LogUtil.e(user + " " + value.getMessage());
                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                    }
                });
    }


    private void getWxpay(final String getOrder_amount, final String getOrder_sn) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();
                builder.add("total", getOrder_amount)
                        .add("order_sn", getOrder_sn);
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(backUrl + "wxpay").header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "微信支付:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 0;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
//                            "appid": "wx071a16ef2a9e5d5d",
//                                    "partnerid": "1508265981",
//                                    "prepayid": "wx30154900030975c0d0bb39c72240365128",
//                                    "package": "Sign=WXPay",
//                                    "noncestr": "O1UpDnjRTSEN38Qo",
//                                    "timestamp": "1532936940",
//                                    "sign": "73C120560F28C0A1C5C94387555594A1"

                            JSONObject data = object.getJSONObject("data");
                            String appid = data.getString("appid");
                            String partnerid = data.getString("partnerid");
                            String prepayid = data.getString("prepayid");
                            String packages = data.getString("package");
                            String noncestr = data.getString("noncestr");
                            String timestamp = data.getString("timestamp");
                            String sign = data.getString("sign");
                            bundle.putString("appid", appid);
                            bundle.putString("partnerid", partnerid);
                            bundle.putString("prepayid", prepayid);
                            bundle.putString("packages", packages);
                            bundle.putString("noncestr", noncestr);
                            bundle.putString("timestamp", timestamp);
                            bundle.putString("sign", sign);
                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    Log.i("yudan", "微信支付:" + e);
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.i("yudan", "微信支付:" + e);
                    e.printStackTrace();
                }
            }
        }).start();


//        ToastUtil.showToast("支付请求！");
//        Map<String, Object> param = NetTool.newParams();
//        param.put("total",getOrder_amount);
//        param.put("order_sn",getOrder_sn);
//        NetTool.getApi().getwxpay(param)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new BaseObserver<BaseResponse<List<WxpayBean>>>() {
//                    @Override
//                    public void onData(BaseResponse<List<WxpayBean>> value) {
//                        if (value.getCode() == 0) {
//                            data2.clear();
////                            List<WxpayBean> user2 = value.getData();
////                            data2.addAll(user2);
////                            for (WxpayBean bean : user2) {
////                                LogUtil.e(bean.toString());
////                            }
//
//                            Log.i(TAG, "onData: ======="+value);
//                            LogUtil.e(  "zhangliang"+value.getMessage());
//                            LogUtil.e(  "zhangliang"+value.getData());
//
//                            ToastUtil.showToast(value.getMessage());
////                            WxPay();
//                        } else {
//                            LogUtil.e(value.error_code + " error code");
//                        }
//                    }
//                });
    }

    private void WxPay(String appid, String partnerid, String prepayid, String packages, String noncestr, String timestamp, String sign) {
        IWXAPI api = WXAPIFactory.createWXAPI(MyDemandFragment.this.getContext(), Const.APP_ID);
        api.registerApp(Const.APP_ID);
        if (!api.isWXAppInstalled() ) {
            ToastUtil.showToast("您没有安装微信，或者微信版本过低，请要先安装或者升级");
            return;
        }
        PayReq payRequest = new PayReq();
        payRequest.appId = Const.APP_ID;
        payRequest.partnerId = partnerid;
        payRequest.prepayId = prepayid;
        payRequest.packageValue = "Sign=WXPay";
        payRequest.nonceStr = noncestr;
        payRequest.timeStamp = timestamp;
        payRequest.sign = sign;
        niceDialog.dismiss();//关掉niceDialog弹窗
        api.sendReq(payRequest);
    }

}
