package com.zadetk.helpyou.fragments;

import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsListFragment;
import com.zadetk.helpyou.bean.MydemandBean;
import com.zadetk.helpyou.bean.RoblistBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.ToastUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Zackv on 2018/5/11.
 */

public class RobOrderFragment extends AbsListFragment {
    private static final String TAG = RobOrderFragment.class.getName();
    private RobAdapter robAdapter;
    private List<RoblistBean> data = new LinkedList<>();

    @Override
    protected void firstRequestData() {
        getRoblist();
        //notifyDataSetChanged方法通过一个外部的方法控制如果适配器的内容改变时需要强制调用getView来刷新每个Item的内容,可以实现动态的刷新列表的功能。

    }

    @Override
    protected void loadMore() {
//        if (data.size() > 20) {
        //加载结束
        robAdapter.loadMoreEnd();
        //  return;
//        }
//        RoblistBean robItemBean = new RoblistBean();
//        data.add(robItemBean);
//        data.add(robItemBean);
//        data.add(robItemBean);
//        data.add(robItemBean);
//        robAdapter.notifyDataSetChanged();
//        robAdapter.loadMoreComplete();
    }

    @Override
    //重载方法 Adapter的get方法
    public BaseQuickAdapter getAdapter() {
        robAdapter = new RobAdapter(R.layout.myorder_item2, data);
        robAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.tv_roborder:
                        robOrder();
                        break;
                }
            }
        });
        return robAdapter;
    }

    private void robOrder() {
        ToastUtil.showToast("抢单请求！");
    }


    @Override
    protected void OnItemClick(int position) {

    }

    @Override
    protected void OnRefresh() {
        setRefreshing(false);
    }
    //继承BaseQuickAdapter引用类 创建RobAdapter的构造方法
    private class RobAdapter extends BaseQuickAdapter<RoblistBean, BaseViewHolder> {

        public RobAdapter(int layoutResId, @Nullable List<RoblistBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, RoblistBean item) {
            helper.addOnClickListener(R.id.tv_roborder);
            helper.setText(R.id.tv_order_name, item.getOrdertitle());
            helper.setText(R.id.tv_order_distance, item.getDistance());
            helper.setText(R.id.tv_order_time, item.getAdd_time());
            helper.setText(R.id.tv_order_address, item.getCustomAddr());

        }
    }

    /**
     * 我要抢单列表
     */
    private void getRoblist() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getRoblist(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<RoblistBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<RoblistBean>> value) {
                        if (value.getCode() == 0) {
                            data.clear();


                            RoblistBean.class.toString();
                            List<RoblistBean> list = value.getData();
                            // data.addAll(list);


                            robAdapter.notifyDataSetChanged();
                            LogUtil.e(list + " " + value.getMessage());

                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                    }
                });
    }

}

