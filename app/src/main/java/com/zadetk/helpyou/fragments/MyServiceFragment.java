package com.zadetk.helpyou.fragments;

import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;
import android.view.View;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zadetk.helpyou.App;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.activities.EvaluateActivity;
import com.zadetk.helpyou.base.AbsListFragment;
import com.zadetk.helpyou.bean.CompleteserviceBean;
import com.zadetk.helpyou.bean.ConfirmorderBean;
import com.zadetk.helpyou.bean.OrdertomeBean;
import com.zadetk.helpyou.im.ChatActivity;
import com.zadetk.helpyou.im.Constant;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.SPUtil;
import com.zadetk.helpyou.utils.ToastUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.zadetk.helpyou.other.Const.USER_UID;

/**
 * Created by Zackv on 2018/5/11.
 */

public class MyServiceFragment extends AbsListFragment {
    private MyServiceAdapter myServiceAdapter;
    private List<OrdertomeBean> data = new LinkedList<>();
    private List<ConfirmorderBean> data2 = new LinkedList<>();//确认接单
    private List<ConfirmorderBean> data3 = new LinkedList<>();//拒绝接单
    private String orderid = "";

    @Override
    protected void firstRequestData() {
        getOrdertome();
        myServiceAdapter.notifyDataSetChanged();
    }

    @Override
    protected void loadMore() {
//        if (data.size() > 20) {
        myServiceAdapter.loadMoreEnd();
//            return;
//        }
//        MydemandBean robItemBean = new MydemandBean();
//        data.add(robItemBean);
//        data.add(robItemBean);
//        data.add(robItemBean);
//        data.add(robItemBean);
//        myServiceAdapter.notifyDataSetChanged();
//        myServiceAdapter.loadMoreComplete();
    }

    @Override
    public BaseQuickAdapter getAdapter() {
        myServiceAdapter = new MyServiceAdapter(R.layout.myorder_item1, data);
        myServiceAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                Intent intent;
                switch (view.getId()) {
                    case R.id.tv_confirm://确认订单请求
                        confirmOrder(data.get(position).getOrder_id());
                        myServiceAdapter.notifyDataSetChanged();
                        break;
                    case R.id.tv_reject://拒绝订单
                        rejectOrder(data.get(position).getOrder_id());
                        break;
                    case R.id.tv_complete:
                        completeOrder(data.get(position).getOrder_id());
                        break;
                    case R.id.tv_unpay:
                        break;
                    case R.id.tv_pay:
                        break;
                    case R.id.tv_evaluate:
                        intent = new Intent(mActivity, EvaluateActivity.class);
                        intent.putExtra("orderid", data.get(position).getOrder_id());
                        startActivity(intent);
                        break;
                    case R.id.tv_cancel:
                        break;
                    case R.id.iv_call:
                        call(data.get(position).getWorker_tel());
                        break;
                    case R.id.iv_message:
                        final String name = SPUtil.getData(getActivity(), USER_UID, 0).toString();
                        if (!TextUtils.isEmpty(name)){
                            Intent i = new Intent(mActivity, ChatActivity.class);
                            i.putExtra(Constant.EXTRA_USER_ID, name);
                            i.putExtra("username", name);
                            startActivity(i);
                        }
                        break;

                }
            }
        });
        return myServiceAdapter;
    }

    private void rejectOrder(String getOrder_id) {

        final String string = getOrder_id;
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        param.put("orderid", getOrder_id);
        param.put("state", "0");
        NetTool.getApi().getConfirmorder(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<ConfirmorderBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<ConfirmorderBean>> value) {
                        if (value.getCode() == 0) {
                            data3.clear();
                            List<ConfirmorderBean> user = value.getData();
                            LogUtil.e(user + " " + value.getMessage());
                            ToastUtil.showToast("已拒绝订单请求！");
                            getOrdertome();
                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                    }
                });
    }


    private void confirmOrder(String getOrder_id) {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        param.put("orderid", getOrder_id);
        param.put("state", "1");
        NetTool.getApi().getConfirmorder(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<ConfirmorderBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<ConfirmorderBean>> value) {
                        if (value.getCode() == 0) {
                            data2.clear();
                            List<ConfirmorderBean> user = value.getData();
                            myServiceAdapter.notifyDataSetChanged();
                            LogUtil.e(user + " " + value.getMessage());
                            ToastUtil.showToast("已确认订单请求！");
                            getOrdertome();
                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                    }
                });
    }

    /**
     * 完成订单请求
     */
    private void completeOrder(String getOrder_id) {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        param.put("orderid", getOrder_id);
        NetTool.getApi().getCompleteservice(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<CompleteserviceBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<CompleteserviceBean>> value) {
                        if (value.getCode() == 0) {
                            ToastUtil.showToast("完成服务请求！");
                            getOrdertome();
                            LogUtil.e(value.getMessage() + "getMessage");
                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                    }
                });
    }

    private void call(String getWorker_tel) {
        Uri uri = Uri.parse("tel:" + getWorker_tel);
        Intent intent = new Intent(Intent.ACTION_CALL, uri);
        startActivity(intent);
    }

    @Override
    protected void OnItemClick(int position) {
//        ToastUtil.showToast("点击了item！");

    }

    @Override
    protected void OnRefresh() {
        setRefreshing(false);
    }

    private class MyServiceAdapter extends BaseQuickAdapter<OrdertomeBean, BaseViewHolder> {

        public MyServiceAdapter(int layoutResId, @Nullable List<OrdertomeBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, OrdertomeBean item) {
            helper.addOnClickListener(R.id.tv_confirm);
            helper.addOnClickListener(R.id.tv_reject);
            helper.addOnClickListener(R.id.tv_complete);
            helper.addOnClickListener(R.id.tv_unpay);
            helper.addOnClickListener(R.id.tv_done);
            helper.addOnClickListener(R.id.tv_pay);
            helper.addOnClickListener(R.id.tv_evaluate);
            helper.addOnClickListener(R.id.tv_cancel);
            helper.addOnClickListener(R.id.iv_call);
            helper.addOnClickListener(R.id.iv_message);
            helper.getView(R.id.tv_confirm).setVisibility(View.GONE);
            helper.getView(R.id.tv_reject).setVisibility(View.GONE);
            helper.getView(R.id.tv_complete).setVisibility(View.GONE);
            helper.getView(R.id.tv_unpay).setVisibility(View.GONE);
            helper.getView(R.id.tv_done).setVisibility(View.GONE);
            helper.getView(R.id.tv_pay).setVisibility(View.GONE);
            helper.getView(R.id.tv_evaluate).setVisibility(View.GONE);
            helper.getView(R.id.tv_cancel).setVisibility(View.GONE);


            switch (item.getOrder_status()) {
                case Const.OREDER_STATUS_DONE:
                    helper.setVisible(R.id.tv_done, true);
                    helper.setText(R.id.tv_order_status, "已完成");
                    break;
                case Const.OREDER_STATUS_CARRY_ON:
                    helper.setVisible(R.id.tv_complete, true);
                    helper.setText(R.id.tv_order_status, "进行中");
                    break;
                case Const.OREDER_STATUS_UNPAY:
                    helper.setVisible(R.id.tv_unpay, true);
                    helper.setText(R.id.tv_order_status, "待支付");

                    break;
                case Const.OREDER_STATUS_WAIT_CONFIRM:
                    helper.setVisible(R.id.tv_confirm, true);
                    helper.setVisible(R.id.tv_reject, true);
                    helper.setText(R.id.tv_order_status, "待确认");
                    break;
            }

            //x add
            helper.setText(R.id.tv_order_name, item.getOrder_name());
            helper.setText(R.id.tv_order_time, item.getAdd_time());
            helper.setText(R.id.tv_order_distance, item.getDistance());
            helper.setText(R.id.tv_order_address, item.getWorker_addr());
            helper.setText(R.id.tv_phonenumber, item.getWorker_tel());

        }
    }

    /**
     * 我的服务列表
     */
    private void getOrdertome() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getOrdertome(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<OrdertomeBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<OrdertomeBean>> value) {
                        if (value.getCode() == 0) {
                            data.clear();
                            List<OrdertomeBean> user = value.getData();
                            data.addAll(user);
                            for (OrdertomeBean bean : user) {
                                LogUtil.e(bean.toString());
                            }
                            myServiceAdapter.notifyDataSetChanged();
                            LogUtil.e(user + " " + value.getMessage());
                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                    }
                });
    }
}
