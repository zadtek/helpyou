package com.zadetk.helpyou.other;

import android.content.Context;

import com.zadetk.helpyou.utils.PathUtils;

/**
 * Created by zack on 2018/5/16.
 */

public class Const {
    public static final int OREDER_STATUS_DONE = 5;
    public static final int OREDER_STATUS_WAIT_CONFIRM = 1;
    public static final int OREDER_STATUS_CARRY_ON = 2;
    public static final int OREDER_STATUS_UNPAY = 3;
    public static final int OREDER_STATUS_UNEVALUATE = 4;
    public static final String USER_UID = "useruid";
    public static final String USER_TOKEN = "usertokrn";
    public static final String USER_NAME = "username";
    public static final String USER_IS_LOGIN = "islogin";

    public static final String TYPE_NAME = "nickname";
    public static final String TYPE_AVATOR = "avator";
    public static final String TYPE_SEX = "sex";
    public static final String TYPE_PHONE = "mobile";
    public static final String TYPE_COMPANY_NAME = "companyname";
    public static final String TYPE_COMPANEY_PHONE = "companytell";
    public static final String TYPE_ADDRESS = "address";
    public static final String TYPE_ID_UP = "id_up";
    public static final String TYPE_ID_down = "id_down";
    public static final String TYPE_COMPANEY_ADDRESS = "companyaddr";
    public static final String TYPE_COMPANEY_IMG = "company_img";
    public static final String TYPE_COMPANEY_LICENCE = "licence";
    public static final String APP_ID = "wx071a16ef2a9e5d5d";//微信支付APPID
    public static final String FX_APP_ID = "wx071a16ef2a9e5d5d";//微信分享APPID
    public static final String APP_SECRET = "537ce0df34dcf0af9ba42a782a275002";//微信APP_SECRET

    public static final String ADVICE_BASE_URL = "http://helpu.zadtek.com/interface.php?m=share";

    public static final String PHONE_CHECK = "^((13[0-9])|(14[5,7])|(15[0-3,5-9])|(17[0,3,5-8])|(18[0-9])|166|198|199|(147))\\d{8}$";

    public static String getSavePath(Context context) {
        return PathUtils.getFilePath(context, "apk");
    }

    //推送信息数据
    public static final int MSG_SET_ALIAS = 1001;// jp绑定别名
    public static final int MSG_SET_TAGS = 1002;// jp绑定标签
}
