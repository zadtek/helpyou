package com.zadetk.helpyou.other;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.zadetk.helpyou.R;


public class CustomDialog extends Dialog {

    private TextView friend;
    private TextView moment;
    private TextView titleTv;
    private TextView messageTv;
    private String titleStr;
    private String messageStr;
    private String yesStr, noStr,cannleStr;

    /*  -------------------------------- 接口监听 -------------------------------------  */

    private onNoOnclickListener noOnclickListener;
    private onYesOnclickListener yesOnclickListener;
    private onCannleOnclickListener cannleOnclickListener;

    public interface onYesOnclickListener {
        void onYesClick();
    }
    public interface onCannleOnclickListener {
        void onCannleClick();
    }

    public interface onNoOnclickListener {
        void onNoClick();
    }

    public void setNoOnclickListener(String str, onNoOnclickListener onNoOnclickListener) {
        if (str != null) {
            noStr = str;
        }
        this.noOnclickListener = onNoOnclickListener;
    }

    public void setYesOnclickListener(String str, onYesOnclickListener onYesOnclickListener) {
        if (str != null) {
            yesStr = str;
        }
        this.yesOnclickListener = onYesOnclickListener;
    }
     public void setCannleOnClickListener(String str, onCannleOnclickListener onCannleOnclickListener) {
            if (str != null) {
                cannleStr = str;
            }
            this.cannleOnclickListener = onCannleOnclickListener;
        }

    /*  ---------------------------------- 构造方法 -------------------------------------  */

    public CustomDialog(Context context) {
        super(context, R.style.MyDialog);//风格主题
    }


    /*  ---------------------------------- onCreate-------------------------------------  */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);//自定义布局
        //按空白处不能取消动画
//        setCanceledOnTouchOutside(false);

        //初始化界面控件
        initView();
        //初始化界面数据
        initData();
        //初始化界面控件的事件
        initEvent();

    }

    /**
     * 初始化界面的确定和取消监听器
     */
    private void initEvent() {
        //设置确定按钮被点击后，向外界提供监听
        friend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (yesOnclickListener != null) {
                    yesOnclickListener.onYesClick();
                }
            }
        });
        //设置取消按钮被点击后，向外界提供监听
        moment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (noOnclickListener != null) {
                    noOnclickListener.onNoClick();
                }
            }
        });
        findViewById(R.id.share_cannle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (cannleOnclickListener != null) {
                    cannleOnclickListener.onCannleClick();
                }
            }
        });
    }

    /**
     * 初始化界面控件的显示数据
     */
    private void initData() {
        //如果用户自定了title和message
        if (titleStr != null) {
            titleTv.setText(titleStr);
        }
        if (messageStr != null) {
            messageTv.setText(messageStr);
        }
        //如果设置按钮的文字
        if (yesStr != null) {
            friend.setText(yesStr);
        }
        if (noStr != null) {
            moment.setText(noStr);
        }
    }

    /**
     * 初始化界面控件
     */
    private void initView() {
        friend = (TextView) findViewById(R.id.share_friend);
        moment = (TextView) findViewById(R.id.share_moment);
        titleTv = (TextView) findViewById(R.id.title);
        messageTv = (TextView) findViewById(R.id.message);


        Window dialogWindow = getWindow();
        if (dialogWindow != null) {
            dialogWindow.setGravity(Gravity.BOTTOM | Gravity.CENTER);//设置窗口位置
        }
    }

    /*  ---------------------------------- set方法 传值-------------------------------------  */
//为外界设置一些public 公开的方法，来向自定义的dialog传递值
    public void setTitle(String title) {
        titleStr = title;
    }

    public void setMessage(String message) {
        messageStr = message;
    }


}
