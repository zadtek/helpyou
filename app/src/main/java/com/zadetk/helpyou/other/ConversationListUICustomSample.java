package com.zadetk.helpyou.other;

import android.support.v4.app.Fragment;

import com.alibaba.mobileim.aop.Pointcut;
import com.alibaba.mobileim.aop.custom.IMConversationListUI;

/**
 * Created by zack on 2018/5/18.
 */

//继承IMConversationListUI，同时提供构造方法
public class ConversationListUICustomSample extends IMConversationListUI {

    public ConversationListUICustomSample(Pointcut pointcut) {
        super(pointcut);
    }

    //不显示标题栏
    @Override
    public boolean needHideTitleView(Fragment fragment) {
        return true;
    }
}