package com.zadetk.helpyou.other;

import android.content.Context;

import com.alibaba.mobileim.IYWLoginService;
import com.alibaba.mobileim.YWAPI;
import com.alibaba.mobileim.YWLoginParam;
import com.alibaba.mobileim.channel.event.IWxCallback;
import com.zadetk.helpyou.App;
import com.zadetk.helpyou.activities.LoginActivity;
import com.zadetk.helpyou.bean.UserInfoBean;
import com.zadetk.helpyou.utils.SPUtil;

import java.lang.reflect.Field;

import static com.zadetk.helpyou.other.Const.USER_IS_LOGIN;
import static com.zadetk.helpyou.other.Const.USER_NAME;
import static com.zadetk.helpyou.other.Const.USER_TOKEN;
import static com.zadetk.helpyou.other.Const.USER_UID;

/**
 * Created by zack on 2018/5/18.
 */

public class UserInfoManger {
    public static final UserInfoBean userInfo = new UserInfoBean();
    public static volatile boolean isLogin, isFetchUserInfo;
    private static Context context;

    public static void init(Context con) {
        context = con;
        boolean islogin = (boolean) SPUtil.getData(con, USER_IS_LOGIN, false);
        if (islogin) {
            int uid = (int) SPUtil.getData(con, USER_UID, -1);
            String name = (String) SPUtil.getData(con, USER_NAME, "");
            String token = (String) SPUtil.getData(con, USER_TOKEN, "");
            userInfo.setUsername(name);
            userInfo.setUid(uid);
            userInfo.setUserToken(token);
            isLogin = true;
        } else {
            isLogin = false;
        }
    }

    public static void login(String token, String username, int uid) {
        isLogin = true;
        userInfo.setNickname(username);
        userInfo.setUid(uid);
        userInfo.setUserToken(token);
        SPUtil.saveDate(context, Const.USER_TOKEN, token);
        SPUtil.saveDate(context, Const.USER_NAME, username);
        SPUtil.saveDate(context, Const.USER_UID, uid);
        SPUtil.saveDate(context, USER_IS_LOGIN, true);
    }

    public static void logout() {
        SPUtil.saveDate(context, Const.USER_TOKEN, "");
        SPUtil.saveDate(context, Const.USER_NAME, "");
        SPUtil.saveDate(context, Const.USER_UID, -1);
        SPUtil.saveDate(context, USER_IS_LOGIN, false);
        isLogin = false;
    }

    public static void loginYM() {
        String userid = "testpro1";
        String password = "taobao1234";
        App.mIMKit = YWAPI.getIMKitInstance(userid, "23015524");
        IYWLoginService loginService = App.mIMKit.getLoginService();
        YWLoginParam loginParam = YWLoginParam.createLoginParam(userid, password);
        loginService.login(loginParam, new IWxCallback() {
            @Override
            public void onSuccess(Object... arg0) {

            }

            @Override
            public void onProgress(int arg0) {

            }

            @Override
            public void onError(int errCode, String description) {
                //如果登录失败，errCode为错误码,description是错误的具体描述信息
            }
        });
    }

    public static Object merage(Object sourceBean, Object targetBean) {
        Class sourceBeanClass = sourceBean.getClass();
        Class targetBeanClass = targetBean.getClass();

        Field[] sourceFields = sourceBeanClass.getDeclaredFields();
        Field[] targetFields = targetBeanClass.getDeclaredFields();
        for (int i = 0; i < sourceFields.length; i++) {
            Field sourceField = sourceFields[i];
            Field targetField = targetFields[i];
            sourceField.setAccessible(true);
            targetField.setAccessible(true);
            try {
                if (!(sourceField.get(sourceBean) == null) && !"serialVersionUID".equals(sourceField.getName().toString())) {
                    targetField.set(targetBean, sourceField.get(sourceBean));
                }
            } catch (IllegalArgumentException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
        return targetBean;
    }
}
