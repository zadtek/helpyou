package com.zadetk.helpyou;

import android.os.Bundle;

public class MessageEvents{
    /**
     * 刷新企业样本列表数据
     */
    public static final String WXLOGIN_SUCCESS = "wxlogin_success";
    /**
     * 默认事件
     */
    private String eventsType = "default_events";
    /**
     * 信息传递
     */
    private Bundle message;

    public MessageEvents setEventsType(String eventsType) {
        this.eventsType = eventsType;
        return this;
    }

    public String getEventsType() {
        return eventsType;
    }

    public Bundle getMessage() {
        return message;
    }

    public MessageEvents setMessage(Bundle message) {
        this.message = message;
        return this;
    }

    public MessageEvents setMessage(Object value) {
        this.message = new Bundle();
        if (value instanceof String) {
            message.putString(getEventsType(), (String) value);
        } else if (value instanceof Integer) {
            message.putInt(getEventsType(), (int) value);
        } else if (value instanceof Boolean) {
            message.putBoolean(getEventsType(), (Boolean) value);
        } else {
            message.putString(getEventsType(), String.valueOf(value));
        }
        return this;
    }
}
