package com.zadetk.helpyou;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.support.multidex.MultiDex;

import com.alibaba.mobileim.YWAPI;
import com.alibaba.mobileim.YWIMKit;
import com.alibaba.mobileim.aop.AdviceBinder;
import com.alibaba.mobileim.aop.PointCutEnum;
import com.alibaba.wxlib.util.SysUtil;
import com.zadetk.helpyou.im.DemoHelper;
import com.zadetk.helpyou.other.ConversationListUICustomSample;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;

import java.util.LinkedList;
import java.util.List;

import cn.jpush.android.api.JPushInterface;


/**
 * Created by Zackv on 2018/4/2.
 */

public class App extends Application {
    public static YWIMKit mIMKit;
    private static Application application;
    private static List<Activity> activities = new LinkedList<>();

    public static Application getApplication() {
        return application;
    }

    public static void finishAllActivity() {
        if (activities == null) {
            return;
        }
        for (Activity activity : activities) {
            activity.finish();
        }
        activities.clear();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        application = this;

        JPushInterface.setDebugMode(true); 	// 设置开启日志,发布时请关闭日志
        JPushInterface.init(application);     		// 初始化 JPush

        UserInfoManger.init(this);
//        initYW();
        addLifecycle();
        DemoHelper.getInstance().init(this);
    }

    private void initYW() {
        final String APP_KEY = "23015524";
        //必须首先执行这部分代码, 如果在":TCMSSevice"进程中，无需进行云旺（OpenIM）和app业务的初始化，以节省内存;
        SysUtil.setApplication(this);
        if (SysUtil.isTCMSServiceProcess(this)) {
            return;
        }
        //第一个参数是Application Context
        //这里的APP_KEY即应用创建时申请的APP_KEY，同时初始化必须是在主进程中
        if (SysUtil.isMainProcess()) {
            YWAPI.init(application, APP_KEY);
        }
        AdviceBinder.bindAdvice(PointCutEnum.CONVERSATION_FRAGMENT_UI_POINTCUT, ConversationListUICustomSample.class);
        if (UserInfoManger.isLogin) {
            UserInfoManger.loginYM();



        }
    }

    private void addLifecycle() {
        application.registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {
                activities.add(activity);
                LogUtil.d(activity.getLocalClassName());
            }

            @Override
            public void onActivityStarted(Activity activity) {
                LogUtil.d(activity.getLocalClassName());
            }

            @Override
            public void onActivityResumed(Activity activity) {
                LogUtil.d(activity.getLocalClassName());
            }

            @Override
            public void onActivityPaused(Activity activity) {
                LogUtil.d(activity.getLocalClassName());
            }

            @Override
            public void onActivityStopped(Activity activity) {
                LogUtil.d(activity.getLocalClassName());
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
                LogUtil.d(activity.getLocalClassName());
            }

            @Override
            public void onActivityDestroyed(Activity activity) {
                if (activities.contains(activity)) {
                    activities.remove(activity);
                }
                LogUtil.d(activity.getLocalClassName());
            }
        });
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
