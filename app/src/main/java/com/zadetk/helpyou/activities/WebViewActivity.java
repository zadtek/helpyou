package com.zadetk.helpyou.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.utils.ToastUtil;

/**
 * TODO 空白webview页面
 */
public class WebViewActivity extends AbsActivity {
    public static final String TagertUrl = "TagertUrl";
    public static final String Title = "Title";
    private ProgressBar progressBar;
    private WebView webView;
    private String url, title;

    @Override
    public int setView() {
        return R.layout.activity_web_view;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initData();
        initViews();
        initLogic();
    }

    private void initData() {
        Intent intent = getIntent();
        url = intent.getStringExtra(TagertUrl);
        title = intent.getStringExtra(Title);
    }

    private void initLogic() {
        WebSettings settings = webView.getSettings();

        settings.setJavaScriptEnabled(true);
        settings.setJavaScriptCanOpenWindowsAutomatically(true);

        settings.setAllowFileAccess(true);

        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setBuiltInZoomControls(true);
        settings.setDomStorageEnabled(true);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            settings.setDisplayZoomControls(false);
        }

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR2) {
            settings.setRenderPriority(WebSettings.RenderPriority.HIGH);//提高渲染优先级
            settings.setSavePassword(false);
            settings.setPluginState(WebSettings.PluginState.ON);
        }

        progressBar.setMax(100);
        progressBar.setProgressDrawable(new ColorDrawable(0x0090ff));
        progressBar.setProgress(5); //先加载5%，以使用户觉得界面没有卡死

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                progressBar.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                progressBar.setVisibility(View.GONE);
            }
        });
        webView.setWebChromeClient(new WebChromeClient() {
            @Override
            public void onProgressChanged(WebView view, int newProgress) {
                super.onProgressChanged(view, newProgress);
                if (newProgress >= 5) {
                    progressBar.setProgress(newProgress);
                }
            }
        });
        if (!TextUtils.isEmpty(url)) {
            webView.loadUrl(url);
        } else {
            ToastUtil.showToast("url为空！");
        }

    }

    private void initViews() {
        if (TextUtils.isEmpty(title)) {
            setTopTitle("未设置标题");
        } else {
            setTopTitle(title);
        }
        progressBar = findViewById(R.id.loading_progress);
        webView = findViewById(R.id.webview);
    }
}
