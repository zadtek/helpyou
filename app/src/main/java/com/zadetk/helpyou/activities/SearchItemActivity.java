package com.zadetk.helpyou.activities;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.alibaba.mobileim.ui.contact.adapter.SearchAdapter;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.squareup.picasso.Picasso;
import com.zadetk.helpyou.ActivityUtils;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.adapter.MyAdapter;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.HotservicecatBean;
import com.zadetk.helpyou.bean.SearchservicecatBean;
import com.zadetk.helpyou.bean.ServicecatBean;
import com.zadetk.helpyou.bean.SkilllistBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.view.NoScrollView;
import com.zhy.view.flowlayout.FlowLayout;
import com.zhy.view.flowlayout.TagAdapter;
import com.zhy.view.flowlayout.TagFlowLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 4-3-1搜索页面
 */
public class SearchItemActivity extends AbsActivity implements View.OnClickListener {

    private static final String TAG = SearchItemActivity.class.getName();
    private ListView mListView;
    private ArrayList<String> list = new ArrayList<>();
    private List<ServicecatBean> data = new LinkedList<>();
    private List<HotservicecatBean> data1 = new LinkedList<>();
    private List<List<SearchservicecatBean>> data2 = new LinkedList<>();
    private SearchservicecatBean bean = new SearchservicecatBean();
    private List<String> tagList = new LinkedList<>();
    private EditText etSearch;
    private TextView tvCancel;
    private NoScrollView gvTag;
    private LinearLayout llHotTag;
    private TagFlowLayout flTag;
    private LinearLayout llList;
    private RecyclerView recycleview;
    private SearchAdapter searchAdapter;
    private ImageView ivBack;
    private ImageView iv;
    private TextView tv;
    private String title;
    //    private String etSearchText;
    private SimpleAdapter adapter;
    private List<String> oneTitle = new LinkedList<>();
    private List<String> twoTitle = new LinkedList<>();
    private List<String> threeTitle = new LinkedList<>();
    private List<String> catId = new LinkedList<>();
    List<Map<String, Object>> mData = new LinkedList<>();
    Handler myhandler = new Handler();
    private MyServiceAdapter serviceAdapter;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case 0:
                    serviceAdapter.notifyDataSetChanged();
                    break;
            }
        }
    };

    /**
     * Find the Views in the layout<br />
     * <br />
     * Auto-created on 2018-05-14 13:40:54 by Android Layout Finder
     * (http://www.buzzingandroid.com/tools/android-layout-finder)
     */
    private void initViews() {
        etSearch = findViewById(R.id.et_search);
        tvCancel = findViewById(R.id.tv_cancel);
        gvTag = findViewById(R.id.gv_tag);
        llHotTag = findViewById(R.id.ll_hot_tag);
        flTag = findViewById(R.id.fl_tag);
        llList = findViewById(R.id.ll_list);
//        recycleview = findViewById(R.id.recycleview);
        mListView = (ListView) findViewById(R.id.mListView);
        ivBack = findViewById(R.id.iv_back);
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
//        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//       recycleview.setLayoutManager(linearLayoutManager);
        setTopTitle("发布服务");
        llList.setVisibility(View.GONE);
        llHotTag.setVisibility(View.GONE);
//        etSearchText = etSearch.getText().toString();
    }

    @Override
    public int setView() {
        return R.layout.activity_search_item;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        ActivityUtils.getInstance().addActivity("SearchItemActivity", this);
        initViews();
        getHotList();
        initLogic();
        getServiceList();
        getSearchList();
//        compare();


    }

    // 搜索值与搜索接口返回值作比较
    private void initLogic() {
        serviceAdapter = new MyServiceAdapter(this);
        gvTag.setAdapter(serviceAdapter);
        gvTag.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.setClass(SearchItemActivity.this, PublishServiceSubActivity.class);
                intent.putExtra("panrentid", data.get(position).getCatid());
//                Log.i(TAG, "initBasic: =========" +data.get(position).getCatid());
                startActivity(intent);
            }
        });

        // 搜索框
        etSearch.clearFocus();
        etSearch.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    llHotTag.setVisibility(View.VISIBLE);
                    llList.setVisibility(View.GONE);
                    gvTag.setVisibility(View.GONE);
                    tvCancel.setVisibility(View.VISIBLE);
                } else {
                    tvCancel.setVisibility(View.GONE);
                    llHotTag.setVisibility(View.GONE);
                    llList.setVisibility(View.GONE);
                    gvTag.setVisibility(View.VISIBLE);
                }
            }
        });
        etSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //charSequence  输入框中改变前的字符串信息
                //i 输入框中改变前的字符串的起始位置
                //i1 输入框中改变前后的字符串改变数量一般为0
                //i2 输入框中改变后的字符串与起始位置的偏移量
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //charSequence  输入框中改变后的字符串信息
                //i 输入框中改变后的字符串的起始位置
                //i1 输入框中改变前的字符串的位置 默认为0
                //i2 输入框中改变后的一共输入字符串的数量
            }

            // editable  输入结束呈现在输入框中的信息
            @Override
            public void afterTextChanged(Editable editable) {

                if (TextUtils.isEmpty(editable.toString())) {
                    if (etSearch.hasFocus()) {
                        llHotTag.setVisibility(View.VISIBLE);
                        llList.setVisibility(View.GONE);
                        gvTag.setVisibility(View.GONE);
                    } else {
                        llHotTag.setVisibility(View.GONE);
                        llList.setVisibility(View.GONE);
                        gvTag.setVisibility(View.VISIBLE);
                    }
                } else {
                    llHotTag.setVisibility(View.GONE);
                    llList.setVisibility(View.VISIBLE);
                    gvTag.setVisibility(View.GONE);
                }
                myhandler.post(eChanged);
            }
        });
        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                tvCancel.setVisibility(View.GONE);
                etSearch.setText("");
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(SearchItemActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                etSearch.clearFocus();
            }
        });
        // item2 房屋维修
//        searchAdapter = new SearchAdapter(R.layout.search_item2, data);
//        searchAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
//            @Override
//            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
//                startActivity(new Intent(SearchItemActivity.this, PublishServiceActivity.class));
//            }
//        });
////        recycleview.setAdapter(searchAdapter);
//        Log.i(TAG, "233: 0======="+tagList);
    }

    // 获取热门类别数据
    private void getHotList() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getHotservicecat(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<HotservicecatBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<HotservicecatBean>> value) {
                        if (value.getCode() == 0) {
                            data1.clear();
                            List<HotservicecatBean> user = value.getData();
                            data1.addAll(user);
                            for (HotservicecatBean bean : user) {
                                LogUtil.e("1=========" + bean.toString());
                            }
                            for (int i = 0; i < data1.size(); i++) {
                                title = data1.get(i).getCatname();
//                                Log.i(TAG, "initLogic: ==========="+data1.get(i).getCatname());
                                tagList.add(title);

                            }

                            LogUtil.e(user + " 4============ " + value.getMessage());
                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                        Log.i(TAG, "233 5=======" + tagList);
                        final TagAdapter tagAdapter = new TagAdapter<String>(tagList) {
                            @Override
                            public View getView(FlowLayout parent, int position, String s) {
                                TextView tv = (TextView) getLayoutInflater().inflate(R.layout.tag_text, null);
                                tv.setText(s);
                                return tv;
                            }
                        };
                        flTag.setAdapter(tagAdapter);
                        flTag.setOnTagClickListener(new TagFlowLayout.OnTagClickListener() {
                            @Override
                            public boolean onTagClick(View view, int position, FlowLayout parent) {
                                startActivity(new Intent(SearchItemActivity.this, PublishServiceActivity.class));
                                return false;
                            }
                        });
                    }
                });
    }

    // 获取一级分类数据
    private void getServiceList() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getServicecat(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<ServicecatBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<ServicecatBean>> value) {
                        if (value.getCode() == 0) {
                            data.clear();
                            List<ServicecatBean> user = value.getData();
                            data.addAll(user);
                            for (ServicecatBean bean : user) {
                                LogUtil.e("==" + bean.toString());
                                list.add("");
                                Log.i(TAG, "onData: ===" + list.size());
                            }
                            Message message = new Message();
                            message.what = 0;
                            handler.sendMessage(message);

                            LogUtil.e(user + " == " + value.getMessage());
                        } else {
                            LogUtil.e(value.error_code + "== error code");
                        }
                    }

                });
    }

    // 获取搜索服务类别接口的返回值


    Runnable eChanged = new Runnable() {

        @Override
        public void run() {
            // TODO Auto-generated method stub
            String data = etSearch.getText().toString();

            mData.clear();

            getmDataSub(mData, data);

            if (adapter != null)
                adapter.notifyDataSetChanged();
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Intent intent1 = new Intent(SearchItemActivity.this, PublishServiceActivity.class);
                    String catid = mData.get(position).get("cat_id").toString();
                    intent1.putExtra("catid", catid);
                    Log.i("caizhijia", "name==" + mData.get(position).get("cat_id").toString());
                    startActivity(intent1);
                }
            });

        }
    };

    private void set_mListView_adapter(final List<Map<String, Object>> mData) {

        adapter = new SimpleAdapter(this, mData, R.layout.search_item2,
                new String[]{"one", "two", "three"}, new int[]{R.id.one, R.id.two, R.id.three});


        mListView.setAdapter(adapter);
    }


    private void getmDataSub(List<Map<String, Object>> mDataSubs, String data) {
        int length = oneTitle.size();
        for (int i = 0; i < length; ++i) {
            if (oneTitle.get(i).contains(data) || twoTitle.get(i).contains(data) || threeTitle.get(i).contains(data)) {
                Map<String, Object> item = new HashMap<String, Object>();
                item.put("one", oneTitle.get(i));
                item.put("two", twoTitle.get(i));
                item.put("three", threeTitle.get(i));
                item.put("cat_id", catId.get(i));
                mDataSubs.add(item);
            }
        }
    }

    private void getSearchList() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getSearchservicecat(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<List<SearchservicecatBean>>>>() {
                    @Override
                    public void onData(BaseResponse<List<List<SearchservicecatBean>>> value) {
                        if (value.getCode() == 0) {
                            data2.clear();
                            List<List<SearchservicecatBean>> user = value.getData();
                            data2.addAll(user);
                            for (int i = 0; i < user.size(); i++) {
                                List<SearchservicecatBean> list = user.get(i);
//                                Log.i("yudan","list=="+list.size());

//                                for (int j=0;j<list.size();j++){
//                                    bean =list.get(j);

                                Map<String, Object> item = new HashMap<String, Object>();
                                oneTitle.add(list.get(0).getCat_name());
                                twoTitle.add(list.get(1).getCat_name());
                                threeTitle.add(list.get(2).getCat_name());
                                catId.add(list.get(2).getCat_id());
                                item.put("cat_id", catId.get(i));
                                item.put("one", oneTitle.get(i));
                                item.put("two", twoTitle.get(i));
                                item.put("three", threeTitle.get(i));

                                mData.add(item);
                                set_mListView_adapter(mData);
//                                        Log.i("yudan","name=="+bean.getCat_name());

                            }
                        } else {
                            LogUtil.e(value.error_code + "== error code");
                        }
                    }
                });

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
//                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputMethodManager.hideSoftInputFromWindow(SearchItemActivity.this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                finish();
                break;
        }
    }

    //搜索Adapter
    private class SearchAdapter extends BaseQuickAdapter<ServicecatBean, BaseViewHolder> {

        public SearchAdapter(int layoutResId, @Nullable List<ServicecatBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, ServicecatBean item) {
        }
    }

    // 分类Adapter
    private class MyServiceAdapter extends BaseAdapter {
        Context context;

        public MyServiceAdapter(Context context) {
            this.context = context;
        }

        @Override
        public int getCount() {
            return data.size();
        }

        @Override
        public Object getItem(int position) {
            return data.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            //通过布局填充器LayoutInflater填充网格单元格内的布局
            View v = LayoutInflater.from(context).inflate(R.layout.search_item, null);
            //使用findViewById分别找到单元格内布局的图片以及文字
            iv = (ImageView) v.findViewById(R.id.iv_search);
            tv = (TextView) v.findViewById(R.id.tv_search);
            //引用数组内元素设置布局内图片以及文字的内容
            String url = data.get(position).getCatico();
//            Picasso.get().
//                    load(url)
//                    .placeholder(R.drawable.list_ico2)
//                    .error(R.drawable.list_ico3)
//                    .into(iv);
//
            // TODO 加载图片过程中默认图以及加载失败的图片
            Glide.with(context)
                    .load(url)
                    .placeholder(R.drawable.aliwx_head_default)//图片加载出来前，显示的图片
                    .error(R.drawable.aliwx_default_gallery_image)//图片加载失败后，显示的图片
                    .into(iv);
//            Picasso.get().load(data.get(position).getCatico()).into(iv);
            tv.setText(data.get(position).getCatname());
            //返回值一定为单元格整体布局v
            return v;
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityUtils.getInstance().delActivity("SearchItemActivity");
    }
}