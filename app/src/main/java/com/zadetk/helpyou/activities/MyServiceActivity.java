package com.zadetk.helpyou.activities;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.PointerIcon;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.activities.PublishServiceActivity;
import com.zadetk.helpyou.activities.SearchItemActivity;
import com.zadetk.helpyou.adapter.ServiceDetailsAdapter;
import com.zadetk.helpyou.adapter.VoiceAdapter;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.DeleteskillBean;
import com.zadetk.helpyou.bean.OrdertomeBean;
import com.zadetk.helpyou.bean.ServiceItemBean;
import com.zadetk.helpyou.bean.SkilldetailBean;
import com.zadetk.helpyou.bean.SkilllistBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.ToastUtil;

import org.androidannotations.annotations.Bean;

import java.sql.DatabaseMetaData;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 4-3-5发布我的服务页面
 */
public class MyServiceActivity extends AbsActivity implements View.OnClickListener {
    private static final String TAG = MyServiceActivity.class.getName();
    private TextView tvPublish;
    private RecyclerView recyclerView;
    private PublishServiceAdapter serviceAdapter;
    private List<SkilllistBean> data = new LinkedList<>();
    private DeleteskillBean data2 = new DeleteskillBean();
    private TextView serviceText;

    @Override
    public int setView() {
        return R.layout.activity_my_service;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();
        requestData();
    }

    private void requestData() {
        getSkillList();

    }

    private void initLogic() {
        serviceAdapter = new PublishServiceAdapter(R.layout.publish_service_item, data);
        serviceAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
//                    case R.id.tv_title:
//                        Intent in = new Intent();
//                        in.setClass(MyServiceActivity.this, MySkilldetailActivity.class);
//                        in.putExtra("skillid", data.get(position).getSkillid());
//                        startActivity(in);
//                        break;
                    case R.id.iv_edit:
                        Intent intent = new Intent();
                        intent.setClass(MyServiceActivity.this, MySkilldetailActivity.class);
                        intent.putExtra("skillid", data.get(position).getSkillid());
                        startActivity(intent);
                        break;
                    case R.id.iv_delete:
                        delete(data.get(position).getSkillid());

                        break;
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(serviceAdapter);
        tvPublish.setOnClickListener(this);
    }

    private void delete(final String skillid) {
        final String string = skillid;
        Dialog dialog = new AlertDialog.Builder(MyServiceActivity.this)
                .setTitle("删除本条服务")
                .setMessage("确认删除吗？")
                //相当于点击确认按钮
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                        Map<String, Object> param = NetTool.newParams();
                        param.put("uid", UserInfoManger.userInfo.getUid());
                        param.put("token", UserInfoManger.userInfo.getUserToken());
                        param.put("skillid", skillid);
                        Log.i(TAG, "=================" + UserInfoManger.userInfo.getUserToken());

                        Log.i(TAG, "=================" + skillid);
                        NetTool.getApi().getDeleteskill(param)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new BaseObserver<BaseResponse<DeleteskillBean>>() {

                                    @Override
                                    public void onData(BaseResponse<DeleteskillBean> value) {
                                        if (value.getCode() == 0) {

                                            ToastUtil.showToast("删除成功");
                                            serviceAdapter.notifyDataSetChanged();
                                            LogUtil.e(data2 + " " + value.getMessage());
                                            getSkillList();
                                        } else {
                                            LogUtil.e(value.error_code + " error code");
                                        }
                                    }
                                });

                    }
                })
                //相当于点击取消按钮
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create();
        dialog.show();
    }


    private void initViews() {
        tvPublish = findViewById(R.id.tv_btn_publish);
        recyclerView = findViewById(R.id.recycleview);
        serviceText = findViewById(R.id.tv_title);
        setTopTitle("我的服务");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_btn_publish:
                startActivity(new Intent(this, SearchItemActivity.class));
                break;
        }
    }

    private class PublishServiceAdapter extends BaseQuickAdapter<SkilllistBean, BaseViewHolder> {
        public PublishServiceAdapter(int layoutResId, @Nullable List<SkilllistBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, SkilllistBean item) {
            helper.addOnClickListener(R.id.iv_edit);
            helper.addOnClickListener(R.id.iv_delete);
            helper.addOnClickListener(R.id.tv_title);
            helper.setText(R.id.tv_title, item.getSkilltitle());
        }
    }

    private void getSkillList() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        Log.i(TAG, "=================" + UserInfoManger.userInfo.getUserToken());

        NetTool.getApi().getskilllist(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<SkilllistBean>>>() {

                    @Override
                    public void onData(BaseResponse<List<SkilllistBean>> value) {

                        if (value.getCode() == 0) {
                            data.clear();
                            List<SkilllistBean> user = value.getData();
                            data.addAll(user);
                            Log.i(TAG, "=================" + data);
                            serviceAdapter.notifyDataSetChanged();

                            LogUtil.e(user + " " + value.getMessage());
                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                    }
                });
    }

}

