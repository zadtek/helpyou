package com.zadetk.helpyou.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.amap.api.maps2d.AMap;
import com.amap.api.maps2d.CameraUpdateFactory;
import com.amap.api.maps2d.MapView;
import com.amap.api.maps2d.model.LatLng;
import com.amap.api.maps2d.model.MarkerOptions;
import com.amap.api.maps2d.model.MyLocationStyle;
import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.BaseActivity;
import com.zadetk.helpyou.bean.ServiceDetailsBean;
import com.zadetk.helpyou.im.ChatActivity;
import com.zadetk.helpyou.im.Constant;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.SPUtil;
import com.zadetk.helpyou.view.NoScrollView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.READ_PHONE_STATE;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.zadetk.helpyou.Urls.book;
import static com.zadetk.helpyou.Urls.collect;
import static com.zadetk.helpyou.Urls.collectDelete;
import static com.zadetk.helpyou.Urls.serverinfo;
import static com.zadetk.helpyou.other.Const.USER_UID;

public class ServiceDetailsActivity extends BaseActivity implements View.OnClickListener {
    private int is_collect = 0;
    private RecyclerView recycleview2;
    private List<ServiceDetailsBean> data2 = new LinkedList<>();
    private Voice2Adpater voice2Adpater;
    private TextView tvName, tvTitle;
    private TextView tvDec1;
    private TextView tvEvaluateNum;
    private SimpleRatingBar rbMark;
    private TextView tvPrice;
    private MapView map;
    private TextView vBtnCall;
    private TextView vBtnMessage;
    private TextView vAddress;
    private ImageView focus;
    private ImageView ivImg;
    private TextView tvBtnReservation;
    private String[] per = {ACCESS_COARSE_LOCATION, WRITE_EXTERNAL_STORAGE, READ_PHONE_STATE};
    private String[] dialPer = {CALL_PHONE};
    private AMap aMap;
    private MyLocationStyle myLocationStyle;
    private ProgressDialog dialog;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            String error_code = "";
            Bundle bundle = null;
            if (msg.getData() != null) {
                bundle = msg.getData();
                error_code = bundle.getString("error_code");
            }
            switch (msg.what) {
                case 0:
                    if (error_code.equals("0")) {
                        final String mobile = bundle.getString("mobile");
                        voice2Adpater.notifyDataSetChanged();
                        tvTitle.setText(bundle.getString("toptitle"));
                        tvName.setText(bundle.getString("sitetitle"));
                        tvDec1.setText(bundle.getString("sitedesc"));
                        tvEvaluateNum.setText("用户点评(" + bundle.getString("commentnum") + ")");
                        rbMark.setRating(Float.parseFloat(bundle.getString("starnum")));
                        tvPrice.setText("价格￥" + bundle.getString("price"));
                        vAddress.setText("地址:" + bundle.getString("address"));
                        Glide.with(ServiceDetailsActivity.this).load(bundle.getString("ivImg")).into(ivImg);
                        //地图
                        LatLng latLng = new LatLng(Double.parseDouble(bundle.getString("lat")), Double.parseDouble(bundle.getString("lnt")));
                        aMap.addMarker(new MarkerOptions().position(latLng).snippet("DefaultMarker").draggable(true).visible(true));
                        aMap.moveCamera(CameraUpdateFactory.changeLatLng(latLng));
                        aMap.moveCamera(CameraUpdateFactory.zoomTo(18));

                        //(0-确认预约，1-等待确认，2-进行中,3-待支付)
                        final int status = Integer.parseInt(bundle.getString("response"));
                        if (status == 0)
                            tvBtnReservation.setText("确认预约");
                        else if (status == 1)
                            tvBtnReservation.setText("等待确认");
                        else if (status == 2)
                            tvBtnReservation.setText("进行中");
                        else if (status == 3)
                            tvBtnReservation.setText("待支付");
                        final Bundle finalBundle = bundle;
                        vBtnMessage.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if (!TextUtils.isEmpty(mobile)) {
                                    Intent i = new Intent(ServiceDetailsActivity.this, ChatActivity.class);
                                    i.putExtra(Constant.EXTRA_USER_ID, mobile);
                                    i.putExtra("username", mobile);
                                    startActivity(i);
                                }
                            }
                        });
                        tvBtnReservation.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                sureToOrder(finalBundle.getString("lat"), finalBundle.getString("lnt"), finalBundle.getString("address"));
                            }
                        });

                        performCodeWithPermission("需要拨打电话权限", new PermissionCallback() {
                            @Override
                            public void hasPermission() {
                                vBtnCall.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Intent intent = new Intent(Intent.ACTION_CALL);
                                        Uri data = Uri.parse("tel:" + mobile);
                                        intent.setData(data);
                                        if (ActivityCompat.checkSelfPermission(ServiceDetailsActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                                            // TODO: Consider calling
                                            //    ActivityCompat#requestPermissions
                                            // here to request the missing permissions, and then overriding
                                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                                            //                                          int[] grantResults)
                                            // to handle the case where the user grants the permission. See the documentation
                                            // for ActivityCompat#requestPermissions for more details.
                                            return;
                                        }
                                        startActivity(intent);
                                    }
                                });
                            }

                            @Override
                            public void noPermission() {

                            }
                        }, dialPer);
                        is_collect = Integer.parseInt(bundle.getString("is_collect"));//0未关注1关注
                        if (is_collect == 0)
                            focus.setImageResource(R.mipmap.unfocus);
                        else
                            focus.setImageResource(R.mipmap.focused);

                    }
                    closeDialog();
                    break;
                case 1:
                    if (error_code.equals("0")) {
                        tvBtnReservation.setText(bundle.getString("response"));
                        Toast.makeText(getApplicationContext(), bundle.getString("response"), Toast.LENGTH_SHORT).show();
                    }
                    break;
                case 2:
                    String data = bundle.getString("data");
                    int isFocus = bundle.getInt("isFocus");
                    if (error_code.equals("0")) {
                        if (isFocus == 0) {
                            is_collect = 1;
                        } else {
                            is_collect = 0;
                        }
                    }
                    break;
                case 7:
                    Toast.makeText(getApplicationContext(), "没有网络", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_details);
        voice2Adpater = new Voice2Adpater(R.layout.voice_item2, data2);
        recycleview2 = findViewById(R.id.recycleview2);
        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
        recycleview2.setLayoutManager(linearLayoutManager2);
        View v2 = getHeaderView2(savedInstanceState);
        voice2Adpater.addHeaderView(v2);
        voice2Adpater.addFooterView(getLayoutInflater().inflate(R.layout.voice_footer, null));
        recycleview2.setAdapter(voice2Adpater);
        vBtnCall.setOnClickListener(this);
        vBtnMessage.setOnClickListener(this);
        dialog = ProgressDialog.show(ServiceDetailsActivity.this, "", "正在加载中……", true, false, null);
        initData();
    }

    private View getHeaderView2(final Bundle savedInstanceState) {
        View v = getLayoutInflater().inflate(R.layout.voice_header2, null);
        focus = v.findViewById(R.id.focus);
        tvTitle = v.findViewById(R.id.tv_title);
        tvName = v.findViewById(R.id.tv_name);
        tvDec1 = v.findViewById(R.id.tv_dec1);
        tvEvaluateNum = v.findViewById(R.id.tv_evaluate_num);
        rbMark = v.findViewById(R.id.rb_mark);
        tvPrice = v.findViewById(R.id.tv_price);
        map = v.findViewById(R.id.map);
        vBtnCall = v.findViewById(R.id.v_btn_call);
        vBtnMessage = v.findViewById(R.id.v_btn_message);
        vAddress = v.findViewById(R.id.v_address);
        focus.setOnClickListener(this);
        ivImg = v.findViewById(R.id.iv_img);
        tvBtnReservation = v.findViewById(R.id.tv_btn_reservation);
        performCodeWithPermission("需要定位权限", new PermissionCallback() {
            @Override
            public void hasPermission() {
                setupMap(savedInstanceState);
            }

            @Override
            public void noPermission() {

            }
        }, per);
        return v;
    }


    private void setupMap(Bundle savedInstanceState) {
        map.onCreate(savedInstanceState);// 此方法必须重写
        if (aMap == null) {
            aMap = map.getMap();
        }
        aMap.setTrafficEnabled(false);// 显示实时交通状况
        //地图模式可选类型：MAP_TYPE_NORMAL,MAP_TYPE_SATELLITE,MAP_TYPE_NIGHT
        aMap.setMapType(AMap.MAP_TYPE_NORMAL);
//        MyLocationStyle myLocationStyle;
//        myLocationStyle = new MyLocationStyle();//初始化定位蓝点样式类myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);//连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）如果不设置myLocationType，默认也会执行此种模式。
//        myLocationStyle.interval(2000); //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATE);//定位一次，且将视角移动到地图中心点。
//        aMap.setMyLocationStyle(myLocationStyle);//设置定位蓝点的Style
////        aMap.getUiSettings().setMyLocationButtonEnabled(false);//设置默认定位按钮是否显示，非必需设置。
//        aMap.setMyLocationEnabled(true);// 设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false。
////        myLocationStyle.showMyLocation(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //在activity执行onDestroy时执行mMapView.onDestroy()，销毁地图
        map.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //在activity执行onResume时执行map.onResume ()，重新绘制加载地图
        map.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //在activity执行onPause时执行map.onPause ()，暂停地图的绘制
        map.onPause();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //在activity执行onSaveInstanceState时执行map.onSaveInstanceState (outState)，保存地图当前的状态
        map.onSaveInstanceState(outState);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.focus:
                if (is_collect == 0) {
                    focus.setImageResource(R.mipmap.focused);
                } else {
                    focus.setImageResource(R.mipmap.unfocus);
                }
                Toast.makeText(getApplicationContext(), "操作成功", Toast.LENGTH_SHORT).show();
                ifFocus(is_collect, getIntent().getIntExtra("siteid", 0) + "");
                break;
        }

    }

    private void ifFocus(final int is_collect, String siteid) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                String url = "";
                if (is_collect == 0)
                    url = collect;
                else
                    url = collectDelete;
                FormBody.Builder builder = new FormBody.Builder()
                        .add("service_id", getIntent().getIntExtra("siteid", 0) + "")
                        .add("uid", UserInfoManger.userInfo.getUid() + "")
                        .add("token", UserInfoManger.userInfo.getUserToken());
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(url).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "收藏或取消收藏:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        String data = object.getString("data");
                        Message message = new Message();
                        message.what = 2;
                        Bundle bundle = new Bundle();
                        bundle.putString("error_code", error_code);
                        bundle.putString("data", data);
                        bundle.putInt("isFocus", is_collect);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    Log.i("yudan", "收藏或取消收藏:" + e);
                    closeDialog();
                    e.printStackTrace();
                    Message message = new Message();
                    message.what = 7;
                    handler.sendMessage(message);
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "收藏或取消收藏:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private class Voice2Adpater extends BaseQuickAdapter<ServiceDetailsBean, BaseViewHolder> {

        public Voice2Adpater(int layoutResId, @Nullable List<ServiceDetailsBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, final ServiceDetailsBean item) {
            CircleImageView user_img;
            TextView name, date, content;
            SimpleRatingBar rb_star;
            NoScrollView gv_img;
            user_img = (CircleImageView) helper.getView(R.id.user_img);
            name = (TextView) helper.getView(R.id.name);
            date = (TextView) helper.getView(R.id.date);
            content = (TextView) helper.getView(R.id.content);
            rb_star = (SimpleRatingBar) helper.getView(R.id.rb_star);
            gv_img = helper.getView(R.id.gv_img);
            name.setText(item.getUsername());
            date.setText(item.getCommenttime());
            content.setText(item.getContent());
            rb_star.setRating(Float.parseFloat(item.getStarnum()));
            Glide.with(ServiceDetailsActivity.this).load(item.getAvatar()).into(user_img);
            gv_img.setAdapter(new BaseAdapter() {
                @Override
                public int getCount() {
                    Log.i("yudan", item.getCommentimg().size() + "");
                    return item.getCommentimg() == null ? 0 : item.getCommentimg().size();
                }

                @Override
                public Object getItem(int i) {
                    return item.getCommentimg().get(i);
                }

                @Override
                public long getItemId(int i) {
                    return i;
                }

                @Override
                public View getView(int i, View view, ViewGroup viewGroup) {
                    View v = View.inflate(ServiceDetailsActivity.this, R.layout.img, null);
                    ImageView image = v.findViewById(R.id.image);
                    Glide.with(ServiceDetailsActivity.this).load(item.getCommentimg().get(i)).into(image);
                    return v;
                }
            });

        }
    }

    /**
     * 获取服务者详情
     */
    public void initData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder()
                        .add("siteid", getIntent().getIntExtra("siteid", 0) + "")
                        .add("uid", UserInfoManger.userInfo.getUid() + "");
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(serverinfo).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "获取服务者详情:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 0;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            String toptitle = data.getString("toptitle");
                            String sitetitle = data.getString("sitetitle");
                            String siteid = data.getString("siteid");
                            String sitedesc = data.getString("sitedesc");
                            String starnum = data.getString("starnum");
                            String commentnum = data.getString("commentnum");
                            String price = data.getString("price");
                            String is_collect = data.getString("is_collect");
                            String lat = data.getString("lat");
                            String lnt = data.getString("lnt");
                            String mobile = data.getString("mobile");
                            String address = data.getString("address");
                            String slideimg = data.getString("slideimg");
                            String respons = data.getString("response");
                            bundle.putString("response", respons);
                            bundle.putString("sitetitle", sitetitle);
                            bundle.putString("siteid", siteid);
                            bundle.putString("sitedesc", sitedesc);
                            bundle.putString("starnum", starnum);
                            bundle.putString("commentnum", commentnum);
                            bundle.putString("price", price);
                            bundle.putString("is_collect", is_collect);
                            bundle.putString("lat", lat);
                            bundle.putString("lnt", lnt);
                            bundle.putString("mobile", mobile);
                            bundle.putString("address", address);
                            bundle.putString("slideimg", slideimg);
                            bundle.putString("toptitle", toptitle);
                            JSONArray commentlist = data.getJSONArray("commentlist");
                            for (int i = 0; i < commentlist.length(); i++) {
                                JSONObject jsonObject = commentlist.getJSONObject(i);
                                ServiceDetailsBean bean = new ServiceDetailsBean();
                                bean.setAvatar(jsonObject.getString("avatar"));
                                JSONArray commentimg = jsonObject.getJSONArray("commentimg");
                                List<String> commentimgList = new ArrayList<>();
                                for (int j = 0; j < commentimg.length(); j++) {
                                    commentimgList.add(commentimg.getString(j));
                                }
                                bean.setCommentimg(commentimgList);
                                bean.setCommenttime(jsonObject.getString("commenttime"));
                                bean.setUsername(jsonObject.getString("username"));
                                bean.setStarnum(jsonObject.getString("starnum"));
                                bean.setContent(jsonObject.getString("content"));
                                data2.add(bean);
                            }

                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    Log.i("yudan", "获取服务者详情:" + e);
                    closeDialog();
                    e.printStackTrace();
                    Message message = new Message();
                    message.what = 7;
                    handler.sendMessage(message);
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "获取服务者详情:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * 确认预约
     */
    public void sureToOrder(final String lat, final String lnt, final String address) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder()
                        .add("siteid", getIntent().getIntExtra("siteid", 0) + "")
                        .add("uid", UserInfoManger.userInfo.getUid() + "")
                        .add("lat", lat)
                        .add("lnt", lnt)
                        .add("address", address);
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(book).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "确认预约:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            String respons = data.getString("response");
                            bundle.putString("response", respons);
                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    Log.i("yudan", "确认预约:" + e);
                    closeDialog();
                    e.printStackTrace();
                    Message message = new Message();
                    message.what = 7;
                    handler.sendMessage(message);
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "确认预约:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
