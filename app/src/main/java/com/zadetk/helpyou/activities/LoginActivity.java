package com.zadetk.helpyou.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.hyphenate.EMCallBack;
import com.hyphenate.EMMessageListener;
import com.hyphenate.chat.EMClient;
import com.hyphenate.chat.EMMessage;
import com.tencent.mm.opensdk.modelmsg.SendAuth;
import com.tencent.mm.opensdk.openapi.IWXAPI;
import com.tencent.mm.opensdk.openapi.WXAPIFactory;
import com.zadetk.helpyou.App;
import com.zadetk.helpyou.MessageEvents;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.UserLoginBean;
import com.zadetk.helpyou.im.EventMsg;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.MD5;
import com.zadetk.helpyou.utils.SPUtil;
import com.zadetk.helpyou.utils.ToastUtil;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import cn.jpush.android.api.JPushInterface;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.isWxLogin;
import static com.zadetk.helpyou.Urls.skilldetail;
import static com.zadetk.helpyou.other.Const.USER_IS_LOGIN;

/**
 * 2-1 登录页面
 */
public class LoginActivity extends AbsActivity implements View.OnClickListener {
    private EditText etPhonenumber;
    private EditText etPassword;
    private TextView tvBtnLogin;
    private TextView tvForgetpassword;
    private TextView tvRegsister;
    private ImageView ivBack;
    private TextView wx_login;
    private IWXAPI api;

    private void initViews() {
        setTopTitle("登录");
        etPhonenumber = findViewById(R.id.et_phonenumber);
        etPassword = findViewById(R.id.et_password);
        tvBtnLogin = findViewById(R.id.tv_btn_login);
        tvForgetpassword = findViewById(R.id.tv_forgetpassword);
        tvRegsister = findViewById(R.id.tv_regsister);
        ivBack = findViewById(R.id.iv_back);
        wx_login = findViewById(R.id.wx_login);
        //TODO 测试代码
//        etPhonenumber.setText("18642036697");
        etPhonenumber.setText("15940070932");
        etPassword.setText("123456");
//        etPassword.setText("456789");
    }

    @Override
    public int setView() {
        return R.layout.activity_login;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        registToWX();
        initViews();
        initLogic();
    }

    private void initLogic() {
        tvBtnLogin.setOnClickListener(this);
        tvForgetpassword.setOnClickListener(this);
        tvRegsister.setOnClickListener(this);
        ivBack.setOnClickListener(this);
        wx_login.setOnClickListener(this);
    }

    private void registToWX() {
        //AppConst.WEIXIN.APP_ID是指你应用在微信开放平台上的AppID，记得替换。
        api = WXAPIFactory.createWXAPI(this, Const.APP_ID, false);
        // 将该app注册到微信
        api.registerApp(Const.APP_ID);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(LoginActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                finish();
                break;
            case R.id.tv_btn_login:
                login();
                break;
            case R.id.tv_forgetpassword:
                startActivity(new Intent(this, ForgetPasswordActivity.class));
                break;
            case R.id.tv_regsister:
                startActivity(new Intent(this, RegsisterActivity.class));
                break;
            case R.id.wx_login:
                SendAuth.Req req = new SendAuth.Req();
                req.scope = "snsapi_userinfo";//
//                req.scope = "snsapi_login";//提示 scope参数错误，或者没有scope权限
                req.state = "wechat_sdk_微信登录";
                api.sendReq(req);
                break;
        }
    }

    private void login() {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(LoginActivity.this.getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        String password = etPassword.getText().toString();
        String username = etPhonenumber.getText().toString();
        if (TextUtils.isEmpty(username) || TextUtils.isEmpty(password)) {
            ToastUtil.showToast("请输入完整信息！");
            return;
        }
        Map<String, Object> param = NetTool.newParams();
        param.put("username", username);
        param.put("password", MD5.getStringMD5(password));
        NetTool.getApi().login(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<UserLoginBean>>() {
                    @Override
                    public void onData(BaseResponse<UserLoginBean> value) {
                        if (value.getCode() == 0) {
                            final UserLoginBean user = value.getData();
                            UserInfoManger.login(user.getToken(), user.getUsername(), user.getUid());
                            EMClient.getInstance().login(user.getUsername(), user.getUsername(), new EMCallBack() {//回调
                                @Override
                                public void onSuccess() {
                                    EMClient.getInstance().groupManager().loadAllGroups();
                                    EMClient.getInstance().chatManager().loadAllConversations();
                                    EMClient.getInstance().chatManager().addMessageListener(msgListener);
                                    Log.d("1", "登录聊天服务器成功！");
                                }

                                @Override
                                public void onProgress(int progress, String status) {

                                }

                                @Override
                                public void onError(int code, String message) {
                                    Log.d("1", "登录聊天服务器失败！");
                                }
                            });

                            new Thread(new Runnable() {

                                @Override
                                public void run() {
                                    JPushInterface.resumePush(getApplicationContext());
                                    // 绑定别名
                                    String tel = user.getUsername() + "";
                                    if (!tel.equals("")) {
                                        setAlias(tel);
                                    }
                                }
                            }).start();
                            App.finishAllActivity();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                        } else {
                            LogUtil.e("登陆失败 " + value.getMessage());
                            ToastUtil.showToast("登陆失败 " + value.getMessage());
                        }
                    }
                });
    }

    EMMessageListener msgListener = new EMMessageListener() {

        @Override
        public void onMessageReceived(List<EMMessage> messages) {
            Log.e("~~msgListener1~~", "");
            int count = EMClient.getInstance().chatManager().getUnreadMsgsCount();
            EventBus.getDefault().post(new EventMsg(count));
        }

        @Override
        public void onCmdMessageReceived(List<EMMessage> messages) {
            //收到透传消息
            Log.e("~~msgListener1~~", "2");
        }

        @Override
        public void onMessageRead(List<EMMessage> messages) {
            //收到已读回执
            Log.e("~~msgListener1~~", "2");

        }

        @Override
        public void onMessageDelivered(List<EMMessage> message) {
            //收到已送达回执
            Log.e("~~msgListener1~~", "2");

        }

        @Override
        public void onMessageRecalled(List<EMMessage> messages) {
            //消息被撤回
            Log.e("~~msgListener1~~", "2");

        }

        @Override
        public void onMessageChanged(EMMessage message, Object change) {
            //消息状态变动
            Log.e("~~msgListener1~~", "2");

        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN, sticky = true)
    public void onStickyEvent(MessageEvents events) {
        switch (events.getEventsType()) {
            case MessageEvents.WXLOGIN_SUCCESS:

                break;
            default:
                break;
        }
    }
}

