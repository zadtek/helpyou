package com.zadetk.helpyou.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.zadetk.helpyou.App;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.GetarticlelistBean;
import com.zadetk.helpyou.bean.LogoutResponseBean;
import com.zadetk.helpyou.bean.SwitchMessageBean;
import com.zadetk.helpyou.bean.UpdateInfo;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.LoadApkAsyncTask;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.DataCleanManager;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.SPUtil;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zadetk.helpyou.utils.VersionUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cn.jpush.android.api.JPushInterface;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.getversion;
import static com.zadetk.helpyou.Urls.skilldetail;
import static com.zadetk.helpyou.activities.WebViewActivity.TagertUrl;
import static com.zadetk.helpyou.activities.WebViewActivity.Title;
import static com.zadetk.helpyou.other.Const.USER_IS_LOGIN;

/**
 * 5-0设置页面
 */
public class SettingActivity extends AbsActivity implements View.OnClickListener {
    private static final String TAG = SettingActivity.class.getName();
    private TextView tvAbout;
    private TextView tvUserguide;
    private TextView achor;
    private Switch sw;
    private TextView tvUpdate;
    private TextView tvClean;
    private TextView tvLogout;
    private LinearLayout llClean;
    //        private UpdateInfo update_info;
    private String url = "";
    private String status;
    private Bundle updateBundle;
    private ProgressDialog dialog;
    private List<GetarticlelistBean> data = new LinkedList<>();
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String error_code = bundle.getString("error_code");
            switch (msg.what) {
                case 0:
                    if (error_code.equals("0")) {
                        updateBundle = bundle;
                        int localVersionCode = VersionUtils.getAppVersionCode(SettingActivity.this);
                        int remoteVersionCode = bundle.getInt("remoteVersionCode");
                        if (remoteVersionCode > localVersionCode) {
                            showUpdateDialog(bundle, "版本升级", "发现新版本！");
                        } else {
                            ToastUtil.showToast("已经是最新版本！");
                        }
                    } else {
                        ToastUtil.showToast(bundle.getString("message"));
                    }
                    closeDialog();
                    break;
            }
        }
    };

    protected Handler loadApkHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case LoadApkAsyncTask.LOAD_APK:
                    if ((Boolean) msg.obj) {
                        VersionUtils.installApk(SettingActivity.this, new File(
                                Const.getSavePath(SettingActivity.this), LoadApkAsyncTask.UPDATE_APK_NAME));
                    } else {
                        if (updateBundle != null)
                            showUpdateDialog(updateBundle, "错误提示", "下载失败，请重新下载？");
                    }
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    public int setView() {
        return R.layout.activity_setting;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();
        SharedPreferences sPreferences = getSharedPreferences("STATUS", MODE_PRIVATE);
        status = sPreferences.getString("receive", "");
//        Log.i(TAG, "=====caizhijia====111====sharedpreferences ===="+status);
        if (status.equals("1")) {
            sw.setChecked(true);
            setReceiveNews();
        }
        if (status.equals("2")) {
            sw.setChecked(false);
            setReceiveNews();
        }
    }


    private void initLogic() {
        tvLogout.setOnClickListener(this);
        tvUpdate.setOnClickListener(this);
        llClean.setOnClickListener(this);
        tvAbout.setOnClickListener(this);
        sw.setOnClickListener(this);
        tvUserguide.setOnClickListener(this);

        try {
            String size = DataCleanManager.getTotalCacheSize(this);
            tvClean.setText(size);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initViews() {
        setTopTitle("设置");
        tvAbout = (TextView) findViewById(R.id.tv_about);
        tvUserguide = (TextView) findViewById(R.id.tv_userguide);
        achor = (TextView) findViewById(R.id.achor);
        sw = (Switch) findViewById(R.id.sw);
        tvUpdate = (TextView) findViewById(R.id.tv_update);
        tvClean = (TextView) findViewById(R.id.tv_clean);
        tvLogout = (TextView) findViewById(R.id.tv_logout);
        llClean = findViewById(R.id.ll_clean);
        sw = findViewById(R.id.sw);
        tvUserguide = findViewById(R.id.tv_userguide);

        if (!UserInfoManger.isLogin) {
            tvLogout.setText("登录");
        }
    }

    @Override
    public void onClick(View view) {
        Intent intent;
        switch (view.getId()) {

            case R.id.tv_logout:
                logout();
                break;
            case R.id.tv_update:
                dialog = ProgressDialog.show(SettingActivity.this, "", "正在检查新版本……", true, false, null);
                updateApk();
                break;
            case R.id.ll_clean:
                DataCleanManager.clearAllCache(this);
                ToastUtil.showToast("已清除！");
                tvClean.setText("0k");
                break;
            case R.id.tv_about:


                intent = new Intent(SettingActivity.this, AboutActivity.class);
                startActivity(intent);
                break;
            case R.id.tv_userguide:

                intent = new Intent(SettingActivity.this, UserGuideActivity.class);
                startActivity(intent);

                break;
            case R.id.sw:

                if (sw.isChecked()) {
                    SharedPreferences sPreferences = getSharedPreferences("STATUS", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sPreferences.edit();
                    editor.putString("receive", "1");
                    //切记最后要使用commit方法将数据写入文件
                    status = "1";
                    editor.commit();
                    setReceiveNews();
                } else {
                    SharedPreferences sPreferences = getSharedPreferences("STATUS", MODE_PRIVATE);
                    SharedPreferences.Editor editor = sPreferences.edit();
                    editor.putString("receive", "2");
                    status = "2";
                    editor.commit();
                    setReceiveNews();
                }
                break;
        }
    }


    private void setReceiveNews() {
        SharedPreferences spf = getSharedPreferences("STATUS", MODE_PRIVATE);
        String str = spf.getString("receive", " ");
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        param.put("status", str);
//        Log.i(TAG, "=====caizhijia========sharedpreferences ===="+status);

        NetTool.getApi().getSwitchMessage(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<SwitchMessageBean>>() {
                    @Override
                    public void onData(BaseResponse<SwitchMessageBean> value) {

                        if (value.getCode() == 0) {
                            SwitchMessageBean user = value.getData();
//                            Log.i(TAG, "=====caizhijia====user========"+user.getStatus());

                        } else {
//                            Log.i(TAG, "=====caizhijia====user========"+value.getMessage());
                        }
                    }
                });

    }

    private void logout() {
        if (!UserInfoManger.isLogin) {
//            ToastUtil.showToast("你未登录！");
            Intent intent = new Intent(this, LoginActivity.class);
//            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
        } else {
            Map<String, Object> param = NetTool.newParams(SettingActivity.this);
            NetTool.getApi().logout(param)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(new BaseObserver<BaseResponse<LogoutResponseBean>>() {
                        @Override
                        public void onData(BaseResponse<LogoutResponseBean> value) {
                            if (value.getCode() == 0 && value.getData().getStatus() == 1) {


//                                JPushInterface.setAliasAndTags(
//                                        getApplicationContext(), "", null,
//                                        mAliasCallback);
                                // 退出账号将推送的通知清除
                               new Thread(new Runnable() {

                                   @Override
                                   public void run() {

                                       JPushInterface
                                               .clearAllNotifications(getApplicationContext());
                                       // 清除自定义消息
                                       NotificationManager mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
                                       mNotificationManager.cancelAll();

                                       //停止激光服务
                                       JPushInterface.stopPush(getApplicationContext());
                                   }
                               }).start();

                                SPUtil.saveDate(SettingActivity.this, Const.USER_TOKEN, "");
                                SPUtil.saveDate(SettingActivity.this, Const.USER_NAME, "");
                                SPUtil.saveDate(SettingActivity.this, USER_IS_LOGIN, false);
                                SPUtil.saveDate(SettingActivity.this, Const.USER_UID, -1);
                                UserInfoManger.isLogin = false;
                                App.finishAllActivity();
                                startActivity(new Intent(SettingActivity.this, MainActivity.class));
                                ToastUtil.showToast("退出成功！");

                            } else {
                                ToastUtil.showToast("退出登录失败" + value.getMessage());
                            }
                        }
                    });
        }

    }

    /**
     * 版本更新
     */
    private void updateApk() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();
                builder.add("uid", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .add("token", UserInfoManger.userInfo.getUserToken())
                        .add("platform", "2");
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(getversion).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "版本更新:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 0;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            int version = data.getInt("version");
                            bundle.putInt("remoteVersionCode", version);
                            bundle.putString("url", data.getString("url"));
                            bundle.putInt("is_force", data.getInt("is_force"));
                        } else {
                            bundle.putString("message", object.getString("message"));
                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    closeDialog();
                    Log.i("yudan", "版本更新:" + e);
                    e.printStackTrace();
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "版本更新:" + e);
                    e.printStackTrace();
                }
            }
        }).start();


    }

    protected void showUpdateDialog(final Bundle bundle, String titleStr, String msgStr) {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(SettingActivity.this);
        builder.setTitle(titleStr);
        builder.setMessage(msgStr);
        builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new LoadApkAsyncTask(SettingActivity.this, "下载中...", loadApkHandler)
                        .execute(bundle.getString("url"),
                                String.valueOf(bundle.getInt("remoteVersionCode")));
            }
        });

        if (bundle.getInt("is_force") == 2) {
            builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
        }
        Dialog dialog = builder.create();
        dialog.show();
    }

    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
