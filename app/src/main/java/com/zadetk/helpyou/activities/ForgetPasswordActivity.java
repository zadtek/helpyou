package com.zadetk.helpyou.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.zadetk.helpyou.App;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.SendBean;
import com.zadetk.helpyou.bean.UserLoginBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.MD5;
import com.zadetk.helpyou.utils.SPUtil;
import com.zadetk.helpyou.utils.ToastUtil;

import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.zadetk.helpyou.other.Const.USER_IS_LOGIN;

/**
 * 2-3密码找回页面
 */
public class ForgetPasswordActivity extends AbsActivity implements View.OnClickListener {
    private EditText etPhonenumber;
    private EditText etVerifycode;
    private TextView tvVirifycode;
    private EditText etPassword;
    private EditText etConfirmpassword;
    private TextView tvBtnResetPassword;
    private ImageView ivBack;
    private CountDownTimer countDownTimer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long l) {
            int ms = (int) (l / 1000);
            tvVirifycode.setText(ms + "s");
            tvVirifycode.setClickable(false);
        }

        @Override
        public void onFinish() {
            tvVirifycode.setText("发送验证码");
            tvVirifycode.setClickable(true);
        }
    };

    private void initViews() {
        etPhonenumber = (EditText) findViewById(R.id.et_phonenumber);
        etVerifycode = (EditText) findViewById(R.id.et_verifycode);
        tvVirifycode = (TextView) findViewById(R.id.tv_virifycode);
        etPassword = (EditText) findViewById(R.id.et_password);
        etConfirmpassword = (EditText) findViewById(R.id.et_confirmpassword);
        tvBtnResetPassword = (TextView) findViewById(R.id.tv_btn_reset);
        ivBack = findViewById(R.id.iv_back);
        setTopTitle("密码找回");
    }

    @Override
    public int setView() {
        return R.layout.activity_forget_password;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();
    }

    private void initLogic() {
        tvBtnResetPassword.setOnClickListener(this);
        tvVirifycode.setOnClickListener(this);
        ivBack.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.iv_back:
                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(ForgetPasswordActivity.this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                finish();
                break;
            case R.id.tv_btn_reset:
                resetPassword();
                break;
            case R.id.tv_virifycode:
                sendSmscode();
                break;
        }
    }

    private void resetPassword() {
        String phone = etPhonenumber.getText().toString();
        String verifycode = etVerifycode.getText().toString();
        String password = etPassword.getText().toString();
        String repassword = etConfirmpassword.getText().toString();
        if (TextUtils.isEmpty(phone) || TextUtils.isEmpty(verifycode) || TextUtils.isEmpty(password) || TextUtils.isEmpty(repassword)) {
            ToastUtil.showToast("请完善信息!");
            return;
        }
        Map<String, Object> param = NetTool.newParams();
        param.put("mobile", phone);
        param.put("password", MD5.getStringMD5(password));
        param.put("verifycode", verifycode);
        param.put("repassword", MD5.getStringMD5(repassword));
        NetTool.getApi().resetPassword(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<UserLoginBean>>() {
                    @Override
                    public void onData(BaseResponse<UserLoginBean> value) {
                        if (value.getCode() == 0) {
                            SPUtil.saveDate(ForgetPasswordActivity.this, Const.USER_TOKEN, value.getData().getToken());
                            //SPUtil.saveDate(ForgetPasswordActivity.this, Const.USER_NAME,value.getData().getUsername());
                            SPUtil.saveDate(ForgetPasswordActivity.this, Const.USER_UID, value.getData().getUid());
                            SPUtil.saveDate(ForgetPasswordActivity.this, USER_IS_LOGIN, true);
                            UserInfoManger.isLogin = true;
                            App.finishAllActivity();
                            startActivity(new Intent(ForgetPasswordActivity.this, MainActivity.class));
                            ToastUtil.showToast("密码修改成功");
                        } else {
                            ToastUtil.showToast("失败！" + value.getMessage());
                        }
                    }
                });
    }

    private void sendSmscode() {
        String phone = etPhonenumber.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            ToastUtil.showToast("请输入手机号码！");
            return;
        }
        Map<String, Object> param = NetTool.newParams();
        param.put("phonenum", phone);
        NetTool.getApi().sendSmsCode(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<SendBean>>() {
                    @Override
                    public void onData(BaseResponse<SendBean> value) {
                        if (value.getCode() == 0 && value.getData().getFlag() == 1) {
                            ToastUtil.showToast("发送成功！");
                        } else {
                            ToastUtil.showToast("发送失败！");
                        }
                        countDownTimer.start();
                    }
                });
    }
}
