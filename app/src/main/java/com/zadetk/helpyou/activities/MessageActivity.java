package com.zadetk.helpyou.activities;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.zadetk.helpyou.App;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.fragments.RobOrderFragment;

import java.util.LinkedList;
import java.util.List;

/**
 * 聊天消息页面
 */
public class MessageActivity extends AbsActivity implements View.OnClickListener {
    private TextView tvSystemMessage;
    private TextView tvChat;
    private ViewPager viewpager;
    private pagerAdapter adapter;
    private List<Fragment> fragments = new LinkedList<>();

    private void initViews() {
        tvSystemMessage = (TextView) findViewById(R.id.tv_system_message);
        tvChat = (TextView) findViewById(R.id.tv_chat);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        setTopTitle("消息");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mToolbar.setElevation(0);
        }
    }

    @Override
    public int setView() {
        return R.layout.activity_message;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();
    }

    private void initLogic() {
        fragments.add(new RobOrderFragment());
        fragments.add(App.mIMKit.getConversationFragment());

        adapter = new pagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switchTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tvSystemMessage.setOnClickListener(this);
        tvChat.setOnClickListener(this);
    }

    private void switchTab(int position) {

        int greenColor = getResources().getColor(R.color.colorGreen);
        int greyColor = getResources().getColor(R.color.textColorGrey);
        Drawable down = getResources().getDrawable(R.drawable.arrow_down);
        down.setBounds(0, 0, down.getMinimumWidth(), down.getMinimumHeight());
        Drawable up = getResources().getDrawable(R.drawable.arrow_up_green);
        up.setBounds(0, 0, up.getMinimumWidth(), up.getMinimumHeight());

        tvSystemMessage.setTextColor(greyColor);
        tvSystemMessage.setCompoundDrawables(null, null, down, null);
        tvChat.setTextColor(greyColor);
        tvChat.setCompoundDrawables(null, null, down, null);

        switch (position) {
            case 0:
                tvSystemMessage.setTextColor(greenColor);
                tvSystemMessage.setCompoundDrawables(null, null, up, null);
                break;
            case 1:
                tvChat.setTextColor(greenColor);
                tvChat.setCompoundDrawables(null, null, up, null);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_system_message:
                switchTab(0);
                viewpager.setCurrentItem(0);
                break;
            case R.id.tv_chat:
                switchTab(1);
                viewpager.setCurrentItem(1);
                break;
        }
    }

    private class pagerAdapter extends FragmentPagerAdapter {

        public pagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }
}
