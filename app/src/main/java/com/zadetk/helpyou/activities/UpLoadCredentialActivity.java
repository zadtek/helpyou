package com.zadetk.helpyou.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.InfoItemBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.CmlRequestBody;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.saveinfo;

/**
 * 4-2-1证件上传页面
 */
public class UpLoadCredentialActivity extends AbsActivity {

    public static final int TypePerson = 1;
    public static final int TypeCompaney = 2;
    public static final String Type = "type";
    private int type;
    private ImageView iv1, iv2;
    private TextView tv_publish;
    private File file1, file2;
    private ProgressDialog dialog;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String error_code = bundle.getString("error_code");
            String data = bundle.getString("data");
            switch (msg.what) {
                case 0:
                    Toast.makeText(getApplicationContext(), data, Toast.LENGTH_SHORT).show();
                    if (error_code.equals("0"))
                        finish();
                    closeDialog();
                    break;
            }
        }
    };

    @Override
    public int setView() {
        return R.layout.activity_up_load_credential;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        type = getIntent().getIntExtra(Type, -1);
        initViews();
        initLogic();
    }

    private void initLogic() {
        iv1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performCodeWithPermission("需要相机和读写存储权限！", new PermissionCallback() {
                    @Override
                    public void hasPermission() {
//                        MultiImageSelector.create().showCamera(true).single().start(FullInfoActivity.this, 200);
                        photo(0);
                    }

                    @Override
                    public void noPermission() {

                    }
                }, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE});
            }
        });
        iv2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                performCodeWithPermission("需要相册权限！", new PermissionCallback() {
                    @Override
                    public void hasPermission() {
//                        MultiImageSelector.create().showCamera(true).single().start(UpLoadCredentialActivity.this, 301);
                        photo(1);
                    }

                    @Override
                    public void noPermission() {

                    }
                }, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA});
            }
        });
    }

    private void initViews() {
        tv_publish = findViewById(R.id.tv_publish);
        setTopTitle("上传证件");
        iv1 = findViewById(R.id.iv_1);
        iv2 = findViewById(R.id.iv_2);
        if (type == TypeCompaney) {
            Glide.with(this)
                    .load(UserInfoManger.userInfo.getCompanyidcard_front())
                    .placeholder(R.drawable.companey_front)
                    .error(R.drawable.companey_front)
                    .into(iv1);
            Glide.with(this)
                    .load(UserInfoManger.userInfo.getCompanyidcard_back())
                    .placeholder(R.drawable.companey_back)
                    .error(R.drawable.companey_back)
                    .into(iv2);
        } else {
            Glide.with(this)
                    .load(UserInfoManger.userInfo.getId_up())
                    .placeholder(R.drawable.id_up)
                    .error(R.drawable.id_up)
                    .into(iv1);
            Glide.with(this)
                    .load(UserInfoManger.userInfo.getId_down())
                    .placeholder(R.drawable.id_down)
                    .error(R.drawable.id_down)
                    .into(iv2);
        }
        tv_publish.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (file1 != null || file2 != null) {
                    dialog = ProgressDialog.show(UpLoadCredentialActivity.this, "", "正在提交……", true, false, null);
                    if (type == 1)
                        perfactInfo("id_up", "id_down");
                    else
                        perfactInfo("licence", "company_img");
                } else {
                    Toast.makeText(getApplicationContext(), "请选择图片", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    private void changeInfoRequest(String key, File file) {
        MultipartBody multipartBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addPart(NetTool.buildUploadMultipart(file, "value"))
                .addFormDataPart("key", key)
                .build();
        NetTool.getApi().saveUserInfo(multipartBody)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<InfoItemBean>>() {
                    @Override
                    public void onData(BaseResponse<InfoItemBean> value) {
                        if (value.getCode() == 0) {
                            switch (value.getData().getKey()) {
                                case Const.TYPE_COMPANEY_IMG:
                                    Glide.with(UpLoadCredentialActivity.this).load(value.getData().getValue()).into(iv1);
                                    UserInfoManger.userInfo.setCompanyidcard_front(value.getData().getValue());
                                    break;
                                case Const.TYPE_COMPANEY_LICENCE:
                                    Glide.with(UpLoadCredentialActivity.this).load(value.getData().getValue()).into(iv2);
                                    UserInfoManger.userInfo.setCompanyidcard_back(value.getData().getValue());
                                    break;
                                case Const.TYPE_ID_UP:
                                    Glide.with(UpLoadCredentialActivity.this).load(value.getData().getValue()).into(iv1);
                                    UserInfoManger.userInfo.setId_up(value.getData().getValue());
                                    break;
                                case Const.TYPE_ID_down:
                                    Glide.with(UpLoadCredentialActivity.this).load(value.getData().getValue()).into(iv2);
                                    UserInfoManger.userInfo.setId_down(value.getData().getValue());
                                    break;
                            }
                            ToastUtil.showToast("修改成功！");
                        } else {
                            ToastUtil.showToast(value.getMessage());
                        }
                    }
                });
    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        if (resultCode == RESULT_OK) {
//            ArrayList<String> list = data.getStringArrayListExtra(MultiImageSelector.EXTRA_RESULT);
//            LogUtil.d(list.get(0));
//            ToastUtil.showToast(list.get(0));
//            File file = new File(list.get(0));
//
//            if (requestCode == 300 && type == TypeCompaney) {
//                changeInfoRequest(Const.TYPE_COMPANEY_IMG, file);
//            } else if (requestCode == 301 && type == TypeCompaney) {
//                changeInfoRequest(Const.TYPE_COMPANEY_LICENCE, file);
//            } else if (requestCode == 300 && type == TypePerson) {
//                changeInfoRequest(Const.TYPE_ID_UP, file);
//            } else if (requestCode == 301 && type == TypePerson) {
//                changeInfoRequest(Const.TYPE_ID_down, file);
//            }
//
//        }
//    }

    private void photo(int position) {
        Glide.get(this.getApplicationContext()).clearMemory();
        Matisse.from(UpLoadCredentialActivity.this)
                .choose(MimeType.ofImage())//图片类型
                .countable(false)//true:选中后显示数字;false:选中后显示对号
                .maxSelectable(1)//可选的最大数
                .capture(true)//选择照片时，是否显示拍照
                .captureStrategy(new CaptureStrategy(true, "com.zadetk.helpyou"))//参数1 true表示拍照存储在共有目录，false表示存储在私有目录；参数2与 AndroidManifest中authorities值相同，用于适配7.0系统 必须设置
                .imageEngine(new GlideEngine())//图片加载引擎
                .forResult(position);//
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Bitmap bitmap = BitmapFactory.decodeFile(Matisse.obtainPathResult(data).get(0));
        if (resultCode == RESULT_OK) {
            if (requestCode == 0) {
                file1 = new File(Matisse.obtainPathResult(data).get(0));
                if (bitmap != null)
                    iv1.setImageBitmap(bitmap);
            } else {
                file2 = new File(Matisse.obtainPathResult(data).get(0));
                iv2.setImageBitmap(bitmap);
            }


        }
    }

    /**
     * 完善信息
     */
    private void perfactInfo(final String param1, final String param2) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = null;
                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("user_id", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .addFormDataPart("token", UserInfoManger.userInfo.getUserToken());
                if (file1 != null) {
                    RequestBody fileBody1 = RequestBody.create(MediaType.parse("application/octet-stream"), file1);
                    builder.addFormDataPart(param1, file1.getName(), fileBody1);
                }
                if (file2 != null) {
                    RequestBody fileBody2 = RequestBody.create(MediaType.parse("application/octet-stream"), file2);
                    builder.addFormDataPart(param2, file2.getName(), fileBody2);
                }
                requestBody = builder.build();
                Request.Builder request = new Request.Builder().url(saveinfo)
                        .header("Content-Type", "application/x-www-form-urlencoded").post(new CmlRequestBody(requestBody) {
                            @Override
                            public void loading(long current, long total, boolean done) {
                            }
                        });
                OkHttpClient client = new OkHttpClient();
                client.newBuilder().readTimeout(60000, TimeUnit.MILLISECONDS).build().newCall(request.build()).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.i("yudan", "完善信息:" + e);
                        closeDialog();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful() || response == null || response.body() == null) {
                            Toast.makeText(getApplicationContext(), "服务器繁忙", Toast.LENGTH_SHORT);
                            closeDialog();
                        } else {
                            String responseData = response.body().string();
                            Log.i("yudan", "完善信息:" + responseData);
                            try {
                                JSONObject object = new JSONObject(responseData);
                                String error_code = object.getString("error_code");
                                String data = object.getString("data");
                                Bundle bundle = new Bundle();
                                bundle.putString("error_code", error_code);
                                bundle.putString("data", data);
                                Message message = new Message();
                                message.what = 0;
                                message.setData(bundle);
                                handler.sendMessage(message);
                            } catch (JSONException e) {
                                Log.i("yudan", "编辑服务:" + e);
                                e.printStackTrace();
                            }


                        }
                    }

                });
            }
        }).start();
    }


    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
