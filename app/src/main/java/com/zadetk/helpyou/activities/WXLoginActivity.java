package com.zadetk.helpyou.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.zadetk.helpyou.App;
import com.zadetk.helpyou.MessageEvents;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.SendBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.SPUtil;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zadetk.helpyou.wxapi.WXEntryActivity;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.isWxLogin;
import static com.zadetk.helpyou.Urls.wxlogin;
import static com.zadetk.helpyou.other.Const.USER_IS_LOGIN;

public class WXLoginActivity extends AbsActivity implements View.OnClickListener {
    private ProgressDialog dialog;
    private TextView tv_virifycode, tv_btn_regsister;
    private EditText et_phonenumber, et_verifycode;
    private String access_token, openId;
    private CountDownTimer countDownTimer = new CountDownTimer(60000, 1000) {
        @Override
        public void onTick(long l) {
            int ms = (int) (l / 1000);
            tv_virifycode.setText(ms + "s");
            tv_virifycode.setClickable(false);
        }

        @Override
        public void onFinish() {
            tv_virifycode.setText("发送验证码");
            tv_virifycode.setClickable(true);
        }
    };
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            switch (msg.what) {
                case 0:
                    String nickname = bundle.getString("nickname");
                    String sex = bundle.getString("sex");
                    String headimgurl = bundle.getString("headimgurl");
                    ifLogin(nickname, sex, headimgurl);
                    break;
                case 1:
                    closeDialog();
                    String uid = bundle.getString("uid");
                    String username = bundle.getString("username");
                    String token = bundle.getString("token");
                    UserInfoManger.login(token, username, Integer.parseInt(uid));
                    App.finishAllActivity();
                    startActivity(new Intent(WXLoginActivity.this, MainActivity.class));
                    finish();
                    break;
            }
        }
    };

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
//            case R.id.iv_back:
//                InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
//                inputMethodManager.hideSoftInputFromWindow(RegsisterActivity.this.getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
//                finish();
//                break;
            case R.id.tv_virifycode:
                sendSmscode();
                break;
            case R.id.tv_btn_regsister:
                String mobile = et_phonenumber.getText().toString().trim();
                String authCode = et_verifycode.getText().toString().trim();
                if (mobile.equals("") || authCode.equals("")) {
                    Toast.makeText(getApplicationContext(), "请填写手机号和验证码", Toast.LENGTH_SHORT).show();
                } else {
                    dialog = ProgressDialog.show(WXLoginActivity.this, "", "正在登录……", true, false, null);
                    getUserInfo();
                }
                break;
        }
    }

    @Override
    public int setView() {
        return R.layout.activity_wxlogin;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        access_token = getIntent().getStringExtra("access_token");
        openId = getIntent().getStringExtra("openId");
        setTopTitle("绑定手机号");
        tv_virifycode = findViewById(R.id.tv_virifycode);
        et_phonenumber = findViewById(R.id.et_phonenumber);
        tv_btn_regsister = findViewById(R.id.tv_btn_regsister);
        et_verifycode = findViewById(R.id.et_verifycode);
        tv_virifycode.setOnClickListener(this);
        tv_btn_regsister.setOnClickListener(this);
    }

    private void sendSmscode() {
        String phone = et_phonenumber.getText().toString();
        if (TextUtils.isEmpty(phone)) {
            ToastUtil.showToast("请输入手机号码！");
            return;
        } else {
            if (!phoneCheck(Const.PHONE_CHECK, phone)) {
                ToastUtil.showToast("请输入正确的手机号码！");
            } else {
                Map<String, Object> param = NetTool.newParams();
                param.put("phonenum", phone);
                NetTool.getApi().sendSmsCode(param)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(new BaseObserver<BaseResponse<SendBean>>() {
                            @Override
                            public void onData(BaseResponse<SendBean> value) {
                                if (value.getCode() == 0 && value.getData().getFlag() == 1) {
                                    ToastUtil.showToast("发送成功！");
                                } else {
                                    ToastUtil.showToast("发送失败！");
                                }
                                startCount();
                            }
                        });
            }
        }
    }

    private void startCount() {
        countDownTimer.start();
    }

    public static boolean phoneCheck(String patttern, String username) {
        Pattern pattern = Pattern.compile(patttern);
        Matcher matcher = pattern.matcher(username);
        if (matcher.find()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 微信登录
     *
     * @param nickname
     * @param sex
     * @param headimgurl
     */
    private void ifLogin(final String nickname, final String sex, final String headimgurl) {
        final String mobile = et_phonenumber.getText().toString().trim();
        final String authCode = et_verifycode.getText().toString().trim();

        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();
                builder.add("openId", openId);
                builder.add("nickname", nickname);
                builder.add("sex", sex);
                builder.add("headimgurl", headimgurl);
                builder.add("mobile", mobile);
                builder.add("verifycode", authCode);
                builder.add("equip", getDeviceId(WXLoginActivity.this));
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(wxlogin).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "微信登录:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            String uid = data.getString("uid");
                            String username = data.getString("username");
                            String token = data.getString("token");
                            bundle.putString("uid", uid);
                            bundle.putString("username", username);
                            bundle.putString("token", token);
                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    closeDialog();
                    Log.i("yudan", "微信登录:" + e);
                    e.printStackTrace();
                    closeDialog();
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "微信登录:" + e);
                    e.printStackTrace();
                    closeDialog();
                }
            }
        }).start();
    }

    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * 微信登录获取用户信息
     */
    private void getUserInfo() {
        //获取授权
        StringBuffer loginUrl = new StringBuffer();
        loginUrl.append("https://api.weixin.qq.com/sns/userinfo")
                .append("?access_token=")
                .append(access_token)
                .append("&openid=")
                .append(openId)
                .append("&lang=")
                .append("zh_CN");
        OkHttpClient okHttpClient = new OkHttpClient();

        Request.Builder builder = new Request.Builder();
        Request request = builder.get().url(String.valueOf(loginUrl)).build();

        Call call = okHttpClient.newCall(request);
        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                closeDialog();
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                if (response.isSuccessful()) {
                    String responseData = response.body().string();
                    Log.i("yudan", "微信登录获取用户信息:" + responseData);
                    try {
                        JSONObject object = new JSONObject(responseData);
                        String nickname = object.getString("nickname");
                        String sex = object.getString("sex");
                        String province = object.getString("province");
                        String city = object.getString("city");
                        String country = object.getString("country");
                        String headimgurl = object.getString("headimgurl");
                        Message message = new Message();
                        message.what = 0;
                        Bundle bundle = new Bundle();
                        bundle.putString("nickname", nickname);
                        bundle.putString("sex", sex);
                        bundle.putString("province", province);
                        bundle.putString("city", city);
                        bundle.putString("country", country);
                        bundle.putString("headimgurl", headimgurl);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } catch (JSONException e) {
                        closeDialog();
                        e.printStackTrace();
                    }
                } else {
                    closeDialog();
                    throw new IOException("Unexpected code" + response);
                }
            }
        });
    }

    public String getDeviceId(Context context) {
        TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            return "";
        }
        String deviceId = telephonyManager.getDeviceId();
        return deviceId;
    }

}