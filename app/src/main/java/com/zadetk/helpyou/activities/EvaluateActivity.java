package com.zadetk.helpyou.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.goyourfly.multi_picture.ImageLoader;
import com.goyourfly.multi_picture.MultiPictureView;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.zadetk.helpyou.MessageEvent;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.adapter.MyAdapter;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.LabelBean;
import com.zadetk.helpyou.net.CmlRequestBody;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zadetk.helpyou.view.NoScrollView;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.commentinfo;
import static com.zadetk.helpyou.Urls.saveskill;
import static com.zadetk.helpyou.Urls.skilldetail;
import static com.zadetk.helpyou.Urls.submitcomment;

/**
 * 9-1评价页面
 */
public class EvaluateActivity extends AbsActivity {
    private ImageView ivAvatar;
    private TextView tvName;
    private TextView tvDesc;
    private TextView tvPrice;
    private TextView tvEvaluate;
    private NoScrollView gvContainer;
    private EditText etContent;
    private MultiPictureView multipleImage;
    private TextView tvBtnSubmit;
    private ArrayList<LabelBean> list = new ArrayList<>();
    private String orderid;
    private SimpleRatingBar rb_mark;
    private MyAdapter myAdapter;
    private ProgressDialog dialog;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String error_code = bundle.getString("error_code");
            switch (msg.what) {
                case 0:
                    if (error_code.equals("0")) {
                        String avatar = bundle.getString("avatar");
                        String count = bundle.getString("count");
                        String skill_title = bundle.getString("skill_title");
                        String star = bundle.getString("star");
                        String amount = bundle.getString("amount");
                        Glide.with(EvaluateActivity.this).load(avatar).into(ivAvatar);
                        tvDesc.setText("成交数量：" + count + "  " + "评价：" + star);
                        tvName.setText(skill_title);
                        rb_mark.setRating(Float.parseFloat("5"));
                        tvPrice.setText("￥" + amount);
                        myAdapter.notifyDataSetChanged();
                    }
                    break;
                case 1:
                    closeDialog();
                    if (error_code.equals("0")) {
                        String status = bundle.getString("status");
                        String respons = bundle.getString("respons");
                        if (status.equals("1")) {
                            Toast.makeText(getApplicationContext(), "提交成功", Toast.LENGTH_SHORT).show();
                            finish();
                        } else {
                            Toast.makeText(getApplicationContext(), respons, Toast.LENGTH_SHORT).show();

                        }
                    }
                    break;
            }
        }
    };

    private void initViews() {
        ivAvatar = (ImageView) findViewById(R.id.iv_avatar);
        tvName = (TextView) findViewById(R.id.tv_name);
        tvDesc = (TextView) findViewById(R.id.tv_desc);
        tvPrice = (TextView) findViewById(R.id.tv_price);
        tvEvaluate = (TextView) findViewById(R.id.tv_evaluate);
        gvContainer = (NoScrollView) findViewById(R.id.gv_container);
        etContent = (EditText) findViewById(R.id.et_content);
        multipleImage = (MultiPictureView) findViewById(R.id.multiple_image);
        tvBtnSubmit = (TextView) findViewById(R.id.tv_btn_submit);
        rb_mark = findViewById(R.id.rb_mark);
        setTopTitle("评价");
        orderid = getIntent().getStringExtra("orderid");
        initData();
    }

    @Override
    public int setView() {
        return R.layout.activity_evaluate;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();

    }

    private void initLogic() {
        MultiPictureView.setImageLoader(new ImageLoader() {
            @Override
            public void loadImage(ImageView image, Uri uri) {
                Glide.with(EvaluateActivity.this)
                        .load(uri)
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(image);
            }
        });
        myAdapter = new MyAdapter<LabelBean>(list, R.layout.tag_text) {

            @Override
            public void bindView(ViewHolder holder, final LabelBean obj) {
                TextView label_name = holder.getView(R.id.label_name);
                label_name.setText(obj.getName());
                if (obj.getSelected()) {
                    label_name.setBackgroundResource(R.drawable.border_red);
                } else {
                    label_name.setBackgroundResource(R.drawable.border_rad);
                }
                label_name.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (obj.getSelected()) {
                            obj.setSelected(false);
                            notifyDataSetChanged();
                        } else {
                            obj.setSelected(true);
                            notifyDataSetChanged();
                        }
                    }
                });
            }
        };
        gvContainer.setAdapter(myAdapter);

        multipleImage.setAddClickCallback(new MultiPictureView.AddClickCallback() {
            @Override
            public void onAddClick(View view) {
                performCodeWithPermission("需要相机权限", new PermissionCallback() {
                    @Override
                    public void hasPermission() {
                        MultiImageSelector.create().showCamera(true).multi().count(9).start(EvaluateActivity.this, 200);
                    }

                    @Override
                    public void noPermission() {

                    }
                }, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA});
            }
        });
        tvBtnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog = ProgressDialog.show(EvaluateActivity.this, "", "正在提交……", true, false, null);
                String comment_label_id = "";
                for (int i = 0; i < list.size(); i++) {
                    LabelBean bean = list.get(i);
                    if (bean.getSelected()) {
                        comment_label_id = bean.getId() + ",";
                    }
                }
                submitComment(comment_label_id);
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 200) {
            ArrayList<String> list = data.getStringArrayListExtra(MultiImageSelector.EXTRA_RESULT);
            for (String path : list) {
                File file = new File(path);
                Uri uri = Uri.fromFile(file);
                multipleImage.addItem(uri);

            }
        }
    }

    /**
     * 评价页信息
     */
    private void initData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();
                builder.add("uid", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .add("token", UserInfoManger.userInfo.getUserToken())
                        .add("orderid", orderid);
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(commentinfo).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "评价页信息:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 0;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            String avatar = data.getString("avatar");
                            String count = data.getString("count");
                            String skill_title = data.getString("skill_title");
                            String star = data.getString("star");
                            String amount = data.getString("amount");
                            JSONArray comment_label = data.getJSONArray("comment_label");
                            for (int i = 0; i < comment_label.length(); i++) {
                                JSONObject jsonObject = comment_label.getJSONObject(i);
                                LabelBean bean = new LabelBean();
                                bean.setId(jsonObject.getInt("id"));
                                bean.setName(jsonObject.getString("name"));
                                bean.setSelected(false);
                                list.add(bean);
                            }
                            bundle.putString("avatar", avatar);
                            bundle.putString("count", count);
                            bundle.putString("skill_title", skill_title);
                            bundle.putString("star", star);
                            bundle.putString("amount", amount);
                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    Log.i("yudan", "评价页信息:" + e);
                    e.printStackTrace();
                } catch (JSONException e) {
                    Log.i("yudan", "评价页信息:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }


    /**
     * 评价内容提交
     */
    private void submitComment(final String comment_label_id) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = null;
                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("uid", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .addFormDataPart("token", UserInfoManger.userInfo.getUserToken())
                        .addFormDataPart("content", etContent.getText().toString().trim())
                        .addFormDataPart("starnum", rb_mark.getRating() + "")
                        .addFormDataPart("orderid", orderid)
                        .addFormDataPart("comment_label_id", comment_label_id);
                if (multipleImage.getCount() > 0) {
                    for (int i = 0; i < multipleImage.getCount(); i++) {
                        File file = uriToFile(multipleImage.getList().get(i), EvaluateActivity.this);
                        RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), file);
                        builder.addFormDataPart("comment_pic[]", file.getName(), fileBody);
                    }
                }
                requestBody = builder.build();
                Request request = new Request.Builder().url(submitcomment).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "评价内容提交:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            String status = data.getString("status");
                            String respons = data.getString("prompt");
                            bundle.putString("status", status);
                            bundle.putString("respons", respons);
                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
//                        ToastUtil.showToast("提交成功！");
                        EventBus.getDefault().post(new MessageEvent("Hello EventBus!"));
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    closeDialog();
                    Log.i("yudan", "评价内容提交:" + e);
                    e.printStackTrace();
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "评价内容提交:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    public static File uriToFile(Uri uri, Context context) {
        String path = null;
        if ("file".equals(uri.getScheme())) {
            path = uri.getEncodedPath();
            if (path != null) {
                path = Uri.decode(path);
                ContentResolver cr = context.getContentResolver();
                StringBuffer buff = new StringBuffer();
                buff.append("(").append(MediaStore.Images.ImageColumns.DATA).append("=").append("'" + path + "'").append(")");
                Cursor cur = cr.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new String[]{MediaStore.Images.ImageColumns._ID, MediaStore.Images.ImageColumns.DATA}, buff.toString(), null, null);
                int index = 0;
                int dataIdx = 0;
                for (cur.moveToFirst(); !cur.isAfterLast(); cur.moveToNext()) {
                    index = cur.getColumnIndex(MediaStore.Images.ImageColumns._ID);
                    index = cur.getInt(index);
                    dataIdx = cur.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
                    path = cur.getString(dataIdx);
                }
                cur.close();
                if (index == 0) {
                } else {
                    Uri u = Uri.parse("content://media/external/images/media/" + index);
                    System.out.println("temp uri is :" + u);
                }
            }
            if (path != null) {
                return new File(path);
            }
        } else if ("content".equals(uri.getScheme())) {
            // 4.2.2以后
            String[] proj = {MediaStore.Images.Media.DATA};
            Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
            if (cursor.moveToFirst()) {
                int columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                path = cursor.getString(columnIndex);
            }
            cursor.close();

            return new File(path);
        } else {
            //Log.i(TAG, "Uri Scheme:" + uri.getScheme());
        }
        return null;
    }
    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}
