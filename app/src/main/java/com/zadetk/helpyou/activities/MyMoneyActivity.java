package com.zadetk.helpyou.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.MoneyItemBean;
import com.zadetk.helpyou.bean.MyMoneyResponseBean;
import com.zadetk.helpyou.bean.WithDrawResponseBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zadetk.helpyou.view.dialog.BaseNiceDialog;
import com.zadetk.helpyou.view.dialog.NiceDialog;
import com.zadetk.helpyou.view.dialog.ViewConvertListener;
import com.zadetk.helpyou.view.dialog.ViewHolder;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 7-0我的资产页面
 */
public class MyMoneyActivity extends AbsActivity implements View.OnClickListener {
    private RecyclerView recyclerView;
    private MyMoneyAdapter myMoneyAdapter;
    private List<MoneyItemBean> data = new LinkedList<>();
    private String leftMoney;
    private TextView tvMyMony;
    private LinearLayout tvSubmit;
    private int page = 1;
    private NiceDialog niceDialog;

    @Override
    public int setView() {
        return R.layout.activity_my_money;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        iniiViews();
        initLogic();
        requestData();
    }

    private void requestData() {
        Map<String, Object> param = NetTool.newParams(this);
        param.put("page", page);
        NetTool.getApi().getMyMoney(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<MyMoneyResponseBean>>() {
                    @Override
                    public void onData(BaseResponse<MyMoneyResponseBean> value) {
                        if (value.getCode() == 0) {
                            MyMoneyResponseBean da = value.getData();
                            leftMoney = da.getMoney();
                            if (da.getMoneyItemBeans().size() == 0) {
                                myMoneyAdapter.loadMoreEnd();
                            } else {
                                data.addAll(da.getMoneyItemBeans());
                                page++;
                                myMoneyAdapter.loadMoreComplete();
                                updateView();
                            }
                        } else {
                            ToastUtil.showToast(value.getMessage());
                        }
                    }
                });
    }

    private void updateView() {
        tvMyMony.setText(leftMoney);
        myMoneyAdapter.notifyDataSetChanged();
    }

    private void initLogic() {
        myMoneyAdapter = new MyMoneyAdapter(R.layout.mymoney_item, data);
        recyclerView.setAdapter(myMoneyAdapter);
        tvSubmit.setOnClickListener(this);
        myMoneyAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                requestData();
            }
        }, recyclerView);
        myMoneyAdapter.setEmptyView(R.layout.normal_empty_view);
    }

    private void iniiViews() {
        setTopTitle("我的资产");
        recyclerView = findViewById(R.id.recycleview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        tvSubmit = findViewById(R.id.ll_btn);
        tvMyMony = findViewById(R.id.tv_money);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ll_btn:
                showdialog();
                break;
        }
    }

    private void showdialog() {
        niceDialog = NiceDialog.init();
        niceDialog.setMargin(20);
        niceDialog.setLayoutId(R.layout.dialog)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    protected void convertView(ViewHolder var1, BaseNiceDialog var2) {
                        TextView btn = (TextView) var1.getView(R.id.tv_btn);
                        final EditText et_money = (EditText) var1.getView(R.id.et_money);
                        btn.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String money = et_money.getText().toString();
                                if (TextUtils.isEmpty(money)) {
                                    ToastUtil.showToast("请输入提现金额！");
                                    return;
                                }
                                withDraw(money);
                            }
                        });
                    }
                })
                .show(getSupportFragmentManager());
    }

    private void withDraw(String amount) {
        Map<String, Object> param = NetTool.newParams(this);
        param.put("amount", amount);
        NetTool.getApi().withDraw(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<WithDrawResponseBean>>() {
                    @Override
                    public void onData(BaseResponse<WithDrawResponseBean> value) {
                        if (value.getCode() == 0) {
                            ToastUtil.showToast("提现成功！");
                        } else {
                            ToastUtil.showToast(value.getMessage());
                        }
                        if (niceDialog != null) {
                            niceDialog.dismiss();
                        }
                    }
                });
    }

    private class MyMoneyAdapter extends BaseQuickAdapter<MoneyItemBean, BaseViewHolder> {

        public MyMoneyAdapter(int layoutResId, @Nullable List<MoneyItemBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, MoneyItemBean item) {
            helper.setText(R.id.tv_reason, item.getOrdertitle());
            helper.setText(R.id.tv_time, item.getChange_time());
            helper.setText(R.id.tv_money, item.getAmount());
        }
    }
}
