package com.zadetk.helpyou.activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.google.gson.JsonObject;
import com.zadetk.helpyou.ActivityUtils;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.adapter.ListViewAdapter;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.ServiceSub;
import com.zadetk.helpyou.bean.ServicetwocatBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.view.NoScrollView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class PublishServiceSubActivity extends AbsActivity {
    private static final String TAG = PublishServiceSubActivity.class.getName();

    private List<ServicetwocatBean> data = new LinkedList<>();
    private ListView mListView;
    private ListViewAdapter mLAdapter;
    private List<ServiceSub> re = new LinkedList<>();
    private TextView text;
    private ArrayList<ArrayList<HashMap<String, Object>>> mData;
    @Override
    public int setView() {
        return R.layout.activity_publish_service_sub;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        ActivityUtils.getInstance().addActivity("PublishServiceSubActivity", this);
        init();


        getServiceSubList();
    }
    private void init(){
        setTopTitle("选择服务项目");
        mListView = (ListView) findViewById(R.id.lv);


    }

    private void initlogic() {
        mLAdapter = new ListViewAdapter(data,PublishServiceSubActivity.this,re);
        Log.i(TAG, "=========3========"+ data.size());
        Log.i(TAG, "=========3========"+ re.size());
        mListView.setAdapter(mLAdapter);

    }
    // 获取二三级分类的数据
    private void getServiceSubList() {
        Intent intent = getIntent();
        String panrentid = intent.getStringExtra("panrentid");
        Map<String, Object> param = NetTool.newParams();
        param.put("parentcat",panrentid);
        NetTool.getApi().getServicetwocat(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<ServicetwocatBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<ServicetwocatBean>> value) {
                        if (value.getCode() == 0) {
                            data.clear();
                            List<ServicetwocatBean> user = value.getData();
                            data.addAll(user);
                            mData = new ArrayList<ArrayList<HashMap<String, Object>>>();
                            HashMap<String, Object> hashMap = null;
                            ArrayList<HashMap<String, Object>> sonData;
                            for(int i=1;i<data.size();i++){
                                re.addAll( data.get(i).getLevel());
                                sonData = new ArrayList<HashMap<String, Object>>();
                                for (int j=1; j<data.get(i).getLevel().size(); j++){
                                    hashMap = new HashMap<String, Object>();

                                    ServiceSub thirdtitle = new ServiceSub();
                                    thirdtitle = data.get(i).getLevel().get(j);
                                    String catname = thirdtitle.getCatname();
                                    hashMap.put("catname", catname);
                                    sonData.add(hashMap);
                                }
                                mData.add(sonData);
                            }
                            initlogic();
                        } else {
                            LogUtil.e(value.error_code + "sub: =======6======== error code");
                        }

                    }
                });




    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        ActivityUtils.getInstance().delActivity("PublishServiceSubActivity");
    }
}
