package com.zadetk.helpyou.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.gyf.barlibrary.ImmersionBar;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.bean.UpdateInfo;
import com.zadetk.helpyou.bean.VoiceBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.LoadApkAsyncTask;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.VersionUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.search;
import static com.zadetk.helpyou.Urls.start;

/**
 * 1-0启动页
 */
public class SplashActivity extends AppCompatActivity implements View.OnClickListener {
    private CountDownTimer countDownTimer;
    private ImmersionBar bar;
    private Handler handler;
    private UpdateInfo update_info;

    private TextView pass;
    Timer timer = new Timer();
    //    private Handler timeHandler;
//    private Runnable runnable;
    boolean clicked = true;
    private ImageView start_img;
    private Thread thread;
    private boolean intentBoolean = false;
    protected Handler loadApkHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what) {
                case LoadApkAsyncTask.LOAD_APK:
                    if ((Boolean) msg.obj) {
                        VersionUtils.installApk(SplashActivity.this, new File(
                                Const.getSavePath(SplashActivity.this), LoadApkAsyncTask.UPDATE_APK_NAME));
                    } else {
                        showUpdateDialog(update_info, "错误提示", "下载失败，请重新下载？");
                    }
                    break;
                default:
                    break;
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int flag = WindowManager.LayoutParams.FLAG_FULLSCREEN;
        //设置当前窗体为全屏显示
        getWindow().setFlags(flag, flag);
        setContentView(R.layout.activity_splash);
        initView();
        thread = new Thread(startImg);
        thread.start();
        Log.i("yudan", "线程开始");

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                Bundle bundle = msg.getData();
                String error_code = bundle.getString("error_code");
                switch (msg.what) {
                    case 0:
                        if (error_code.equals("0")) {
                            String imgurl = bundle.getString("imgurl");
                            String duration = bundle.getString("duration");
                            Log.i("yudan", "计时start");
                            countTime(Integer.parseInt(duration));
                            SplashActivity activity = SplashActivity.this;
                            if (activity != null && !activity.isDestroyed())
                                Glide.with(SplashActivity.this).load(imgurl).into(start_img);
                        }
                        break;
                }

            }
        };
        bar = ImmersionBar.with(this);
        bar.init();
        //updateApk();
        //       handler.sendEmptyMessageDelayed(0, 5000);
    }

    private void initView() {
        pass = findViewById(R.id.tv_pass);//跳过
        pass.setOnClickListener((View.OnClickListener) this);//跳过监听
        start_img = findViewById(R.id.start_img);
    }


    @Override
    protected void onDestroy() {
        if (bar != null) bar.destroy();
        super.onDestroy();
    }

    private void updateApk() {

        Map<String, Object> params = NetTool.newParams();
        //params.put("uid", AppPreferenceUtil.getUserId(mActivity));
        NetTool.getApi().checkUpdate(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<UpdateInfo>>() {
                    @Override
                    public void onData(BaseResponse<UpdateInfo> value) {
                        UpdateInfo info;
                        if (value != null && (info = value.getData()) != null) {
                            update_info = info;
                            int localVersionCode = VersionUtils.getAppVersionCode(SplashActivity.this);
                            int remoteVersionCode = info.getNumber();
                            if (remoteVersionCode > localVersionCode) {
                                showUpdateDialog(info, "版本升级", "发现新版本！");
                            } else {
                                handler.sendEmptyMessageDelayed(0, 2000);
                            }
                        }
                    }
                });
    }

    protected void showUpdateDialog(final UpdateInfo updateApkInfo, String titleStr, String msgStr) {
        android.app.AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle(titleStr);
        builder.setMessage(msgStr);
        builder.setPositiveButton("是", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                new LoadApkAsyncTask(SplashActivity.this, "下载中...", loadApkHandler)
                        .execute(updateApkInfo.getUrl(),
                                String.valueOf(updateApkInfo.getNumber()));
            }
        });

        if (updateApkInfo.getIsForce() == 2) {
            builder.setNegativeButton("否", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int whichButton) {
                    dialog.cancel();
                }
            });
        }
        Dialog dialog = builder.create();
        dialog.show();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_pass:
                //从闪屏界面跳转到首界面
                intentBoolean = true;
                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                startActivity(intent);
                finish();
                if (countDownTimer != null) {
                    countDownTimer.cancel();
                }
                break;
            default:
                break;
        }
    }

    /**
     * 启动页
     */
    Runnable startImg = new Runnable() {
        @Override
        public void run() {
            FormBody.Builder builder = new FormBody.Builder();
            RequestBody requestBody = builder.build();
            Request request = new Request.Builder().url(start).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
            try {
                Response response = new OkHttpClient().newCall(request).execute();
                if (response.isSuccessful()) {
                    String responseData = response.body().string();
                    Log.i("yudan", "启动页:" + responseData);
                    JSONObject object = new JSONObject(responseData);
                    String error_code = object.getString("error_code");//返回码
                    Message message = new Message();
                    message.what = 0;
                    Bundle bundle = new Bundle();
                    if (error_code.equals("0")) {
                        JSONObject data = object.getJSONObject("data");
                        String imgurl = data.getString("imgurl");
                        String duration = data.getString("duration");
                        bundle.putString("imgurl", imgurl);
                        bundle.putString("duration", duration);
                    }
                    bundle.putString("error_code", error_code);
                    message.setData(bundle);
                    handler.sendMessage(message);
                } else {
                    throw new IOException("Unexpected code" + response);
                }
            } catch (IOException e) {
                Log.i("yudan", "启动页:" + e);
                e.printStackTrace();
            } catch (JSONException e) {
                Log.i("yudan", "启动页:" + e);
                e.printStackTrace();
            }
        }
    };

    public void countTime(int recLen) {
        countDownTimer = new CountDownTimer(recLen * 1000, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                pass.setText("跳过" + millisUntilFinished / 1000);
            }

            @Override
            public void onFinish() {
                if (!intentBoolean) {
                    Log.i("yudan", "计时finish");
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                    startActivity(intent);
                    finish();
                }
            }
        }.start();
    }


}
