package com.zadetk.helpyou.activities;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.DeletesjobBean;
import com.zadetk.helpyou.bean.JoblistBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.ToastUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 6-2发布岗位页面
 */
public class MyJobActivity extends AbsActivity implements View.OnClickListener {
    private static final String TAG =MyJobActivity.class.getName() ;
    private TextView tvPublish;
    private RecyclerView recyclerView;
    private PublishJobAdapter serviceAdapter;
    private List<JoblistBean> data = new LinkedList<>();
    private List<DeletesjobBean> data2 = new LinkedList<>();

    @Override
    public int setView() {
        return R.layout.activity_my_job;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();
        getJoblist();
    }



    private void initLogic() {
        serviceAdapter = new PublishJobAdapter(R.layout.publish_service_item, data);
        serviceAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.iv_delete:
                        delete(data.get(position).getJobid());
                        Log.i(TAG, "=================" + data.get(position).getJobid());
                        break;
                    case R.id.iv_edit:
                        Intent intent = new Intent();
                        intent.setClass(MyJobActivity.this, PublishJobActivity.class);
                        intent.putExtra("jobid", data.get(position).getJobid());
                        startActivity(intent);
                        break;
                    case R.id.tv_title:
                        ToastUtil.showToast(data.get(position).getJobtitle());
                }
            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(serviceAdapter);
        tvPublish.setOnClickListener(this);
    }


// TODO: 2018/7/12 0012  暂无数据，待测试

    /**
     * @param getJobid 删除服务接口
     */
    private void delete(String getJobid) {
        final String string = getJobid;
        Dialog dialog = new AlertDialog.Builder(MyJobActivity.this)
                .setTitle("删除本条服务")
                .setMessage("确认删除吗？")
                //相当于点击确认按钮
                .setPositiveButton("确认", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Map<String, Object> param = NetTool.newParams();
                        param.put("uid", UserInfoManger.userInfo.getUid());
                        param.put("token", UserInfoManger.userInfo.getUserToken());
                        param.put("jobid", string);
                        NetTool.getApi().getDeletesjob(param)
                                .subscribeOn(Schedulers.io())
                                .observeOn(AndroidSchedulers.mainThread())
                                .subscribe(new BaseObserver<BaseResponse<List<DeletesjobBean>>>() {
                                    @Override
                                    public void onData(BaseResponse<List<DeletesjobBean>> value) {
                                        if (value.getCode() == 0) {
                                            ToastUtil.showToast("删除成功！");
                                            getJoblist();
                                        } else {
                                            LogUtil.e(value.error_code + " ========error code");
                                        }
                                    }
                                });


                    }
                })
                //相当于点击取消按钮
                .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .create();
        dialog.show();



    }

    private void initViews() {
        tvPublish = findViewById(R.id.tv_btn_publish);
        recyclerView = findViewById(R.id.recycleview);
        setTopTitle("我发布的岗位");
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_btn_publish:
                startActivity(new Intent(this, PublishJobActivity.class));
                break;
        }
    }

    private class PublishJobAdapter extends BaseQuickAdapter<JoblistBean, BaseViewHolder> {
        public PublishJobAdapter(int layoutResId, @Nullable List<JoblistBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, JoblistBean item) {
            helper.addOnClickListener(R.id.iv_delete);
            helper.addOnClickListener(R.id.iv_edit);
            helper.addOnClickListener(R.id.tv_title);
            helper.setText(R.id.tv_title, item.getJobtitle());
        }
    }

    // TODO: 2018/7/12 0012 后台暂无数据

    /**
     *获取岗位接口
     */
    private void getJoblist() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        Log.i(TAG, "================="+UserInfoManger.userInfo.getUserToken());

        NetTool.getApi().getJoblist(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<JoblistBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<JoblistBean>> value) {
                        if (value.getCode() == 0) {
                            data.clear();
                            List<JoblistBean> user = value.getData();
                            data.addAll(user);
                            Log.i(TAG, "================="+data);
                            serviceAdapter.notifyDataSetChanged();
                            Log.i(TAG, "================="+"有数据");
                            LogUtil.e(user + " " + value.getMessage());
                        } else {
                            LogUtil.e(value.error_code + " error code");
                            Log.i(TAG, "================="+"无数据");
                        }
                    }
                });
    }
}
