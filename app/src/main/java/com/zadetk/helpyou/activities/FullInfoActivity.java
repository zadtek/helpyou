package com.zadetk.helpyou.activities;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.InfoItemBean;
import com.zadetk.helpyou.bean.UserInfoBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.CmlRequestBody;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.Const;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zadetk.helpyou.view.dialog.BaseNiceDialog;
import com.zadetk.helpyou.view.dialog.NiceDialog;
import com.zadetk.helpyou.view.dialog.SingleDialog;
import com.zadetk.helpyou.view.dialog.ViewConvertListener;
import com.zadetk.helpyou.view.dialog.ViewHolder;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.saveinfo;
import static com.zadetk.helpyou.Urls.saveskill;

/**
 * 4-1完善信息页面
 */
public class FullInfoActivity extends AbsActivity implements View.OnClickListener {

    private TextView rightTv;
    private ImageView ivChooseImg;
    private EditText tvNickname, tvPhonenumber, tvCompanyname, tvCompanyphonenumber, tvCompaneyAddrss, tvAddress;
    private TextView tvSex, tvIdcard, tvCompaneyIdcard;
    private LinearLayout llAddress;
    private LinearLayout llSelfCredentials;
    private LinearLayout llCompanyAddress;
    private LinearLayout llCompanyCredentials;
    private UserInfoBean userInfoBean;
    //    final SingleDialog singleDialog = new SingleDialog();
    private File file;
    private int sex = 0;
    private ProgressDialog dialog;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String error_code = bundle.getString("error_code");
            String data = bundle.getString("data");
            switch (msg.what) {
                case 0:
                    Toast.makeText(getApplicationContext(), data, Toast.LENGTH_SHORT).show();
                    if (error_code.equals("0"))
                        finish();
                    closeDialog();
                    break;
            }
        }
    };

    @Override
    public int setView() {
        return R.layout.activity_full_info;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();
        requestData();

    }

    private void initDialog(final int sexInt) {
        Dialog dialog;
        final AlertDialog.Builder builder = new AlertDialog.Builder(FullInfoActivity.this);
//        builder.setIcon(R.drawable.ic_launcher);
        builder.setTitle("请选择性别");
        final String[] sexs = {"保密", "男", "女"};
        //    设置一个单项选择下拉框
        /**
         * 第一个参数指定我们要显示的一组下拉单选框的数据集合
         * 第二个参数代表索引，指定默认哪一个单选框被勾选上，1表示默认‘女‘ 会被勾选上
         * 第三个参数给每一个单选项绑定一个监听器
         */
        builder.setSingleChoiceItems(sexs, sex, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                sex = i;
//                Toast.makeText(FullInfoActivity.this, "性别为：" + sex[which], Toast.LENGTH_SHORT).show();
            }
        });
        builder.setPositiveButton("确定", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int i) {
                if (sex == 1) {
                    tvSex.setText("男");
                } else if (sex == 2) {
                    tvSex.setText("女");
                } else if (sex == 0) {
                    tvSex.setText("保密");
                }
            }
        });
        builder.setNegativeButton("取消", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                sex = sexInt;
                dialog.dismiss();
            }
        });
        dialog = builder.create();
        dialog.show();


//        final String[] item = new String[]{"男", "女", "保密"};
//        final StringBuilder selectedPayWay = new StringBuilder();
//
//        singleDialog.init("性别", item, "确定", "取消", new SingleDialog.SingleCallBack() {
//            @Override
//            public void onItems(DialogInterface dialogInterface, int i) {
//                selectedPayWay.delete(0, selectedPayWay.length());
//                selectedPayWay.append(item[i]);
//            }
//
//            @Override
//            public void onButton(DialogInterface dialogInterface, int i) {
//                if (i == 0) {
//                    tvSex.setText("男");
//                    sex = 1;
//                } else if (i == 1) {
//                    tvSex.setText("女");
//                    sex = 2;
//                } else if (i == 2) {
//                    tvSex.setText("保密");
//                    sex = 0;
//                }
//            }
//
//            @Override
//            public void onNegtiveButton(DialogInterface dialogInterface, int i) {
//            }
//        });
    }

    private void requestData() {
//        if (UserInfoManger.isFetchUserInfo) {
//            userInfoBean = UserInfoManger.userInfo;
//            upDateView();
//            return;
//        }
        Map<String, Object> param = NetTool.newParams(this);
        NetTool.getApi().getUserInfo(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<UserInfoBean>>() {
                    @Override
                    public void onData(BaseResponse<UserInfoBean> value) {
                        if (value.getCode() == 0) {
                            UserInfoManger.merage(value.getData(), UserInfoManger.userInfo);
                            userInfoBean = UserInfoManger.userInfo;
                            upDateView();
                        } else {
                            ToastUtil.showToast("获取用户信息出错！");
                            userInfoBean = new UserInfoBean();
                        }
                    }
                });
    }

    private void upDateView() {
        if (!TextUtils.isEmpty(userInfoBean.getNickname())) {
            tvNickname.setText(userInfoBean.getNickname());
        }
        if (!TextUtils.isEmpty(userInfoBean.getAvatar())) {
           try{
               Glide.with(this).load(userInfoBean.getAvatar()).into(ivChooseImg);
           }catch (Exception e){
               e.printStackTrace();
           }
        }
        sex = userInfoBean.getUsersex();
        if (userInfoBean.getUsersex() == 1) {
            tvSex.setText("男");
        } else if (userInfoBean.getUsersex() == 2) {
            tvSex.setText("女");
        } else if (userInfoBean.getUsersex() == 0) {
            tvSex.setText("保密");
        }
        if (!TextUtils.isEmpty(userInfoBean.getUserphone())) {
            tvPhonenumber.setText(userInfoBean.getUserphone());
        }
        if (!TextUtils.isEmpty(userInfoBean.getUseraddress())) {
            tvAddress.setText(userInfoBean.getAddress());
        }
        if (!TextUtils.isEmpty(userInfoBean.getCompanyname())) {
            tvCompanyname.setText(userInfoBean.getCompanyname());
        }
        if (!TextUtils.isEmpty(userInfoBean.getCompanyphone())) {
            tvCompanyphonenumber.setText(userInfoBean.getCompanyphone());
        }
        if (!TextUtils.isEmpty(userInfoBean.getCompanyaddress())) {
            tvCompaneyAddrss.setText(userInfoBean.getCompanyaddress());
        }

    }

    private void initLogic() {
        rightTv.setOnClickListener(this);
        llSelfCredentials.setOnClickListener(this);
        llCompanyCredentials.setOnClickListener(this);
        ivChooseImg.setOnClickListener(this);
        tvNickname.setOnClickListener(this);
        tvSex.setOnClickListener(this);
        tvPhonenumber.setOnClickListener(this);
        tvCompanyname.setOnClickListener(this);
        tvCompanyphonenumber.setOnClickListener(this);
        tvAddress.setOnClickListener(this);
    }

    private void initViews() {
        rightTv = mToolbar.findViewById(R.id.tv_righttext);
        rightTv.setVisibility(View.VISIBLE);
        ivChooseImg = (ImageView) findViewById(R.id.iv_choose_img);
        tvNickname = (EditText) findViewById(R.id.tv_nickname);
        tvSex = (TextView) findViewById(R.id.tv_sex);
        tvPhonenumber = (EditText) findViewById(R.id.tv_phonenumber);
        llAddress = (LinearLayout) findViewById(R.id.ll_address);
        llSelfCredentials = (LinearLayout) findViewById(R.id.ll_self_credentials);
        tvCompanyname = (EditText) findViewById(R.id.tv_companyname);
        tvCompanyphonenumber = (EditText) findViewById(R.id.tv_companyphonenumber);
        llCompanyAddress = (LinearLayout) findViewById(R.id.ll_company_address);
        llCompanyCredentials = (LinearLayout) findViewById(R.id.ll_company_credentials);
        tvAddress = findViewById(R.id.tv_address);
        tvCompaneyAddrss = findViewById(R.id.tv_companey_address);
        tvIdcard = findViewById(R.id.tv_idcard);
        tvCompaneyIdcard = findViewById(R.id.tv_companey_idcard);
        setTopTitle("完善信息");
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_righttext:
                String name = tvNickname.getText().toString().trim();
                String mobile = tvPhonenumber.getText().toString().trim();
                String address = tvAddress.getText().toString().trim();
                String companyName = tvCompanyname.getText().toString().trim();
                String companyPhone = tvCompanyphonenumber.getText().toString().trim();
                String companyAddress = tvCompaneyAddrss.getText().toString().trim();
                dialog = ProgressDialog.show(FullInfoActivity.this, "", "正在提交……", true, false, null);
                perfactInfo(name, mobile, address, companyName, companyPhone, companyAddress);
                break;
            case R.id.ll_company_credentials:
                startActivity(new Intent(this, UpLoadCredentialActivity.class).putExtra("type", 2));
                break;
            case R.id.ll_self_credentials:
                startActivity(new Intent(this, UpLoadCredentialActivity.class).putExtra("type", 1));
                break;
            case R.id.iv_choose_img:
                performCodeWithPermission("需要相机和读写存储权限！", new PermissionCallback() {
                    @Override
                    public void hasPermission() {
//                        MultiImageSelector.create().showCamera(true).single().start(FullInfoActivity.this, 200);
                        photo();
                    }

                    @Override
                    public void noPermission() {

                    }
                }, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE});
                break;
            case R.id.tv_nickname:
                showInputDialog(Const.TYPE_NAME);
                break;
            case R.id.tv_sex:
//                singleDialog.show(getFragmentManager(), "SingleDialog");
                initDialog(sex);
                break;
            case R.id.tv_phonenumber:
//                showInputDialog(Const.TYPE_PHONE);
                break;
            case R.id.tv_companyname:
//                showInputDialog(Const.TYPE_COMPANY_NAME);
                break;
            case R.id.tv_companyphonenumber:
//                showInputDialog(Const.TYPE_COMPANEY_PHONE);
                break;
            case R.id.tv_address:
                break;
        }
    }

    /**
     * 完善信息
     */
    private void perfactInfo(final String name, final String mobile, final String address, final String companyName, final String companyPhone, final String companyAddress) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = null;
                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("user_id", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .addFormDataPart("token", UserInfoManger.userInfo.getUserToken())
                        .addFormDataPart("nickname", name)
                        .addFormDataPart("sex", sex + "")
                        .addFormDataPart("mobile", mobile)
                        .addFormDataPart("address", address)
                        .addFormDataPart("companyname", companyName)
                        .addFormDataPart("companyaddr", companyPhone)
                        .addFormDataPart("companytell", companyAddress);
                if (file != null) {
                    RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), file);
                    builder.addFormDataPart("avatar", file.getName(), fileBody);
                }
                requestBody = builder.build();
                Request.Builder request = new Request.Builder().url(saveinfo)
                        .header("Content-Type", "application/x-www-form-urlencoded").post(new CmlRequestBody(requestBody) {
                            @Override
                            public void loading(long current, long total, boolean done) {
                            }
                        });
                OkHttpClient client = new OkHttpClient();
                client.newBuilder().readTimeout(60000, TimeUnit.MILLISECONDS).build().newCall(request.build()).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.i("yudan", "完善信息:" + e);
                        closeDialog();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful() || response == null || response.body() == null) {
                            Toast.makeText(getApplicationContext(), "服务器繁忙", Toast.LENGTH_SHORT);
                            closeDialog();
                        } else {
                            String responseData = response.body().string();
                            Log.i("yudan", "完善信息:" + responseData);
                            try {
                                JSONObject object = new JSONObject(responseData);
                                String error_code = object.getString("error_code");
                                String data = object.getString("data");
                                Bundle bundle = new Bundle();
                                bundle.putString("error_code", error_code);
                                bundle.putString("data", data);
                                Message message = new Message();
                                message.what = 0;
                                message.setData(bundle);
                                handler.sendMessage(message);
                            } catch (JSONException e) {
                                Log.i("yudan", "编辑服务:" + e);
                                e.printStackTrace();
                            }


                        }
                    }

                });
            }
        }).start();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            file = new File(Matisse.obtainPathResult(data).get(0));
            Bitmap bitmap = BitmapFactory.decodeFile(Matisse.obtainPathResult(data).get(0));
            if (bitmap != null)
                ivChooseImg.setImageBitmap(bitmap);
        }
    }


    private void showInputDialog(final String type) {
        final NiceDialog tempDialog = NiceDialog.init();

        tempDialog.setLayoutId(R.layout.commit_layout)
                .setConvertListener(new ViewConvertListener() {
                    @Override
                    public void convertView(ViewHolder holder, final BaseNiceDialog dialog) {
                        holder.setText(R.id.tv_title, type);
                        final EditText editText = (EditText) holder.getView(R.id.edit_input);
                        editText.post(new Runnable() {
                            @Override
                            public void run() {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.showSoftInput(editText, 0);
                            }
                        });
                        holder.setOnClickListener(R.id.tv_cancel_edit, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                tempDialog.dismiss();
                            }
                        });
                        holder.setOnClickListener(R.id.tv_confirm_edit, new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                String input = editText.getText().toString();
                                if (TextUtils.isEmpty(input)) {
                                    ToastUtil.showToast("输入点什么再确定吧！");
                                } else {
                                    changeInfoRequest(type, input);
                                    tempDialog.dismiss();
                                }
                            }
                        });
                    }
                })
                .setOutCancel(false)
                .setShowBottom(true)
                .show(getSupportFragmentManager());
    }

    private void changeInfoRequest(String key, String value) {
        Map<String, Object> param = NetTool.newParams(this);
        param.put("key", key);
        param.put("value", value);
        NetTool.getApi().saveUserInfo(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<InfoItemBean>>() {
                    @Override
                    public void onData(BaseResponse<InfoItemBean> value) {
                        if (value.getCode() == 0) {
                            switch (value.getData().getKey()) {
                                case Const.TYPE_NAME:
                                    tvNickname.setText(value.getData().getValue());
                                    userInfoBean.setNickname(value.getData().getValue());
                                    break;
                                case Const.TYPE_SEX:
                                    tvSex.setText(value.getData().getValue());
                                    //userInfoBean.setUsersex(value.getData().getValue());
                                    break;
                                case Const.TYPE_PHONE:
                                    tvPhonenumber.setText(value.getData().getValue());
                                    userInfoBean.setUserphone(value.getData().getValue());
                                    break;
                                case Const.TYPE_AVATOR:
                                    Glide.with(FullInfoActivity.this).load(value.getData().getValue()).into(ivChooseImg);
                                    userInfoBean.setAvatar(value.getData().getValue());
                                    break;
                                case Const.TYPE_COMPANY_NAME:
                                    tvCompanyname.setText(value.getData().getValue());
                                    userInfoBean.setCompanyname(value.getData().getValue());
                                    break;
                                case Const.TYPE_COMPANEY_ADDRESS:
                                    tvAddress.setText(value.getData().getValue());
                                    userInfoBean.setAddress(value.getData().getValue());
                                    break;
                                case Const.TYPE_COMPANEY_PHONE:
                                    tvCompanyphonenumber.setText(value.getData().getValue());
                                    userInfoBean.setCompanyphone(value.getData().getValue());
                                    break;
                                case Const.TYPE_ADDRESS:
                                    tvCompaneyAddrss.setText(value.getData().getValue());
                                    userInfoBean.setCompanyaddress(value.getData().getValue());
                                    break;
                            }
                            ToastUtil.showToast("修改成功！");
                        } else {
                            ToastUtil.showToast(value.getMessage());
                        }
                    }
                });
    }

//    private void changeInfoRequest(String key, File file) {
//        MultipartBody multipartBody = new MultipartBody.Builder()
//                .setType(MultipartBody.FORM)
//                .addPart(NetTool.buildUploadMultipart(file, "value"))
//                .addFormDataPart("key", key)
//                .build();
//        NetTool.getApi().saveUserInfo(multipartBody)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new BaseObserver<BaseResponse<InfoItemBean>>(this) {
//                    @Override
//                    public void onData(BaseResponse<InfoItemBean> value) {
//                        if (value.getCode() == 0) {
//                            switch (value.getData().getKey()) {
//                                case Const.TYPE_AVATOR:
//                                    Glide.with(FullInfoActivity.this).load(value.getData().getValue()).into(ivChooseImg);
//                                    userInfoBean.setAvatar(value.getData().getValue());
//                                    break;
//                            }
//                            ToastUtil.showToast("修改成功！");
//                        } else {
//                            ToastUtil.showToast(value.getMessage());
//                        }
//                    }
//                });
//    }

    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private void photo() {
        Glide.get(this.getApplicationContext()).clearMemory();
        Matisse.from(FullInfoActivity.this)
                .choose(MimeType.ofImage())//图片类型
                .countable(false)//true:选中后显示数字;false:选中后显示对号
                .maxSelectable(1)//可选的最大数
                .capture(true)//选择照片时，是否显示拍照
                .captureStrategy(new CaptureStrategy(true, "com.zadetk.helpyou"))//参数1 true表示拍照存储在共有目录，false表示存储在私有目录；参数2与 AndroidManifest中authorities值相同，用于适配7.0系统 必须设置
                .imageEngine(new GlideEngine())//图片加载引擎
                .forResult(0);//
    }
}
