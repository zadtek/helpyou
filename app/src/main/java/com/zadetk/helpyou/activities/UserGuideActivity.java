package com.zadetk.helpyou.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.GetarticlelistBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.UserInfoManger;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

import static com.zadetk.helpyou.activities.WebViewActivity.TagertUrl;
import static com.zadetk.helpyou.activities.WebViewActivity.Title;

public class UserGuideActivity extends AbsActivity {
    private static final String TAG = UserGuideActivity.class.getName();
    private RecyclerView guide;
    private GuideAdapter GuideAdapter;
    private TextView userguide;
    private List<GetarticlelistBean> data = new LinkedList<>();

    @Override
    public int setView() {
        return R.layout.activity_user_guide;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
       initView();
        initLogic();
        userguide();
        setTopTitle("用户指南");


    }
    private void initView(){
        guide = findViewById(R.id.tv_guide);
        userguide = findViewById(R.id.tv_title);
    }
    private void initLogic() {
        GuideAdapter = new GuideAdapter(R.layout.about_and_guide, data);
        guide.setAdapter(GuideAdapter);
        GuideAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {

                            String guideUrl  = data.get(position).getUrl();
                            Intent intent;
                            intent = new Intent(UserGuideActivity.this, WebViewActivity.class);
                            intent.putExtra(TagertUrl, guideUrl);
                            intent.putExtra(Title, "用户指南");
                            Log.i(TAG, "=====caizhijia====guideUrl========"+guideUrl);
                            startActivity(intent);
                            Log.i(TAG, "=====caizhijia====user========"+guideUrl);

            }
        });
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        guide.setLayoutManager(linearLayoutManager);


    }
    private void   userguide(){
        Map<String, Object> param = NetTool.newParams();
        param.put("articlecat", "2");
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        Log.i(TAG, "=====caizhijia====uid========"+ UserInfoManger.userInfo.getUid());
        Log.i(TAG, "=====caizhijia====token========"+ UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getGetarticlelist(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<GetarticlelistBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<GetarticlelistBean>> value) {

                        if(value.getCode() == 0){
                            List<GetarticlelistBean> user = value.getData();
                            data.addAll(user);
                            Log.i(TAG, "=====caizhijia====token========"+ data.get(1).getArticletitle());
                            GuideAdapter.notifyDataSetChanged();
                        }else{

                        }
                    }
                });

    }
    private class GuideAdapter extends BaseQuickAdapter<GetarticlelistBean, BaseViewHolder> {
        public GuideAdapter(int layoutResId, @Nullable List<GetarticlelistBean> data) {
            super(layoutResId, data);
        }
        @Override
        protected void convert(BaseViewHolder helper, GetarticlelistBean item) {
            helper.addOnClickListener(R.id.tv_title);
            helper.setText(R.id.tv_title, item.getArticletitle());
        }
    }

}
