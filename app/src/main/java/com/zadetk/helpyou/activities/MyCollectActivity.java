package com.zadetk.helpyou.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.CollectItemBean;
import com.zadetk.helpyou.bean.MyCollectResponseBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.ToastUtil;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * 我的管家页面
 */
public class MyCollectActivity extends AbsActivity {
    private RecyclerView recyclerView;
    private MyCollectAdapter myCollectAdapter;
    private List<CollectItemBean> data = new LinkedList<>();
    private int page = 1;
    private double lat, lnt;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        lat = getIntent().getDoubleExtra("lat", 0);
        lnt = getIntent().getDoubleExtra("lnt", 0);

    }

    @Override
    public int setView() {
        return R.layout.activity_my_collect;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initView();
        initLogic();
        requestData();
    }

    private void requestData() {
        Map<String, Object> param = NetTool.newParams(this);
        param.put("lat", lat);
        param.put("lnt", lnt);
        param.put("page", page);
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getMyCollect(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<MyCollectResponseBean>>() {
                    @Override
                    public void onData(BaseResponse<MyCollectResponseBean> value) {
                        if (value.getCode() == 0) {
                            if (value.getData().getItem() != null) {//容错判断
                                if (value.getData().getItem().isEmpty()) {
                                    myCollectAdapter.loadMoreEnd();
                                } else {
                                    data.addAll(value.getData().getItem());
                                    myCollectAdapter.notifyDataSetChanged();
                                    myCollectAdapter.loadMoreComplete();
                                    page++;
                                }
                            }
                        } else {
                            ToastUtil.showToast(value.getMessage());
                            myCollectAdapter.loadMoreEnd();
                        }
                    }
                });
    }

    private void initLogic() {
        myCollectAdapter = new MyCollectAdapter(R.layout.my_collect_item, data);
        myCollectAdapter.setOnLoadMoreListener(new BaseQuickAdapter.RequestLoadMoreListener() {
            @Override
            public void onLoadMoreRequested() {
                requestData();
            }
        }, recyclerView);
        myCollectAdapter.setOnItemClickListener(new BaseQuickAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(BaseQuickAdapter adapter, View view, int position) {
                ToastUtil.showToast(position + "被点击了");
            }
        });
        myCollectAdapter.setEmptyView(R.layout.normal_empty_view);
        myCollectAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.iv_message:
                        ToastUtil.showToast("message");
                        break;
                    case R.id.iv_call:
                        ToastUtil.showToast("call");
                        break;
                }
            }
        });
        recyclerView.setAdapter(myCollectAdapter);
    }

    private void initView() {
        recyclerView = findViewById(R.id.recycleview);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);
        setTopTitle("我的管家");
    }

    private class MyCollectAdapter extends BaseQuickAdapter<CollectItemBean, BaseViewHolder> {
        public MyCollectAdapter(int layoutResId, @Nullable List<CollectItemBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(BaseViewHolder helper, CollectItemBean item) {
            helper.setText(R.id.tv_order_name, item.getTitle());
            helper.setText(R.id.tv_order_distance, item.getSitedesc());
            SimpleRatingBar ratingBar = helper.getView(R.id.rb_star);
            ratingBar.setRating(Float.valueOf(item.getCommentnum()));
            helper.setText(R.id.tv_evaluate_num, "(  " + item.getCommentnum() + "  )");
            helper.setText(R.id.tv_phonenumber, "电话：  " + item.getMobile());
            Glide.with(MyCollectActivity.this)
                    .load(item.getPhotourl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into((ImageView) helper.getView(R.id.iv_avatar));

            helper.addOnClickListener(R.id.iv_message);
            helper.addOnClickListener(R.id.iv_call);
        }
    }
}
