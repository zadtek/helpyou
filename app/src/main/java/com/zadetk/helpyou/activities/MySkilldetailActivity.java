package com.zadetk.helpyou.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.SaveskilldetailBean;
import com.zadetk.helpyou.net.CmlRequestBody;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zadetk.helpyou.view.dialog.BaseNiceDialog;
import com.zadetk.helpyou.view.dialog.NiceDialog;
import com.zadetk.helpyou.view.dialog.ViewConvertListener;
import com.zadetk.helpyou.view.dialog.ViewHolder;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.saveskill;
import static com.zadetk.helpyou.Urls.saveskilldetail;
import static com.zadetk.helpyou.Urls.skilldetail;

/**
 * 4-3-3服务详情页面
 */
public class MySkilldetailActivity extends AbsActivity implements View.OnClickListener {
    private EditText etTitle;
    private TextView tvTitleNum;
    private EditText etContent;
    private TextView tvContentNum;
    //    private MultiPictureView multipleImage;
    private ImageView add_img;
    private RadioButton tvFulltime;
    private RadioButton tvParttime;
    private TextView tvChooseMethod;
    private EditText etPrice;
    private TextView tvPublish;
    private ArrayList<String> list = new ArrayList<>();
    private SaveskilldetailBean data = new SaveskilldetailBean();
    private static final String TAG = PublishServiceActivity.class.getName();
    private String type = "-1";
    private String payway = "-1";
    private String skillcat;
    private String amount;
    private CheckBox free;
    private CheckBox price;
    private File upFile;
    private String catid = "-1";
    private Bitmap bitmap = null;
    private File file;
    private ProgressDialog dialog;
    private String skillimg = "";

    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            switch (msg.what) {
                case 0:
                    String status = bundle.getString("status");//1保存成功；2失败
                    String response = bundle.getString("respons");
                    if (status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                    }
                    closeDialog();
                    break;
                case 1:
                    String error_code = bundle.getString("error_code");
                    if (error_code.equals("0")) {
                        etTitle.setText(bundle.getString("skilltitle"));
                        etContent.setText(bundle.getString("skilldesc"));
                        type = bundle.getString("type");//1全职；2兼职
                        if (type.equals("1"))
                            tvFulltime.setChecked(true);
                        else
                            tvParttime.setChecked(true);
                        payway = bundle.getString("payway");//1一口价；2免费预约
                        if (payway.equals("1"))
                            tvChooseMethod.setText("一口价");
                        else
                            tvChooseMethod.setText("免费预约");
                        etPrice.setText(bundle.getString("amount"));
                        skillimg = bundle.getString("skillimg");
                        Glide.with(MySkilldetailActivity.this).load(skillimg).into(add_img);
                    }
                    closeDialog();
                    break;
            }
        }
    };

    @Override
    public int setView() {
        return R.layout.activity_publish_service;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        catid = getIntent().getStringExtra("skillid");
        initViews();
        initLogic();
        dialog = ProgressDialog.show(MySkilldetailActivity.this, "", "正在加载……", true, false, null);
        initData();
    }

    /**
     * 服务详情回显
     */
    private void initData() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();
                builder.add("uid", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .add("token", UserInfoManger.userInfo.getUserToken())
                        .add("skillid", catid);
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(skilldetail).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "服务详情回显:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 1;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            String skilltitle = data.getString("skilltitle");
                            String skilldesc = data.getString("skilldesc");
                            String skillcat = data.getString("skillcat");
                            String type = data.getString("type");
                            String payway = data.getString("payway");
                            String amount = data.getString("amount");
                            String skillimg = data.getString("skillimg");
                            bundle.putString("skilltitle", skilltitle);
                            bundle.putString("skilldesc", skilldesc);
                            bundle.putString("skillcat", skillcat);
                            bundle.putString("type", type);
                            bundle.putString("payway", payway);
                            bundle.putString("amount", amount);
                            bundle.putString("skillimg", skillimg);
                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    closeDialog();
                    Log.i("yudan", "服务详情回显:" + e);
                    e.printStackTrace();
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "服务详情回显:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }


    private void initLogic() {

        tvChooseMethod.setOnClickListener(this);

        tvPublish.setOnClickListener(this);
        tvParttime.setOnClickListener(this);
        tvFulltime.setOnClickListener(this);
        add_img.setOnClickListener(this);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
            file = new File(Matisse.obtainPathResult(data).get(0));
            bitmap = BitmapFactory.decodeFile(Matisse.obtainPathResult(data).get(0));
            if (bitmap != null)
                add_img.setImageBitmap(bitmap);
        }
    }

    private void initViews() {
        setTopTitle("服务详情");
        etTitle = (EditText) findViewById(R.id.et_title);
        tvTitleNum = (TextView) findViewById(R.id.tv_title_num);
        etContent = (EditText) findViewById(R.id.et_content);
        tvContentNum = (TextView) findViewById(R.id.tv_content_num);
        add_img = (ImageView) findViewById(R.id.add_img);
        tvFulltime = (RadioButton) findViewById(R.id.tv_fulltime);
        tvParttime = (RadioButton) findViewById(R.id.tv_parttime);
        tvChooseMethod = findViewById(R.id.tv_choose_method);
        etPrice = (EditText) findViewById(R.id.et_price);
        tvPublish = (TextView) findViewById(R.id.tv_publish);
        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.radiogroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton radioButton = (RadioButton) group.findViewById(checkedId);
                int id = group.getCheckedRadioButtonId();
                RadioButton choise = (RadioButton) findViewById(id);
                if (choise.getId() == tvFulltime.getId()) {
                    type = "1";
                } else {
                    type = "2";
                }
            }
        });
        etTitle.addTextChangedListener(new TextWatcher() {

            private CharSequence etTitletemp;
            private int editTitletStart;
            private int editTitletentEnd;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etTitletemp = charSequence;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                editTitletStart = etTitle.getSelectionStart();
                editTitletentEnd = etTitle.getSelectionEnd();
                tvTitleNum.setText(etTitletemp.length() + "/15");
                if (etTitletemp.length() > 15) {
                    ToastUtil.showToast("你输入的字数已经超过了限制！");
                    editable.delete(editTitletStart - 1, editTitletentEnd);
                    int tempSelection = editTitletStart;
                    etTitle.setText(editable);
                    etTitle.setSelection(tempSelection);
                }
            }
        });

        etContent.addTextChangedListener(new TextWatcher() {//服务详情输入内容判断
            private CharSequence Contenttemp;
            private int editContentStart;
            private int editetContentEnd;

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                Contenttemp = charSequence;
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                editContentStart = etContent.getSelectionStart();
                editetContentEnd = etContent.getSelectionEnd();
                tvContentNum.setText(Contenttemp.length() + "/500");
                if (Contenttemp.length() > 500) {
                    ToastUtil.showToast("你输入的字数已经超过了限制！");
                    editable.delete(editContentStart - 1, editetContentEnd);
                    int tempSelection = editContentStart;
                    etContent.setText(editable);
                    etContent.setSelection(tempSelection);
                }

            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_choose_method:
                showChoose();
                break;
            case R.id.tv_publish:
                //发布岗位
                String skilltitle = etTitle.getText().toString().trim();
                String skilldesc = etContent.getText().toString().trim();
                String amount = etPrice.getText().toString().trim();
                if (skilltitle.equals("") || skilldesc.equals("")) {
                    Toast.makeText(getApplicationContext(), "请填写标题和描述", Toast.LENGTH_SHORT).show();
                } else {
                    if (type.equals("-1")) {
                        Toast.makeText(getApplicationContext(), "请选择全职或兼职", Toast.LENGTH_SHORT).show();
                    } else {
                        if (payway.equals("-1")) {
                            Toast.makeText(getApplicationContext(), "请选择收费方式", Toast.LENGTH_SHORT).show();
                        } else {
                            if (payway.equals("1")) {
                                if (amount.equals(""))
                                    Toast.makeText(getApplicationContext(), "请输入价格", Toast.LENGTH_SHORT).show();
                                else {
                                    dialog = ProgressDialog.show(MySkilldetailActivity.this, "", "正在发布……", true, false, null);
                                    publishService("skillimg", file, skilltitle, skilldesc, amount);
                                }
                            } else {
                                dialog = ProgressDialog.show(MySkilldetailActivity.this, "", "正在发布……", true, false, null);
                                if (amount.equals(""))
                                    publishService("skillimg", file, skilltitle, skilldesc, "100");
                                else
                                    publishService("skillimg", file, skilltitle, skilldesc, amount);
                            }
                        }
                    }
                }

//                upData();
                break;
            case R.id.tv_parttime:
                break;
            case R.id.tv_fulltime:
                break;
            case R.id.add_img:
                performCodeWithPermission("需要相机和读写存储权限", new PermissionCallback() {
                    @Override
                    public void hasPermission() {
                        photo();
                    }

                    @Override
                    public void noPermission() {

                    }
                }, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE});
                break;
        }
    }

    /**
     * 编辑服务
     */
    private void publishService(final String image, final File file, final String skilltitle, final String skilldesc, final String amount) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = null;
                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("uid", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .addFormDataPart("token", UserInfoManger.userInfo.getUserToken())
                        .addFormDataPart("skillid", catid)
                        .addFormDataPart("skilltitle", skilltitle)
                        .addFormDataPart("skilldesc", skilldesc)
                        .addFormDataPart("type", type)
                        .addFormDataPart("payway", payway)
                        .addFormDataPart("amount", amount);
                if (file != null) {
                    RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), file);
                    builder.addFormDataPart(image, ".jpg", fileBody);
                }
                requestBody = builder.build();
                Request.Builder request = new Request.Builder().url(saveskill)
                        .header("Content-Type", "application/x-www-form-urlencoded").post(new CmlRequestBody(requestBody) {
                            @Override
                            public void loading(long current, long total, boolean done) {
                            }
                        });
                OkHttpClient client = new OkHttpClient();
                client.newBuilder().readTimeout(60000, TimeUnit.MILLISECONDS).build().newCall(request.build()).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.i("yudan", "编辑服务:" + e);
                        closeDialog();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful() || response == null || response.body() == null) {
                            closeDialog();
                        } else {
                            String responseData = response.body().string();
                            Log.i("yudan", "编辑服务:" + responseData);
                            try {
                                JSONObject object = new JSONObject(responseData);
                                JSONObject data = object.getJSONObject("data");
                                String status = data.getString("status");
                                String respons = data.getString("response");
                                Bundle bundle = new Bundle();
                                bundle.putString("status", status);
                                bundle.putString("respons", respons);
                                Message message = new Message();
                                message.what = 0;
                                message.setData(bundle);
                                handler.sendMessage(message);
                            } catch (JSONException e) {
                                Log.i("yudan", "编辑服务:" + e);
                                e.printStackTrace();
                            }


                        }
                    }

                });
            }
        }).start();
    }

    private void showChoose() {
        NiceDialog niceDialog = NiceDialog.init();
        niceDialog.setLayoutId(R.layout.pay_choose).setConvertListener(new ViewConvertListener() {
            @Override
            protected void convertView(ViewHolder var1, BaseNiceDialog var2) {
                free = (CheckBox) var1.getView(R.id.free);
                free.setOnClickListener(checkbox_listener);
                price = (CheckBox) var1.getView(R.id.price);
                price.setOnClickListener(checkbox_listener);
            }

            View.OnClickListener checkbox_listener = new View.OnClickListener() {
                public void onClick(View v) {
                    switch (v.getId()) {
                        case R.id.free:
                            payway = "2";
                            price.setChecked(false);
                            tvChooseMethod.setText("免费预约");
                            break;
                        case R.id.price:
                            free.setChecked(false);
                            payway = "1";
                            tvChooseMethod.setText("一口价");
                            break;

                    }

                }
            };


        }).setShowBottom(true).show(getSupportFragmentManager());
    }

    private void photo() {
        Glide.get(this.getApplicationContext()).clearMemory();
        Matisse.from(MySkilldetailActivity.this)
                .choose(MimeType.ofImage())//图片类型
                .countable(false)//true:选中后显示数字;false:选中后显示对号
                .maxSelectable(1)//可选的最大数
                .capture(true)//选择照片时，是否显示拍照
                .captureStrategy(new CaptureStrategy(true, "com.zadetk.helpyou"))//参数1 true表示拍照存储在共有目录，false表示存储在私有目录；参数2与 AndroidManifest中authorities值相同，用于适配7.0系统 必须设置
                .imageEngine(new GlideEngine())//图片加载引擎
                .forResult(0);//
    }

    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }
}