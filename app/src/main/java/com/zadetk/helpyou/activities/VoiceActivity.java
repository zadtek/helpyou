package com.zadetk.helpyou.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.iflytek.cloud.ErrorCode;
import com.iflytek.cloud.InitListener;
import com.iflytek.cloud.RecognizerListener;
import com.iflytek.cloud.RecognizerResult;
import com.iflytek.cloud.SpeechConstant;
import com.iflytek.cloud.SpeechError;
import com.iflytek.cloud.SpeechRecognizer;
import com.iflytek.cloud.SpeechSynthesizer;
import com.iflytek.cloud.SpeechUtility;
import com.iflytek.cloud.SynthesizerListener;
import com.zadetk.helpyou.App;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.adapter.VoiceAdapter;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.VoiceBean;
import com.zadetk.helpyou.im.ChatActivity;
import com.zadetk.helpyou.im.Constant;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.SPUtil;
import com.zadetk.helpyou.utils.ToastUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import pl.droidsonroids.gif.GifImageView;

import static com.zadetk.helpyou.Urls.search;
import static com.zadetk.helpyou.other.Const.USER_UID;

/**
 * 8-1语音页面
 */
public class VoiceActivity extends AbsActivity implements View.OnClickListener {
    //语音合成器
    private SpeechSynthesizer mSynthesizer;
    private GifImageView gifBtn;
    private SpeechRecognizer mIat;
    private RecognizerListener mRecognizerListener;
    private TextView header;
    private int ret;
    private TextView tvVoiceContent;
    private RecyclerView recycleview;
    //    private RecyclerView recycleview2;
    private VoiceAdapter voiceAdpater;
    //    private ViewFlipper viewFlipper;
    private List<VoiceBean> dataList = new LinkedList<>();
    private List<VoiceBean> linshiList = new LinkedList<>();
    //    private List<VoiceBean> data2 = new LinkedList<>();
//    private Voice2Adpater voice2Adpater;
    private TextView tvTitle;
//    private TextView tvName;
//    private TextView tvDec1;
//    private TextView tvEvaluateNum;
//    private SimpleRatingBar rbMark;
//    private TextView tvPrice;
//    private MapView map;
//    private TextView vBtnCall;
//    private TextView vBtnMessage;
//    private TextView vAddress;
//    private ImageView ivImg;
//    private TextView tvBtnReservation;

    //    private String[] per = {ACCESS_COARSE_LOCATION, WRITE_EXTERNAL_STORAGE, READ_PHONE_STATE};
//    private AMap aMap;
//    private MyLocationStyle myLocationStyle;
    private double lat = 0, lnt = 0;
    private ProgressDialog dialog;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String error_code = bundle.getString("error_code");
            switch (msg.what) {
                case 0:
                    dataList.clear();
                    dataList.addAll(linshiList);
                    voiceAdpater.notifyDataSetChanged();
                    if (error_code.equals("0")) {
                        header.setVisibility(View.VISIBLE);
                        String reponse = bundle.getString("reponse");
                        tvTitle.setText(reponse);
                        if (dataList.size() > 0) {
                            String listtitle = bundle.getString("listtitle");
                            header.setText(listtitle);
                            header.setVisibility(View.VISIBLE);
                        } else {
                            header.setVisibility(View.GONE);
                        }
                        //语音播报
                        //处理语音合成关键类
                        mSynthesizer = SpeechSynthesizer.createSynthesizer(VoiceActivity.this, mInitListener);
//                        //设置发音人
//                        mSynthesizer.setParameter(SpeechConstant.VOICE_NAME, "xiaoyan");
//                        //设置音调
//                        mSynthesizer.setParameter(SpeechConstant.PITCH, "50");
//                        //设置音量
//                        mSynthesizer.setParameter(SpeechConstant.VOLUME, "50");
                        mSynthesizer.startSpeaking(reponse, new SynthesizerListener() {
                            @Override
                            public void onSpeakBegin() {

                            }

                            @Override
                            public void onBufferProgress(int i, int i1, int i2, String s) {

                            }

                            @Override
                            public void onSpeakPaused() {

                            }

                            @Override
                            public void onSpeakResumed() {

                            }

                            @Override
                            public void onSpeakProgress(int i, int i1, int i2) {

                            }

                            @Override
                            public void onCompleted(SpeechError speechError) {

                            }

                            @Override
                            public void onEvent(int i, int i1, int i2, Bundle bundle) {

                            }
                        });
                    } else {
                        header.setVisibility(View.GONE);
                    }
                    closeDialog();
                    break;
                case 7:
                    Toast.makeText(getApplicationContext(), "没有网络", Toast.LENGTH_SHORT).show();
                    break;
            }
        }
    };

//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        //在activity执行onDestroy时执行mMapView.onDestroy()，销毁地图
//        map.onDestroy();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        //在activity执行onResume时执行map.onResume ()，重新绘制加载地图
//        map.onResume();
//    }
//
//    @Override
//    protected void onPause() {
//        super.onPause();
//        //在activity执行onPause时执行map.onPause ()，暂停地图的绘制
//        map.onPause();
//    }
//
//    @Override
//    protected void onSaveInstanceState(Bundle outState) {
//        super.onSaveInstanceState(outState);
//        //在activity执行onSaveInstanceState时执行map.onSaveInstanceState (outState)，保存地图当前的状态
//        map.onSaveInstanceState(outState);
//    }

//    @Override
//    public void onBackPressed() {
//        if (viewFlipper.getDisplayedChild() == 0) {
//            super.onBackPressed();
//        } else {
//            viewFlipper.showPrevious();
//        }
//    }

    @Override
    protected void onStop() {
        super.onStop();
        mIat.stopListening();
    }

    @Override
    public int getStatusBarColor() {
        return R.color.transparent;
    }

    @Override
    public boolean isfit() {
        return false;
    }

    @Override
    public LinearLayout initToolbar(ViewGroup parent) {
        return null;
    }

    @Override
    public int setView() {
        return R.layout.activity_voice;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();
//        map.onCreate(savedInstanceState);
        lat = getIntent().getDoubleExtra("lat", 0);
        lnt = getIntent().getDoubleExtra("lnt", 0);
        Log.i("yudan", lat + "," + lnt);
    }


    private void initLogic() {
        initSpeech();
        voiceAdpater = new VoiceAdapter(VoiceActivity.this, dataList);
        recycleview.setAdapter(voiceAdpater);
//        View v = getLayoutInflater().inflate(R.layout.voice_header, null);
//        voiceAdpater.addHeaderView(v);
//        startListen();
        gifBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startListen();
                tvVoiceContent.setText("正在倾听...");
//                tvTitle.setVisibility(View.GONE);
//                recycleview.setVisibility(View.GONE);
//                if (viewFlipper.getDisplayedChild() == 1) {
//                    viewFlipper.showPrevious();
//                }
            }
        });
        voiceAdpater.setOnItemClickListener(new VoiceAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(getApplicationContext(), ServiceDetailsActivity.class);
                intent.putExtra("siteid", dataList.get(position).getSiteid());
                startActivity(intent);
//                viewFlipper.showNext();
            }
        });

//        voice2Adpater = new Voice2Adpater(R.layout.voice_item2, data2);
//        View v2 = getHeaderView2();
//        voice2Adpater.addHeaderView(v2);
//        recycleview2.setAdapter(voice2Adpater);
//        vBtnCall.setOnClickListener(this);
//        vBtnMessage.setOnClickListener(this);
    }

//    private View getHeaderView2() {
//        View v = getLayoutInflater().inflate(R.layout.voice_header2, null);
//        tvTitle = v.findViewById(R.id.tv_title);
//        tvName = v.findViewById(R.id.tv_name);
//        tvDec1 = v.findViewById(R.id.tv_dec1);
//        tvEvaluateNum = v.findViewById(R.id.tv_evaluate_num);
//        rbMark = v.findViewById(R.id.rb_mark);
//        tvPrice = v.findViewById(R.id.tv_price);
//        map = v.findViewById(R.id.map);
//        vBtnCall = v.findViewById(R.id.v_btn_call);
//        vBtnMessage = v.findViewById(R.id.v_btn_message);
//        vAddress = v.findViewById(R.id.v_address);
//        ivImg = v.findViewById(R.id.iv_img);
//        tvBtnReservation = v.findViewById(R.id.tv_btn_reservation);
//        performCodeWithPermission("需要定位权限", new PermissionCallback() {
//            @Override
//            public void hasPermission() {
//                setupMap();
//            }
//
//            @Override
//            public void noPermission() {
//
//            }
//        }, per);
//        return v;
//    }


//    private void setupMap() {
//        if (aMap == null) {
//            aMap = map.getMap();
//        }
//        myLocationStyle = new MyLocationStyle();//初始化定位蓝点样式类myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_LOCATION_ROTATE);//连续定位、且将视角移动到地图中心点，定位点依照设备方向旋转，并且会跟随设备移动。（1秒1次定位）如果不设置myLocationType，默认也会执行此种模式。
//        myLocationStyle.interval(2000); //设置连续定位模式下的定位间隔，只在连续定位模式下生效，单次定位模式下不会生效。单位为毫秒。
//        myLocationStyle.myLocationType(MyLocationStyle.LOCATION_TYPE_FOLLOW);//定位一次，且将视角移动到地图中心点。
//        aMap.setMyLocationStyle(myLocationStyle);//设置定位蓝点的Style
//        aMap.setMyLocationEnabled(true);// 设置为true表示启动显示定位蓝点，false表示隐藏定位蓝点并不进行定位，默认是false。
//        UiSettings uiSet = aMap.getUiSettings();
//        uiSet.setZoomControlsEnabled(false);
//    }

    private void initViews() {
        header = findViewById(R.id.header);
        gifBtn = findViewById(R.id.gif2);
        tvVoiceContent = findViewById(R.id.tv_voice_content);
        tvTitle = findViewById(R.id.tv_title);
        recycleview = findViewById(R.id.recycleview);
//        recycleview2 = findViewById(R.id.recycleview2);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleview.setLayoutManager(linearLayoutManager);
//        LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(this);
//        linearLayoutManager2.setOrientation(LinearLayoutManager.VERTICAL);
//        recycleview2.setLayoutManager(linearLayoutManager2);
        tvVoiceContent.setText("");
//        tvTitle.setVisibility(View.GONE);
//        recycleview.setVisibility(View.GONE);
//        viewFlipper = findViewById(R.id.viewflipper);
//        viewFlipper.setInAnimation(this, R.anim.push_pic_left_in);
//        viewFlipper.setOutAnimation(this, R.anim.push_pic_right_out);

    }


    private void initSpeech() {
        SpeechUtility.createUtility(this, SpeechConstant.APPID + "=5c34074e");
        mIat = SpeechRecognizer.createRecognizer(VoiceActivity.this, null);
        mRecognizerListener = new RecognizerListener() {

            @Override
            public void onBeginOfSpeech() {
                LogUtil.d("onBeginOfSpeech");
            }

            @Override
            public void onError(SpeechError error) {
                LogUtil.d("onError" + error.getErrorDescription());
                ToastUtil.showToast("你好像没有说话哦！");
                tvVoiceContent.setText("你好像没有说话哦！");
                //FIXME 测试代码
//                tvVoiceContent.setText("这是程序测试内容，请在发布前删除");
//                tvTitle.setVisibility(View.VISIBLE);
//                recycleview.setVisibility(View.VISIBLE);

            }

            @Override
            public void onEndOfSpeech() {
                LogUtil.d("onEndOfSpeech");
            }

            @Override
            public void onResult(RecognizerResult results, boolean isLast) {
                String result = printResult(results);
                if (TextUtils.isEmpty(result)) {
                    return;
                }
                tvVoiceContent.setText(result);
                dialog = ProgressDialog.show(VoiceActivity.this, "", "正在加载中……", true, false, null);
                initData(result);
//                tvTitle.setVisibility(View.VISIBLE);
//                recycleview.setVisibility(View.VISIBLE);
//                if (isLast) {
//                }
            }

            @Override
            public void onVolumeChanged(int volume, byte[] data) {
            }

            @Override
            public void onEvent(int eventType, int arg1, int arg2, Bundle obj) {

            }
        };
        mIat.setParameter(SpeechConstant.PARAMS, null);

        // 设置听写引擎
        mIat.setParameter(SpeechConstant.ENGINE_TYPE, SpeechConstant.TYPE_CLOUD);
        // 设置返回结果格式
        mIat.setParameter(SpeechConstant.RESULT_TYPE, "json");

        // 设置语言
        mIat.setParameter(SpeechConstant.LANGUAGE, "zh_cn");
        // 设置语言区域
        mIat.setParameter(SpeechConstant.ACCENT, "mandarin");
        mIat.setParameter(SpeechConstant.ASR_PTT, "0");
    }

    public String parseVoice(String resultString) {
        Gson gson = new Gson();
        Voice voiceBean = gson.fromJson(resultString, Voice.class);

        StringBuilder sb = new StringBuilder();
        ArrayList<Voice.WSBean> ws = voiceBean.ws;
        for (Voice.WSBean wsBean : ws) {
            String word = wsBean.cw.get(0).w;
            sb.append(word);
        }
        return sb.toString();
    }

    private String printResult(RecognizerResult results) {
        String text = parseVoice(results.getResultString());
//        ToastUtil.showToast(text);
        return text;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.v_btn_call:
                call("13708079634");
                break;
            case R.id.v_btn_message:
                final String name = SPUtil.getData(VoiceActivity.this, USER_UID, 0).toString();
                if (!TextUtils.isEmpty(name)){
                    Intent i = new Intent(mActivity, ChatActivity.class);
                    i.putExtra(Constant.EXTRA_USER_ID, name);
                    i.putExtra("username", name);
                    startActivity(i);
                }
                break;
        }
    }

    private void call(final String phoneNumber) {
        performCodeWithPermission("拨打电话的权限", new PermissionCallback() {
            @Override
            public void hasPermission() {
                Uri uri = Uri.parse("tel:" + phoneNumber);
                Intent intent = new Intent(Intent.ACTION_CALL, uri);
                if (ActivityCompat.checkSelfPermission(VoiceActivity.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                startActivity(intent);
            }

            @Override
            public void noPermission() {
            }
        }, Manifest.permission.CALL_PHONE);
    }

    private void startListen() {
        performCodeWithPermission("需要语音权限", new PermissionCallback() {
            @Override
            public void hasPermission() {
                ret = mIat.startListening(mRecognizerListener);
                ToastUtil.showToast("开始识别");
                if (ret != ErrorCode.SUCCESS) {
                    ToastUtil.showToast("识别失败,错误码：" + ret);
                }
//                tvTitle.setVisibility(View.GONE);
//                recycleview.setVisibility(View.GONE);
            }

            @Override
            public void noPermission() {

            }
        }, Manifest.permission.RECORD_AUDIO);
    }

    public class Voice {

        public ArrayList<WSBean> ws;

        public class WSBean {
            public ArrayList<CWBean> cw;
        }

        public class CWBean {
            public String w;
        }
    }


//    private class Voice2Adpater extends BaseQuickAdapter<VoiceBean, BaseViewHolder> {
//
//        public Voice2Adpater(int layoutResId, @Nullable List<VoiceBean> data) {
//            super(layoutResId, data);
//        }
//
//        @Override
//        protected void convert(BaseViewHolder helper, VoiceBean item) {
//            NoScrollView scrollView = helper.getView(R.id.gv_img);
//            ArrayList<String> temp = new ArrayList<>();
//            temp.add("");
//            temp.add("");
//            temp.add("");
//            temp.add("");
//            scrollView.setAdapter(new MyAdapter<String>(temp, R.layout.img) {
//
//                @Override
//                public void bindView(ViewHolder holder, String obj) {
//
//                }
//            });
//
//        }
//    }

    /**
     * 语音搜索
     *
     * @param keyWords
     */
    public void initData(final String keyWords) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                linshiList.clear();
                Log.i("yudan", lat + "," + lnt);
                FormBody.Builder builder = new FormBody.Builder()
                        .add("keywords", keyWords)
                        .add("uid", UserInfoManger.userInfo.getUid() + "")
                        .add("token", UserInfoManger.userInfo.getUserToken())
                        .add("lat", String.valueOf(lat)).add("lnt", String.valueOf(lnt));
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(search).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "语音搜索:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 0;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            String reponse = data.getString("reponse");
                            String listtitle = data.getString("listtitle");
                            bundle.putString("reponse", reponse);
                            bundle.putString("listtitle", listtitle);
                            JSONArray item = data.getJSONArray("item");
                            for (int i = 0; i < item.length(); i++) {
                                JSONObject jsonObject = item.getJSONObject(i);
                                VoiceBean bean = new VoiceBean();
                                bean.setSiteid(jsonObject.getInt("siteid"));
                                bean.setSitetitle(jsonObject.getString("sitetitle"));
                                bean.setSitedesc(jsonObject.getString("sitedesc"));
                                bean.setStarnum(jsonObject.getDouble("starnum"));
                                bean.setCommentnum(jsonObject.getInt("commentnum"));
                                bean.setPhotourl(jsonObject.getString("photourl"));
                                linshiList.add(bean);
                            }

                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    Log.i("yudan", "搜索:" + e);
                    closeDialog();
                    e.printStackTrace();
                    Message message = new Message();
                    message.what = 7;
                    handler.sendMessage(message);
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "搜索:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    private InitListener mInitListener = new InitListener() {
        @Override
        public void onInit(int code) {
            Log.e("tag", "initListener init code = " + code);
        }
    };
}
