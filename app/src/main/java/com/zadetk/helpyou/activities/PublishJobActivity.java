package com.zadetk.helpyou.activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chad.library.adapter.base.BaseQuickAdapter;
import com.chad.library.adapter.base.BaseViewHolder;
import com.goyourfly.multi_picture.ImageLoader;
import com.goyourfly.multi_picture.MultiPictureView;
import com.zadetk.helpyou.R;
import com.zadetk.helpyou.adapter.MyAdapter;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.bean.JobcatlistBean;
import com.zadetk.helpyou.bean.JobinsertBean;
import com.zadetk.helpyou.bean.JoblistBean;
import com.zadetk.helpyou.bean.SavejobBean;
import com.zadetk.helpyou.net.BaseObserver;
import com.zadetk.helpyou.net.BaseResponse;
import com.zadetk.helpyou.net.CmlRequestBody;
import com.zadetk.helpyou.net.NetTool;
import com.zadetk.helpyou.other.UserInfoManger;
import com.zadetk.helpyou.utils.LogUtil;
import com.zadetk.helpyou.utils.ToastUtil;
import com.zadetk.helpyou.view.NoScrollView;
import com.zhihu.matisse.Matisse;
import com.zhihu.matisse.MimeType;
import com.zhihu.matisse.engine.impl.GlideEngine;
import com.zhihu.matisse.internal.entity.CaptureStrategy;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import me.nereo.multi_image_selector.MultiImageSelector;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

import static com.zadetk.helpyou.Urls.jobdetail;
import static com.zadetk.helpyou.Urls.jobinsert;
import static com.zadetk.helpyou.Urls.savejob;
import static com.zadetk.helpyou.Urls.saveskill;
import static com.zadetk.helpyou.Urls.skilldetail;

/**
 * 6-1发布岗位页面
 */
public class PublishJobActivity extends AbsActivity implements View.OnClickListener {

    private static final String TAG = "";
    private RecyclerView gvCategory;
    private EditText etTitle;
    private EditText etContent;
    //    private MultiPictureView multipleImage;
    private JobcatListAdapter jobcatlistAdapter;
    private EditText etLow;
    private EditText etHigh;
    private Switch sw;
    private TextView tvBtnSubmit;
    private ArrayList<String> list = new ArrayList<>();
    private File upFile;
    private String swIs = "1";//是否议价（1-可议价 2-否议价）
    private List<JobcatlistBean> data = new LinkedList<>();
    private String jobid;
    private ImageView add_img;
    private Bitmap bitmap = null;
    private int service = 0;
    private File file;
    private ProgressDialog dialog;
    private String job_cat;
    private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            Bundle bundle = msg.getData();
            String status = bundle.getString("status");//1保存成功；2失败
            String response = bundle.getString("respons");
            switch (msg.what) {
                case 0:
                    if (status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "发布成功", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                    }
                    closeDialog();
                    break;
                case 2:
                    String error_code = bundle.getString("error_code");
                    if (error_code.equals("0")) {
                        etTitle.setText(bundle.getString("jobtitle"));
                        etContent.setText(bundle.getString("jobdesc"));
                        etHigh.setText(bundle.getString("high"));
                        etLow.setText(bundle.getString("low"));
                        String can_bargin = bundle.getString("can_bargin");
                        if (can_bargin.equals("1")) {
                            sw.setChecked(true);
                        } else {
                            sw.setChecked(false);
                        }
                        job_cat = bundle.getString("job_cat");
                        for (int i = 0; i < data.size(); i++) {
                            JobcatlistBean bean = data.get(i);
                            if (bean.getCatid().equals(job_cat))
                                bean.setSelected(1);
                        }
                        if (jobcatlistAdapter != null)
                            jobcatlistAdapter.notifyDataSetChanged();
                        Glide.with(PublishJobActivity.this).load(bundle.getString("jobimg")).into(add_img);
                    }
                    closeDialog();
                    break;
                case 3:
                    if (status.equals("1")) {
                        Toast.makeText(getApplicationContext(), "修改成功", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), response, Toast.LENGTH_SHORT).show();
                    }
                    closeDialog();
                    break;
            }
        }
    };

    private void initViews() {

        Intent intent = getIntent();
        jobid = intent.getStringExtra("jobid");
        gvCategory = (RecyclerView) findViewById(R.id.gv_category);
        etTitle = (EditText) findViewById(R.id.et_title);
        etContent = (EditText) findViewById(R.id.et_content);
//        multipleImage = (MultiPictureView) findViewById(R.id.multiple_image);
        etLow = (EditText) findViewById(R.id.et_low);
        etHigh = (EditText) findViewById(R.id.et_high);
        sw = (Switch) findViewById(R.id.sw);
        tvBtnSubmit = (TextView) findViewById(R.id.tv_btn_submit);
        add_img = findViewById(R.id.add_img);
        if (jobid != null) {
            setTopTitle("修改已发布的岗位");
            dialog = ProgressDialog.show(PublishJobActivity.this, "", "正在加载……", true, false, null);
            getJobInfo();
        } else {
            setTopTitle("发布岗位");
        }

        sw.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    swIs = "1";
                } else {
                    swIs = "2";
                }
            }
        });
    }

    @Override
    public int setView() {
        return R.layout.activity_publish_job;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();
        getJobcatlist();


    }


    private void initLogic() {
        MultiPictureView.setImageLoader(new ImageLoader() {
            @Override
            public void loadImage(ImageView image, Uri uri) {
                Glide.with(PublishJobActivity.this)
                        .load(uri)
                        .placeholder(R.drawable.placeholder)
                        .error(R.drawable.placeholder)
                        .into(image);
            }
        });

        add_img.setOnClickListener(this);
        tvBtnSubmit.setOnClickListener(this);

        jobcatlistAdapter = new JobcatListAdapter(R.layout.tag_checkbox, data);
        gvCategory.setLayoutManager(new GridLayoutManager(this, 4));
        gvCategory.setAdapter(jobcatlistAdapter);
        jobcatlistAdapter.setOnItemChildClickListener(new BaseQuickAdapter.OnItemChildClickListener() {
            @Override
            public void onItemChildClick(BaseQuickAdapter adapter, View view, int position) {
                switch (view.getId()) {
                    case R.id.classification:
                        JobcatlistBean item = data.get(position);
                        if (item.getSelected() == 1) {
                            item.setSelected(0);
                            service = 0;
                        } else {
                            service = Integer.parseInt(item.getCatid());
                            item.setSelected(1);
                            for (int i = 0; i < data.size(); i++) {
                                JobcatlistBean bean = data.get(i);
                                if (i != position) {
                                    bean.setSelected(0);
                                }

                            }
                        }
                        jobcatlistAdapter.notifyDataSetChanged();
                        break;
                }
            }
        });

    }

    private class JobcatListAdapter extends BaseQuickAdapter<JobcatlistBean, BaseViewHolder> {
        public JobcatListAdapter(int layoutResId, @Nullable List<JobcatlistBean> data) {
            super(layoutResId, data);
        }

        @Override
        protected void convert(final BaseViewHolder helper, final JobcatlistBean item) {
            helper.addOnClickListener(R.id.classification);
            helper.setText(R.id.classification, item.getCatname());
            CheckBox classification = helper.getView(R.id.classification);
            if (item.getSelected() == 0) {
                classification.setChecked(false);
            } else {
                classification.setChecked(true);
            }
        }
    }

    /**
     * @param view 发布岗位
     */
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_btn_submit:
                if (jobid == null) {
                    //发布岗位
                    if (file != null && file.exists()) {
                        String title = etTitle.getText().toString().trim();
                        String content = etContent.getText().toString().trim();
                        String hign = etHigh.getText().toString().trim();
                        String low = etLow.getText().toString().trim();
                        if (title.equals("") || content.equals("") || hign.equals("") || low.equals(""))
                            Toast.makeText(getApplicationContext(), "请将内容填全", Toast.LENGTH_SHORT).show();
                        else {
                            if (service == 0)
                                Toast.makeText(getApplicationContext(), "请选择分类", Toast.LENGTH_SHORT).show();
                            else {
                                dialog = ProgressDialog.show(PublishJobActivity.this, "", "正在发布……", true, false, null);
                                publishJob("image", file, title, content, hign, low);
                            }
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), "图片不存在，请重新选择", Toast.LENGTH_SHORT).show();
                    }
                } else {
                    //编辑岗位
                    String title = etTitle.getText().toString().trim();
                    String content = etContent.getText().toString().trim();
                    String hign = etHigh.getText().toString().trim();
                    String low = etLow.getText().toString().trim();
                    if (title.equals("") || content.equals("") || hign.equals("") || low.equals(""))
                        Toast.makeText(getApplicationContext(), "请将内容填全", Toast.LENGTH_SHORT).show();
                    else {
                        if (service == 0)
                            Toast.makeText(getApplicationContext(), "请选择分类", Toast.LENGTH_SHORT).show();
                        else {
                            dialog = ProgressDialog.show(PublishJobActivity.this, "", "正在发布……", true, false, null);
                            editJob("image", file, service);
                        }
                    }

                }
                break;
            case R.id.add_img:
                performCodeWithPermission("需要相机和读写存储权限", new PermissionCallback() {
                    @Override
                    public void hasPermission() {
                        photo();
                    }

                    @Override
                    public void noPermission() {

                    }
                }, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE});
                break;

        }
    }

    private void photo() {
        Glide.get(this.getApplicationContext()).clearMemory();
        Matisse.from(PublishJobActivity.this)
                .choose(MimeType.ofImage())//图片类型
                .countable(false)//true:选中后显示数字;false:选中后显示对号
                .maxSelectable(1)//可选的最大数
                .capture(true)//选择照片时，是否显示拍照
                .captureStrategy(new CaptureStrategy(true, "com.zadetk.helpyou"))//参数1 true表示拍照存储在共有目录，false表示存储在私有目录；参数2与 AndroidManifest中authorities值相同，用于适配7.0系统 必须设置
                .imageEngine(new GlideEngine())//图片加载引擎
                .forResult(0);//
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 0 && resultCode == RESULT_OK) {
//            for (int i = 0; i < Matisse.obtainPathResult(data).size(); i++) {
//                File file = new File(Matisse.obtainPathResult(data).get(i));
//                if (file.exists()) {
//                    upLoadImage(i + list.size(), "img", file);
//                }
//            }
//            for (int i = 0; i < Matisse.obtainPathResult(data).size(); i++) {
//                list.add(BitmapFactory.decodeFile(Matisse.obtainPathResult(data).get(i)));
//            }
//            adapter.notifyDataSetChanged();
            file = new File(Matisse.obtainPathResult(data).get(0));
            bitmap = BitmapFactory.decodeFile(Matisse.obtainPathResult(data).get(0));
            if (bitmap != null)
                add_img.setImageBitmap(bitmap);
        }

    }

    // FIXME: 2018/7/19 0019 图片上传和分类还有些问题

    /**
     * 提交发布岗位
     */
//    private void getJobinsert() {
//        Map<String, Object> param = NetTool.newParams();
//        param.put("user_id", UserInfoManger.userInfo.getUid());
//        param.put("token", UserInfoManger.userInfo.getUserToken());
//        param.put("service", 2);
//        param.put("title", etTitle.getText().toString());
//        param.put("content", etContent.getText().toString());
//        param.put("image", upFile);
//        param.put("high", etLow.getText());
//        param.put("low", etHigh.getText());
//        param.put("is", swIs);
//        NetTool.getApi().getJobinsert(param)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new BaseObserver<BaseResponse<List<JobinsertBean>>>() {
//
//                    @Override
//                    public void onData(BaseResponse<List<JobinsertBean>> value) {
//                        if (value.getCode() == 0) {
//                            ToastUtil.showToast("提交成功！");
//                            finish();
//                        } else {
//                            LogUtil.e(value.error_code + " error code");
//                        }
//                    }
//                });
//    }


    /**
     * 获取岗位分类
     */
    private void getJobcatlist() {
        Map<String, Object> param = NetTool.newParams();
        param.put("uid", UserInfoManger.userInfo.getUid());
        param.put("token", UserInfoManger.userInfo.getUserToken());
        Log.i(TAG, "kasumi" + UserInfoManger.userInfo.getUserToken());
        NetTool.getApi().getJobcatlist(param)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new BaseObserver<BaseResponse<List<JobcatlistBean>>>() {
                    @Override
                    public void onData(BaseResponse<List<JobcatlistBean>> value) {
                        if (value.getCode() == 0) {
                            data.clear();
                            List<JobcatlistBean> user = value.getData();
                            for (int i = 0; i < user.size(); i++) {
                                JobcatlistBean bean = user.get(i);
                                bean.setSelected(0);
                                data.add(bean);
                            }
                            if (job_cat != null) {
                                for (int i = 0; i < data.size(); i++) {
                                    JobcatlistBean bean = data.get(i);
                                    if (bean.getCatid().equals(job_cat)) {
                                        bean.setSelected(1);
                                        break;
                                    }

                                }
                            }
//                            Log.i(TAG, "kasumi" + data);
//                            ToastUtil.showToast("赋值成功！");
                            LogUtil.e(user + " " + value.getMessage());
                            jobcatlistAdapter.notifyDataSetChanged();
                        } else {
                            LogUtil.e(value.error_code + " error code");
                        }
                    }
                });
    }

//    /**
//     * 修改已发布
//     */
//    private void getSavejob() {
//        Map<String, Object> param = NetTool.newParams();
//        param.put("uid", UserInfoManger.userInfo.getUid());
//        param.put("token", UserInfoManger.userInfo.getUserToken());
//        param.put("jobid", jobid);
//        param.put("service", 2);
//        param.put("title", etTitle.getText().toString());
//        param.put("content", etContent.getText().toString());
//        param.put("image", upFile);
//        param.put("high", etLow.getText());
//        param.put("low", etHigh.getText());
//        param.put("is", swIs);
//        NetTool.getApi().getSavejob(param)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new BaseObserver<BaseResponse<List<SavejobBean>>>() {
//                    @Override
//                    public void onData(BaseResponse<List<SavejobBean>> value) {
//                        if (value.getCode() == 0) {
//                            ToastUtil.showToast("修改成功！");
//                            finish();
//                        } else {
//                            LogUtil.e(value.error_code + " error code");
//                        }
//                    }
//                });
//    }

    /**
     * 上传图片
     */
    private void publishJob(final String fileName, final File file, final String title, final String content, final String hign, final String low) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), file);
                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart(fileName, ".jpg", fileBody)
                        .addFormDataPart("user_id", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .addFormDataPart("token", UserInfoManger.userInfo.getUserToken())
                        .addFormDataPart("service", String.valueOf(service))
                        .addFormDataPart("title", title)
                        .addFormDataPart("content", content)
                        .addFormDataPart("high", hign)
                        .addFormDataPart("low", low)
                        .addFormDataPart("is", swIs);
                RequestBody requestBody = builder.build();
                Request.Builder request = new Request.Builder().url(jobinsert)
                        .header("Content-Type", "application/x-www-form-urlencoded").post(new CmlRequestBody(requestBody) {
                            @Override
                            public void loading(long current, long total, boolean done) {
                            }
                        });
                OkHttpClient client = new OkHttpClient();
                client.newBuilder().readTimeout(60000, TimeUnit.MILLISECONDS).build().newCall(request.build()).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.i("yudan", "发布岗位接口:" + e);
                        closeDialog();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful() || response == null || response.body() == null) {
                            closeDialog();
                        } else {
                            String responseData = response.body().string();
                            Log.i("yudan", "发布岗位接口:" + responseData);
                            try {
                                JSONObject object = new JSONObject(responseData);
                                JSONObject data = object.getJSONObject("data");
                                String status = data.getString("status");
                                String respons = data.getString("response");
                                Bundle bundle = new Bundle();
                                bundle.putString("status", status);
                                bundle.putString("respons", respons);
                                Message message = new Message();
                                message.what = 0;
                                message.setData(bundle);
                                handler.sendMessage(message);
                            } catch (JSONException e) {
                                Log.i("yudan", "发布岗位接口:" + e);
                                e.printStackTrace();
                            }


                        }
                    }

                });
            }
        }).start();
    }

    private void closeDialog() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    /**
     * 岗位信息回显
     */
    public void getJobInfo() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                FormBody.Builder builder = new FormBody.Builder();
                builder.add("uid", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .add("token", UserInfoManger.userInfo.getUserToken())
                        .add("jobid", jobid);
                RequestBody requestBody = builder.build();
                Request request = new Request.Builder().url(jobdetail).header("Content-Type", "application/x-www-form-urlencoded").post(requestBody).build();
                try {
                    Response response = new OkHttpClient().newCall(request).execute();
                    if (response.isSuccessful()) {
                        String responseData = response.body().string();
                        Log.i("yudan", "岗位信息回显:" + responseData);
                        JSONObject object = new JSONObject(responseData);
                        String error_code = object.getString("error_code");//返回码
                        Message message = new Message();
                        message.what = 2;
                        Bundle bundle = new Bundle();
                        if (error_code.equals("0")) {
                            JSONObject data = object.getJSONObject("data");
                            String jobtitle = data.getString("jobtitle");
                            String jobdesc = data.getString("jobdesc");
                            String low = data.getString("low");
                            String high = data.getString("high");
                            String job_cat = data.getString("job_cat");
                            String can_bargin = data.getString("can_bargin");
                            String jobimg = data.getString("jobimg");
                            bundle.putString("jobtitle", jobtitle);
                            bundle.putString("jobdesc", jobdesc);
                            bundle.putString("low", low);
                            bundle.putString("high", high);
                            bundle.putString("job_cat", job_cat);
                            bundle.putString("can_bargin", can_bargin);
                            bundle.putString("jobimg", jobimg);
                        }
                        bundle.putString("error_code", error_code);
                        message.setData(bundle);
                        handler.sendMessage(message);
                    } else {
                        throw new IOException("Unexpected code" + response);
                    }
                } catch (IOException e) {
                    closeDialog();
                    Log.i("yudan", "岗位信息回显:" + e);
                    e.printStackTrace();
                } catch (JSONException e) {
                    closeDialog();
                    Log.i("yudan", "岗位信息回显:" + e);
                    e.printStackTrace();
                }
            }
        }).start();
    }

    /**
     * 编辑岗位
     */
    private void editJob(final String image, final File file, final int service) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                RequestBody requestBody = null;
                MultipartBody.Builder builder = new MultipartBody.Builder()
                        .setType(MultipartBody.FORM)
                        .addFormDataPart("user_id", String.valueOf(UserInfoManger.userInfo.getUid()))
                        .addFormDataPart("token", UserInfoManger.userInfo.getUserToken())
                        .addFormDataPart("jobid", jobid)
                        .addFormDataPart("title", etTitle.getText().toString().trim())
                        .addFormDataPart("content", etContent.getText().toString().trim())
                        .addFormDataPart("service", service + "")
                        .addFormDataPart("low", etLow.getText().toString().trim())
                        .addFormDataPart("high", etHigh.getText().toString().trim())
                        .addFormDataPart("is", swIs);
                if (file != null) {
                    RequestBody fileBody = RequestBody.create(MediaType.parse("application/octet-stream"), file);
                    builder.addFormDataPart(image, ".jpg", fileBody);
                }
                requestBody = builder.build();
                Request.Builder request = new Request.Builder().url(savejob)
                        .header("Content-Type", "application/x-www-form-urlencoded").post(new CmlRequestBody(requestBody) {
                            @Override
                            public void loading(long current, long total, boolean done) {
                            }
                        });
                OkHttpClient client = new OkHttpClient();
                client.newBuilder().readTimeout(60000, TimeUnit.MILLISECONDS).build().newCall(request.build()).enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        Log.i("yudan", "编辑岗位:" + e);
                        closeDialog();
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        if (!response.isSuccessful() || response == null || response.body() == null) {
                            closeDialog();
                        } else {
                            String responseData = response.body().string();
                            Log.i("yudan", "编辑岗位:" + responseData);
                            try {
                                JSONObject object = new JSONObject(responseData);
                                JSONObject data = object.getJSONObject("data");
                                String status = data.getString("status");
                                String respons = data.getString("response");
                                Bundle bundle = new Bundle();
                                bundle.putString("status", status);
                                bundle.putString("respons", respons);
                                Message message = new Message();
                                message.what = 3;
                                message.setData(bundle);
                                handler.sendMessage(message);
                            } catch (JSONException e) {
                                Log.i("yudan", "编辑岗位:" + e);
                                e.printStackTrace();
                            }


                        }
                    }

                });
            }
        }).start();
    }
}
