package com.zadetk.helpyou.activities;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.TextView;

import com.zadetk.helpyou.R;
import com.zadetk.helpyou.base.AbsActivity;
import com.zadetk.helpyou.fragments.MyDemandFragment;
import com.zadetk.helpyou.fragments.MyServiceFragment;
import com.zadetk.helpyou.fragments.RobOrderFragment;

import java.util.LinkedList;
import java.util.List;

/**
 * 4-0我的订单页面
 */
public class MyOrderActivity extends AbsActivity implements View.OnClickListener {
    private TextView tvRoborder;
    private TextView tvMydemand;
    private TextView tvMyservice;
    private ViewPager viewpager;
    private pagerAdapter adapter;
    private List<Fragment> fragments = new LinkedList<>();

    private void initViews() {
        tvRoborder = findViewById(R.id.tv_roborder);
        tvMydemand = findViewById(R.id.tv_mydemand);
        tvMyservice = findViewById(R.id.tv_myservice);
        viewpager = findViewById(R.id.viewpager);
        setTopTitle("我的订单");
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mToolbar.setElevation(0);
        }
    }

    @Override
    public int setView() {
        return R.layout.activity_my_order;
    }

    @Override
    public void initBasic(Bundle savedInstanceState) {
        initViews();
        initLogic();

       int index =  getIntent().getIntExtra("index", 0);
        switchTab(index);
    }

    private void initLogic() {
        fragments.add(new RobOrderFragment());
        fragments.add(new MyDemandFragment());
        fragments.add(new MyServiceFragment());
        adapter = new pagerAdapter(getSupportFragmentManager());
        viewpager.setAdapter(adapter);
        viewpager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switchTab(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tvMyservice.setOnClickListener(this);
        tvMydemand.setOnClickListener(this);
        tvRoborder.setOnClickListener(this);
    }

    private void switchTab(int position) {

        int greenColor = getResources().getColor(R.color.colorGreen);
        int greyColor = getResources().getColor(R.color.textColorGrey);
        Drawable down = getResources().getDrawable(R.drawable.arrow_down);
        down.setBounds(0, 0, down.getMinimumWidth(), down.getMinimumHeight());
        Drawable up = getResources().getDrawable(R.drawable.arrow_up_green);
        up.setBounds(0, 0, up.getMinimumWidth(), up.getMinimumHeight());

        tvRoborder.setTextColor(greyColor);
        tvRoborder.setCompoundDrawables(null, null, down, null);
        tvMydemand.setTextColor(greyColor);
        tvMydemand.setCompoundDrawables(null, null, down, null);
        tvMyservice.setTextColor(greyColor);
        tvMyservice.setCompoundDrawables(null, null, down, null);

        switch (position) {
            case 0:
                tvRoborder.setTextColor(greenColor);
                tvRoborder.setCompoundDrawables(null, null, up, null);
                break;
            case 1:
                tvMydemand.setTextColor(greenColor);
                tvMydemand.setCompoundDrawables(null, null, up, null);
                break;
            case 2:
                tvMyservice.setTextColor(greenColor);
                tvMyservice.setCompoundDrawables(null, null, up, null);
                break;
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_myservice:
                switchTab(2);
                viewpager.setCurrentItem(2);
                break;
            case R.id.tv_mydemand:
                switchTab(1);
                viewpager.setCurrentItem(1);
                break;
            case R.id.tv_roborder:
                switchTab(0);
                viewpager.setCurrentItem(0);
                break;
        }
    }

    private class pagerAdapter extends FragmentPagerAdapter {

        public pagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }
    }

}
