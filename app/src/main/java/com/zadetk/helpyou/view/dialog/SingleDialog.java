package com.zadetk.helpyou.view.dialog;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;

/**
 * Created by ASUS on 2018/4/24.
 */

public class SingleDialog extends DialogFragment{

    private String title;
    private String[] item;
    private String positiveText;
    private String negtiveText;
    private SingleCallBack singleCallBack;
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(title).setSingleChoiceItems(item, 0, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                singleCallBack.onItems(dialogInterface,i);
            }
        }).setPositiveButton(positiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
               singleCallBack.onButton(dialogInterface,i);
            }
        }).setNegativeButton(negtiveText, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                singleCallBack.onNegtiveButton(dialogInterface,i);
            }
        });
        return builder.create();
    }
    public void init(String title,String[] item,String positiveText,String negtiveText,SingleCallBack singleCallBack){
        this.title = title;
        this.item = item;
        this.positiveText = positiveText;
        this.negtiveText = negtiveText;
        this.singleCallBack = singleCallBack;
    }

    public interface SingleCallBack{
        void onItems(DialogInterface dialogInterface, int i);
        void onButton(DialogInterface dialogInterface, int i);
        void onNegtiveButton(DialogInterface dialogInterface, int i);
    }
}
